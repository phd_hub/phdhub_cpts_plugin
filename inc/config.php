<?php
/*
 * PhD Hub CPTs - Files
 * Include all required files
 */
defined('ABSPATH') or die;

/*
 * Core Files
 */
include_once dirname(plugin_dir_path(__FILE__)) . '/inc/config/register-custom-cpts.php'; // Register all custom CPTs
include_once dirname(plugin_dir_path(__FILE__)) . '/inc/config/phdhub-user-roles.php'; // Functions for custom CPTs
include_once dirname(plugin_dir_path(__FILE__)) . '/inc/config/phdhub-cpts-functions.php'; // Functions for custom CPTs
include_once dirname(plugin_dir_path(__FILE__)) . '/inc/config/institutions-cpt.php'; // Custom Institutions CPT
include_once dirname(plugin_dir_path(__FILE__)) . '/inc/config/faculties-cpt.php'; // Custom Faculties CPT
include_once dirname(plugin_dir_path(__FILE__)) . '/inc/config/phd-programs-cpt.php'; // Custom PhD Programs CPT
include_once dirname(plugin_dir_path(__FILE__)) . '/inc/config/phd-openings-cpt.php'; // Custom PhD Openings CPT
include_once dirname(plugin_dir_path(__FILE__)) . '/inc/config/phd-templates-cpt.php'; // Custom PhD Templates CPT
include_once dirname(plugin_dir_path(__FILE__)) . '/inc/config/cooperation-calls-cpt.php'; // Custom Cooperation Calls CPT
include_once dirname(plugin_dir_path(__FILE__)) . '/inc/config/research-teams-cpt.php'; // Custom Research Teams CPT
include_once dirname(plugin_dir_path(__FILE__)) . '/inc/config/companies-cpt.php'; // Custom Companies CPT
include_once dirname(plugin_dir_path(__FILE__)) . '/inc/config/phdhub-user-meta.php'; // Custom User Meta based on roles
include_once dirname(plugin_dir_path(__FILE__)) . '/inc/config/class-tgm-plugin-activation.php'; // TGM Plugin Activation class
include_once dirname(plugin_dir_path(__FILE__)) . '/inc/config/phdhub-widgets.php'; // Register PhD Hub's Widgets
include_once dirname(plugin_dir_path(__FILE__)) . '/inc/config/phdhub-shortcodes.php'; // Register PhD Hub's Shortcodes
include_once dirname(plugin_dir_path(__FILE__)) . '/inc/config/phdhub-cpts-scripts.php'; // Load CSS/JS Scripts

/*
 * Frontend Forms
 */

?>