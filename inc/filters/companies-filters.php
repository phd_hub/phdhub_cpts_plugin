<?php

	defined('ABSPATH') or die;

	/*
	 * Display Filters for companies CPT
	 */

	$companyCountryFilter = (isset($_GET['company_country']) ? $_GET['company_country'] : null);
?>
<form class="companies-filters">
	<select name="company_country">
		<option <?php echo selected( $companyCountryFilter, 'all'); ?> value="all"><?php echo __('Select Country', 'phdhub-cpts'); ?></option>
		<option <?php echo selected( $companyCountryFilter, 'all'); ?> value="all"><?php echo __('All Countries', 'phdhub-cpts'); ?></option>
		<?php
			$location_values = get_posts(
				array(
					'post_type' => 'companies',
					'meta_key' => 'country',
					'posts_per_page' => -1,
				)
			);

			$locations = array();
			foreach( $location_values as $location ) {
				$location_value = esc_attr( get_post_meta( $location->ID, 'country', true ) );
				if (!empty ($location_value) ) {
					if(!in_array($location_value, $locations)){
						array_push($locations, $location_value);
					}
				}
			}
			sort($locations);
			foreach( $locations as $location_val ) {
		?>
		<option <?php echo selected( $companyCountryFilter, $location_val ); ?> value="<?php echo $location_val; ?>"><?php echo $location_val; ?></option>
		<?php
			}
		?>
	</select>
	<input type="submit" name="filterCompaniess" value="<?php echo __('Find Companies', 'phdhub-cpts'); ?>">
</form>