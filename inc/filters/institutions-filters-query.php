<?php
	defined('ABSPATH') or die;

	/*
	 * Change query for events_list CPT based on filters
	 */
	$institutionCountryFilter = (isset($_GET['institution_country']) ? $_GET['institution_country'] : null);
	$institutionFindFilter = (isset($_GET['filterInstitutions']) ? $_GET['filterInstitutions'] : null);

	$newQuery = array();
	if ($institutionCountryFilter != 'all') {
		$newQuery['meta_query'][] = array(
			array(
				'key' => 'country',
				'value' => $institutionCountryFilter,
				'compare' => 'LIKE',
			),
		);
	}
	else if ($institutionCountryFilter == 'all') {
		$newQuery['meta_query'][] = array(
			array(
				'key' => 'country',
				'value' => '',
				'compare' => 'LIKE',
			),
		);
	}

	if ($post && $post->post_type == 'institutions') {
		if( is_archive( 'institutions' ) ) {
			if($institutionFindFilter) {
				$args = array_merge(
					$wp_query->query_vars,
					$newQuery,
					array(
						'order' => 'ASC',
					)
				);

				query_posts($args);
			}
		}
	}
?>