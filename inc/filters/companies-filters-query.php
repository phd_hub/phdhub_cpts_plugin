<?php
	defined('ABSPATH') or die;

	/*
	 * Change query for events_list CPT based on filters
	 */
	$companyCountryFilter = (isset($_GET['company_country']) ? $_GET['company_country'] : null);
	$companyFindFilter = (isset($_GET['filterCompanies']) ? $_GET['filterCompanies'] : null);

	$newQuery = array();
	if ($companyCountryFilter != 'all') {
		$newQuery['meta_query'][] = array(
			array(
				'key' => 'country',
				'value' => $companyCountryFilter,
				'compare' => 'LIKE',
			),
		);
	}
	else if ($companyCountryFilter == 'all') {
		$newQuery['meta_query'][] = array(
			array(
				'key' => 'country',
				'value' => '',
				'compare' => 'LIKE',
			),
		);
	}

	if ($post && $post->post_type == 'companies') {
		if( is_archive( 'companies' ) ) {
			if($companyFindFilter) {
				$args = array_merge(
					$wp_query->query_vars,
					$newQuery,
					array(
						'order' => 'ASC',
					)
				);

				query_posts($args);
			}
		}
	}
?>