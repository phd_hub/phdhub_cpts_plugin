<?php

defined('ABSPATH') or die;

/*
 * Display Custom Meta Fields for PhD Templates CPT
 */
function display_phd_template_meta( $phd_template ) {
	/*
	 * Create the custom meta fields for PhD Templates CPT
	 */
	$salary = 'mean_salary';
    $mean_salary = get_post_meta( $phd_template->ID, 'mean_salary', true );
	$conditions = 'phd_template_conditions';
    $phd_template_conditions = get_post_meta( $phd_template->ID, 'phd_template_conditions', true );
	$info = 'phd_template_info';
    $phd_template_info = get_post_meta( $phd_template->ID, 'phd_template_info', true );
?>
<div class="phd-cpt-box">
	<div class="uk-grid">
		<!-- Start Left Column -->
		<div class="uk-width-1-2">
			<div class="inner-settings-box-right">
				<!-- Start Meta Field: Mean Salary -->
				<p>
					<label><?php echo __('Mean Salary', 'phdhub-cpts'); ?></label>
				</p>
				<?php wp_editor( html_entity_decode(stripcslashes($mean_salary)), $salary ); ?>
				<!-- End Meta Field: Mean Salary -->
				<!-- Start Meta Field: Conditions -->
				<p>
					<label><?php echo __('Conditions', 'phdhub-cpts'); ?></label>
				</p>
				<?php wp_editor( html_entity_decode(stripcslashes($phd_template_conditions)), $conditions ); ?>
				<!-- End Meta Field: Conditions -->
			</div>
		</div>
		<!-- End Left Column -->
		<!-- Start Right Column -->
		<div class="uk-width-1-2 inner-settings-box-right">
			<!-- Start Meta Field: Other Info -->
			<p>
				<label><?php echo __('Other Info', 'phdhub-cpts'); ?></label>
			</p>
			<?php wp_editor( html_entity_decode(stripcslashes($phd_template_info)), $info ); ?>
			<!-- End Meta Field: Other Info -->
		</div>
		<!-- End Right Column -->
	</div>
</div>
<?php
}



/*
 * Save Custom Meta Fields of PhD Templates CPT
 */
function save_phd_template_fields( $phd_template_id, $phd_template ) {
    if ( $phd_template->post_type == 'phd-templates' ) {
		/* Save Meta Field: Mean Salary */
        if ( isset( $_POST['mean_salary'] )) {
            update_post_meta( $phd_template->ID, 'mean_salary', $_POST['mean_salary'] );
        }
		/* Save Meta Field: Conditions */
        if ( isset( $_POST['phd_template_conditions'] )) {
            update_post_meta( $phd_template->ID, 'phd_template_conditions', $_POST['phd_template_conditions'] );
        }
		/* Save Meta Field: Other Info */
        if ( isset( $_POST['phd_template_info'] )) {
            update_post_meta( $phd_template->ID, 'phd_template_info', $_POST['phd_template_info'] );
        }
    }
}
add_action( 'save_post', 'save_phd_template_fields', 10, 2 );

?>