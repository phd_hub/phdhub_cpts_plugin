<?php

defined('ABSPATH') or die;


/*
 * Display Custom Meta Fields for PhD Openings CPT
 */
function display_phd_opening_meta( $phd_opening ) {
	/*
	 * Create the custom meta fields for PhD Openings CPT
	 */
    $institution = esc_attr( get_post_meta( $phd_opening->ID, 'institution', true ) );
    $institution_label = esc_attr( get_post_meta( $phd_opening->ID, 'institution_label', true ) );
    $faculty = esc_attr( get_post_meta( $phd_opening->ID, 'faculty', true ) );
    $phd_program = esc_attr( get_post_meta( $phd_opening->ID, 'phd_program', true ) );
    $research_team = esc_attr( get_post_meta( $phd_opening->ID, 'research_team', true ) );
    $short_info = esc_attr( get_post_meta( $phd_opening->ID, 'short_info', true ) );
    $thesis_supervisor = esc_attr( get_post_meta( $phd_opening->ID, 'thesis_supervisor', true ) );
    $thesis_supervisor_label = esc_attr( get_post_meta( $phd_opening->ID, 'thesis_supervisor_label', true ) );
    $duration = esc_attr( get_post_meta( $phd_opening->ID, 'duration', true ) );
    $duration_type = esc_attr( get_post_meta( $phd_opening->ID, 'duration_type', true ) );
    $tuition_fees = esc_attr( get_post_meta( $phd_opening->ID, 'tuition_fees', true ) );
    $tuition_fees_currency = esc_attr( get_post_meta( $phd_opening->ID, 'tuition_fees_currency', true ) );
    $master_degree = esc_attr( get_post_meta( $phd_opening->ID, 'master_degree', true ) );
    $type = esc_attr( get_post_meta( $phd_opening->ID, 'type', true ) );
    $starting_date = esc_attr( get_post_meta( $phd_opening->ID, 'starting_date', true ) );
    $deadline = esc_attr( get_post_meta( $phd_opening->ID, 'deadline', true ) );
    $funded = esc_attr( get_post_meta( $phd_opening->ID, 'funded', true ) );
    $link = esc_attr( get_post_meta( $phd_opening->ID, 'link', true ) );
    $phd_template = esc_attr( get_post_meta( $phd_opening->ID, 'phd_template', true ) );

	$info = 'phd_opening_info';
    $phd_opening_info = get_post_meta( $phd_opening->ID, 'phd_opening_info', true );
?>
<div class="phd-cpt-box">
    <ul id="phdhub-cpt-nav" data-uk-switcher="{connect: '#offer-settings'}">
        <li><a href=""><?php echo __('Description', 'phdhub-cpts'); ?></a></li>
        <li><a href=""><?php echo __('Basic Details', 'phdhub-cpts'); ?></a></li>
        <li><a href=""><?php echo __('More Info', 'phdhub-cpts'); ?></a></li>
    </ul>
    
    <ul id="offer-settings" class="uk-switcher">
        <li>
			<?php wp_editor( html_entity_decode(stripcslashes($phd_opening_info)), $info ); ?>
        </li>
        <li>
            <div class="uk-grid">
                <div class="uk-width-1-2">
                    <div class="inner-settings-box">
                        <input type="hidden" id="phd-id" value="<?php echo $phd_opening->ID; ?>" />
                        <!-- Start Meta Field: Institution -->
                        <p>
                            <label><?php echo __('Institution', 'phdhub-cpts'); ?></label>
                            <input type="text" name="institution_label" id="institution-autocomplete" value="<?php echo $institution_label; ?>" placeholder="<?php echo __('Search by typing institution name', 'phdhub-cpts'); ?>..." required />
                            <input type="hidden" name="institution" value="<?php echo $institution; ?>" required />
                        </p>
                        <!-- End Meta Field: Institution -->
                        <!-- Start Meta Field: Faculty -->
                        <p>
                            <label><?php echo __('Faculty', 'phdhub-cpts'); ?></label>
                            <select id="faculty" name="faculty">
                                <option value="empty" <?php selected( $faculty, 'empty' ); ?>><?php echo __('Select an institution before selecting a faculty', 'phdhub-cpts'); ?></option>
                                <?php institution_faculty_list( ); ?>
                            </select>
                        </p>
                        <!-- End Meta Field: Faculty -->
                        <!-- Start Meta Field: PhD Program -->
                        <p>
                            <label><?php echo __('PhD Program', 'phdhub-cpts'); ?></label>
                            <select id="phd-program" name="phd_program">
                                <option value="empty" <?php selected( $phd_program, 'empty' ); ?>><?php echo __('Select PhD Program', 'phdhub-cpts'); ?></option>
                                <?php institution_phd_programs_list( ); ?>
                            </select>
                        </p>
                        <!-- End Meta Field: PhD Program -->
                        <!-- Start Meta Field: Research Team -->
                        <p>
                            <label><?php echo __('Research Team', 'phdhub-cpts'); ?></label>
                            <select id="research-team" name="research_team">
                                <option value="empty" <?php selected( $research_team, 'empty' ); ?>><?php echo __('Select Research Team', 'phdhub-cpts'); ?></option>
                                <?php institution_research_teams_list( ); ?>
                            </select>
                        </p>
                        <!-- End Meta Field: Research Team -->
                    </div>
                </div>
                <div class="uk-width-1-2">
                    <div class="inner-settings-box">
                        <!-- Start Meta Field: Thesis Coordinator -->
                        <p>
                            <label><?php echo __('Thesis Supervisor', 'phdhub-cpts'); ?></label>
                            <input type="text" name="thesis_supervisor_label" id="supervisor-autocomplete" value="<?php echo $thesis_supervisor_label; ?>" placeholder="<?php echo __('Search by typing supervisor\'s name', 'phdhub-cpts'); ?>..." required />
                            <input type="hidden" name="thesis_supervisor" value="<?php echo $thesis_supervisor; ?>" required />
                        </p>
                        <!-- End Meta Field: Thesis Coordinator -->
                        <div class="uk-grid">
                            <div class="uk-width-1-2">
                                <!-- Start Meta Field: Duration -->
                                <p>
                                    <label><?php echo __('Duration (in months)', 'phdhub-cpts'); ?></label>
                                    <input type="number" min="1" step="1" name="duration" value="<?php echo $duration; ?>" />
                                </p>
                                <!-- End Meta Field: Duration -->
                            </div>
                            <div class="uk-width-1-2">
                                <!-- Start Meta Field: Duration Type -->
                                <p>
                                    <label><?php echo __('Display duration to users as', 'phdhub-cpts'); ?></label>
                                    <select name="duration_type">
                                        <option value="months" <?php selected( $duration_type, 'months' ); ?>><?php echo 'Months'; ?></option>
                                        <option value="years" <?php selected( $duration_type, 'years' ); ?>><?php echo 'Years'; ?></option>
                                    </select>
                                </p>
                                <!-- End Meta Field: Duration Type -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <div class="uk-grid">
                <div class="uk-width-1-2">
                    <div class="inner-settings-box">
						<div class="uk-grid">
							<!-- Start Inner Left Column -->
							<div class="uk-width-1-2">
								<!-- Start Meta Field: Tuition Fees -->
								<p>
									<label><?php echo __('Tuition Fees', 'phdhub-cpts'); ?></label>
									<input type="number" step="1" min="1" name="tuition_fees" value="<?php echo $tuition_fees; ?>" />
								</p>
								<!-- End Meta Field: Tuition Fees -->
							</div>
							<!-- End Inner Left Column -->
							<!-- Start Inner Middle Column -->
							<div class="uk-width-1-2">
								<!-- Start Meta Field: Tuition Fees Currency -->
								<p>
									<label><?php echo __('Currency', 'phdhub-cpts'); ?></label>
									<select name="tuition_fees_currency">
										<option value="eur" <?php selected( $tuition_fees_currency, 'eur' ); ?>><?php echo '€'; ?></option>
										<option value="bgn" <?php selected( $tuition_fees_currency, 'bgn' ); ?>><?php echo 'лв'; ?></option>
										<option value="gpb" <?php selected( $tuition_fees_currency, 'gbp' ); ?>><?php echo '£'; ?></option>
										<option value="hrk" <?php selected( $tuition_fees_currency, 'hrk' ); ?>><?php echo 'kn'; ?></option>
										<option value="czk" <?php selected( $tuition_fees_currency, 'czk' ); ?>><?php echo 'Kč'; ?></option>
										<option value="dkk" <?php selected( $tuition_fees_currency, 'dkk' ); ?>><?php echo 'kr'; ?></option>
										<option value="huf" <?php selected( $tuition_fees_currency, 'huf' ); ?>><?php echo 'Ft'; ?></option>
										<option value="pln" <?php selected( $tuition_fees_currency, 'pln' ); ?>><?php echo 'zł'; ?></option>
										<option value="ron" <?php selected( $tuition_fees_currency, 'ron' ); ?>><?php echo 'Leu'; ?></option>
										<option value="chf" <?php selected( $tuition_fees_currency, 'chf' ); ?>><?php echo 'Fr.'; ?></option>
									</select>
								</p>
								<!-- End Meta Field: Tuition Fees -->
							</div>
							<!-- End Inner Middle Column -->
                            <div class="uk-width-1-1">
								<!-- Start Meta Field: Master Degree -->
								<p>
									<label><?php echo __('Master Degree', 'phdhub-cpts'); ?></label>
									<select name="master_degree">
										<option value="empty" <?php selected( $master_degree, 'empty' ); ?>><?php echo __('Select', 'phdhub-cpts'); ?></option>
										<option value="required" <?php selected( $master_degree, 'required' ); ?>><?php echo __('Required', 'phdhub-cpts'); ?></option>
										<option value="optional" <?php selected( $master_degree, 'optional' ); ?>><?php echo __('Optional', 'phdhub-cpts'); ?></option>
									</select>
								</p>
								<!-- End Meta Field: Master Degree -->
                            </div>
                            <div class="uk-width-1-1">
                                <!-- Start Meta Field: Full/Part Time -->
                                <p>
                                    <label><?php echo __('Full/Part Time', 'phdhub-cpts'); ?></label>
                                    <select name="type">
                                        <option value="empty" <?php selected( $type, 'empty' ); ?>><?php echo __('Select', 'phdhub-cpts'); ?></option>
                                        <option value="full-time" <?php selected( $type, 'full-time' ); ?>><?php echo __('Full Time', 'phdhub-cpts'); ?></option>
                                        <option value="part-time" <?php selected( $type, 'part-time' ); ?>><?php echo __('Part Time', 'phdhub-cpts'); ?></option>
                                    </select>
                                </p>
                                <!-- End Meta Field: Full/Part Time -->
                            </div>
                            <!-- Start Inner Right Column -->
                            <div class="uk-width-1-1">
                                <!-- Start Meta Field: Funded or Not -->
                                <p>
                                    <label><?php echo __('Funded', 'phdhub-cpts'); ?></label>
                                    <select name="funded">
                                        <option value="empty" <?php selected( $funded, 'empty' ); ?>><?php echo __('Select', 'phdhub-cpts'); ?></option>
                                        <option value="partial" <?php selected( $funded, 'partial' ); ?>><?php echo __('Partial Funded', 'phdhub-cpts'); ?></option>
                                        <option value="full" <?php selected( $funded, 'full' ); ?>><?php echo __('Full Funded', 'phdhub-cpts'); ?></option>
                                        <option value="no" <?php selected( $funded, 'no' ); ?>><?php echo __('Not Funded', 'phdhub-cpts'); ?></option>
                                    </select>
                                </p>
                                <!-- End Meta Field: Funded or Not -->
                            </div>
                            <!-- End Inner Right Column -->
                        </div>  
                    </div>
                </div>
                <div class="uk-width-1-2">
                    <div class="inner-settings-box">
                        <div class="uk-grid">
                            <!-- Start Inner Left Column -->
                            <div class="uk-width-1-2">
                                <!-- Start Meta Field: Starting Date -->
                                <p>
                                    <label><?php echo __('Starting Date', 'phdhub-cpts'); ?></label>
                                    <input type="date" name="starting_date" value="<?php echo $starting_date; ?>" />
                                </p>
                                <!-- End Meta Field: Starting Date-->
                            </div>
                            <!-- End Inner Left Column -->
                            <!-- Start Inner Right Column -->
                            <div class="uk-width-1-2">
                                <!-- Start Meta Field: Deadline to Apply -->
                                <p>
                                    <label><?php echo __('Deadline to Apply', 'phdhub-cpts'); ?></label>
                                    <input type="date" name="deadline" value="<?php echo $deadline; ?>" />
                                </p>
                                <!-- End Meta Field: Deadline to Apply -->
                            </div>
                            <!-- End Inner Right Column -->
                        </div>
                        <!-- Start Meta Field: Apply Now link -->
                        <p>
                            <label><?php echo __('Apply Now Link', 'phdhub-cpts'); ?></label>
                            <input type="url" size="80" name="link" value="<?php echo $link; ?>" />
                        </p>
                        <!-- End Meta Field: Apply Now link -->
                        <!-- Start Meta Field: PhD Template -->
                        <p class="uk-hidden">
                            <label><?php echo __('PhD Template', 'phdhub-cpts'); ?></label>
                            <select name="phd_template">
                                <option value="empty" <?php selected( $phd_template, 'empty' ); ?>><?php echo __('Select a PhD Template', 'phdhub-cpts'); ?></option>
                                <?php
                                    /*
                                     * Get all PhD Templates and display them as an option
                                     */
                                    $args = array(
                                        'posts_per_page' => -1,
                                        'post_type' => 'phd-templates',
                                        'orderby'=> 'title',
                                        'order' => 'ASC'
                                    );

                                    $phd_templates = get_posts($args);
                                    foreach ($phd_templates as $phd_template_item) {
                                ?>
                                <option value="<?php echo $phd_template_item->ID; ?>" <?php selected( $phd_template, $phd_template_item->ID ); ?>><?php echo $phd_template_item->post_title; ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </p>
                        <!-- End Meta Field: PhD Template -->
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>
<?php
}



/*
 * Save Custom Meta Fields of PhD Openings CPT
 */
function save_phd_openings_fields( $phd_opening_id, $phd_opening ) {
    if ( $phd_opening->post_type == 'phd-openings' ) {
		/* Save Meta Field: Institution */
        if ( isset( $_POST['institution'] )) {
            update_post_meta( $phd_opening->ID, 'institution', $_POST['institution'] );
        }
		/* Save Meta Field: Institution */
        if ( isset( $_POST['institution_label'] )) {
            update_post_meta( $phd_opening->ID, 'institution_label', $_POST['institution_label'] );
        }
		/* Save Meta Field: Faculty */
        if ( isset( $_POST['faculty'] )) {
            update_post_meta( $phd_opening->ID, 'faculty', $_POST['faculty'] );
        }
		/* Save Meta Field: Thesis Supervisor */
        if ( isset( $_POST['thesis_supervisor'] )) {
			if (username_exists($_POST['thesis_supervisor'])) {
				update_post_meta( $phd_opening->ID, 'thesis_supervisor', $_POST['thesis_supervisor'] );
			}
        }
		/* Save Meta Field: Thesis Supervisor */
        if ( isset( $_POST['thesis_supervisor_label'] )) {
			if (username_exists($_POST['thesis_supervisor'])) {
				update_post_meta( $phd_opening->ID, 'thesis_supervisor_label', $_POST['thesis_supervisor_label'] );
			}
        }
		/* Save Meta Field: Tuition Fees */
        if ( isset( $_POST['tuition_fees'] )) {
            update_post_meta( $phd_opening->ID, 'tuition_fees', $_POST['tuition_fees'] );
        }
		/* Save Meta Field: Tuition Fees Currency */
        if ( isset( $_POST['tuition_fees_currency'] )) {
            update_post_meta( $phd_opening->ID, 'tuition_fees_currency', $_POST['tuition_fees_currency'] );
        }
		/* Save Meta Field: Master Degree */
        if ( isset( $_POST['master_degree'] )) {
            update_post_meta( $phd_opening->ID, 'master_degree', $_POST['master_degree'] );
        }
		/* Save Meta Field: Duration */
        if ( isset( $_POST['duration'] )) {
            update_post_meta( $phd_opening->ID, 'duration', $_POST['duration'] );
        }
		/* Save Meta Field: Duration Type */
        if ( isset( $_POST['duration_type'] )) {
            update_post_meta( $phd_opening->ID, 'duration_type', $_POST['duration_type'] );
        }
		/* Save Meta Field: Type (Full/Part Time) */
        if ( isset( $_POST['type'] )) {
            update_post_meta( $phd_opening->ID, 'type', $_POST['type'] );
        }
		/* Save Meta Field: Starting Date */
        if ( isset( $_POST['starting_date'] )) {
            update_post_meta( $phd_opening->ID, 'starting_date', $_POST['starting_date'] );
        }
		/* Save Meta Field: Deadline to Apply */
        if ( isset( $_POST['deadline'] )) {
            update_post_meta( $phd_opening->ID, 'deadline', $_POST['deadline'] );
        }
		/* Save Meta Field: Funded or not */
        if ( isset( $_POST['funded'] )) {
            update_post_meta( $phd_opening->ID, 'funded', $_POST['funded'] );
        }
		/* Save Meta Field: Apply Now link */
        if ( isset( $_POST['link'] )) {
            update_post_meta( $phd_opening->ID, 'link', $_POST['link'] );
        }
		/* Save Meta Field: PhD Program */
        if ( isset( $_POST['phd_program'] )) {
            update_post_meta( $phd_opening->ID, 'phd_program', $_POST['phd_program'] );
        }
		/* Save Meta Field: Research Team */
        if ( isset( $_POST['research_team'] )) {
            update_post_meta( $phd_opening->ID, 'research_team', $_POST['research_team'] );
        }
		/* Save Meta Field: Short Info */
        if ( isset( $_POST['short_info'] )) {
            update_post_meta( $phd_opening->ID, 'short_info', $_POST['short_info'] );
        }
		/* Save Meta Field: PhD Template */
        if ( isset( $_POST['phd_template'] )) {
            update_post_meta( $phd_opening->ID, 'phd_template', $_POST['phd_template'] );
        }
		/* Save Meta Field: Other Info */
        if ( isset( $_POST['phd_opening_info'] )) {
            update_post_meta( $phd_opening->ID, 'phd_opening_info', $_POST['phd_opening_info'] );
        }
    }
}
add_action( 'save_post', 'save_phd_openings_fields', 10, 2 );

?>
