<?php
/*
 * PhDHub CPTs - Shortcodes
 */

defined('ABSPATH') or die;

/*
 * Register shortcode [institution_sc]
 */
function phdhub_institution_shortcodes($atts)
{
	$phdhub = shortcode_atts( array(
		'type' => 'new'
	), $atts );
	
    ob_start();
	if ( $phdhub['type'] == 'new') {
		include dirname(plugin_dir_path(__DIR__)) . '/inc/shortcodes/new-institution-sc.php';
		$output = ob_get_clean();
		return $output;
	}
	else if ( $phdhub['type'] == 'edit') {
		include dirname(plugin_dir_path(__DIR__)) . '/inc/shortcodes/edit-institution-sc.php';
		$output = ob_get_clean();
		return $output;
	}
}
add_shortcode('institution_sc', 'phdhub_institution_shortcodes');


/*
 * Register shortcode [faculty_sc]
 */
function phdhub_faculty_shortcodes($atts)
{
	$phdhub = shortcode_atts( array(
		'type' => 'new'
	), $atts );
	
    ob_start();
	if ( $phdhub['type'] == 'new') {
		include dirname(plugin_dir_path(__DIR__)) . '/inc/shortcodes/new-faculty-sc.php';
		$output = ob_get_clean();
		return $output;
	}
	else if ( $phdhub['type'] == 'edit') {
		include dirname(plugin_dir_path(__DIR__)) . '/inc/shortcodes/edit-faculty-sc.php';
		$output = ob_get_clean();
		return $output;
	}
}
add_shortcode('faculty_sc', 'phdhub_faculty_shortcodes');


/*
 * Register shortcode [phd_program_sc]
 */
function phdhub_phd_program_shortcodes($atts)
{
	$phdhub = shortcode_atts( array(
		'type' => 'new'
	), $atts );
	
    ob_start();
	if ( $phdhub['type'] == 'new') {
		include dirname(plugin_dir_path(__DIR__)) . '/inc/shortcodes/new-phd-program-sc.php';
		$output = ob_get_clean();
		return $output;
	}
	else if ( $phdhub['type'] == 'edit') {
		include dirname(plugin_dir_path(__DIR__)) . '/inc/shortcodes/edit-phd-program-sc.php';
		$output = ob_get_clean();
		return $output;
	}
}
add_shortcode('phd_program_sc', 'phdhub_phd_program_shortcodes');


/*
 * Register shortcode [research_team_sc]
 */
function phdhub_research_team_shortcodes($atts)
{
	$phdhub = shortcode_atts( array(
		'type' => 'new'
	), $atts );
	
    ob_start();
	if ( $phdhub['type'] == 'new') {
		include dirname(plugin_dir_path(__DIR__)) . '/inc/shortcodes/new-research-team-sc.php';
		$output = ob_get_clean();
		return $output;
	}
	else if ( $phdhub['type'] == 'edit') {
		include dirname(plugin_dir_path(__DIR__)) . '/inc/shortcodes/edit-research-team-sc.php';
		$output = ob_get_clean();
		return $output;
	}
}
add_shortcode('research_team_sc', 'phdhub_research_team_shortcodes');


/*
 * Register shortcode [research_team_sc]
 */
function phdhub_company_shortcodes($atts)
{
	$phdhub = shortcode_atts( array(
		'type' => 'new'
	), $atts );
	
    ob_start();
	if ( $phdhub['type'] == 'new') {
		include dirname(plugin_dir_path(__DIR__)) . '/inc/shortcodes/new-company-sc.php';
		$output = ob_get_clean();
		return $output;
	}
	else if ( $phdhub['type'] == 'edit') {
		include dirname(plugin_dir_path(__DIR__)) . '/inc/shortcodes/edit-company-sc.php';
		$output = ob_get_clean();
		return $output;
	}
}
add_shortcode('company_sc', 'phdhub_company_shortcodes');


/*
 * Register shortcode [phd_offer_sc]
 */
function phdhub_phd_offer_shortcodes($atts)
{
	$phdhub = shortcode_atts( array(
		'type' => 'new'
	), $atts );
	
    ob_start();
	if ( $phdhub['type'] == 'new') {
		include dirname(plugin_dir_path(__DIR__)) . '/inc/shortcodes/new-phd-offer-sc.php';
		$output = ob_get_clean();
		return $output;
	}
	else if ( $phdhub['type'] == 'edit') {
		include dirname(plugin_dir_path(__DIR__)) . '/inc/shortcodes/edit-phd-offer-sc.php';
		$output = ob_get_clean();
		return $output;
	}
}
add_shortcode('phd_offer_sc', 'phdhub_phd_offer_shortcodes');


/*
 * Register shortcode [cooperation_call_sc]
 */
function phdhub_cooperation_call_shortcodes($atts)
{
	$phdhub = shortcode_atts( array(
		'type' => 'new'
	), $atts );
	
    ob_start();
	if ( $phdhub['type'] == 'new') {
		include dirname(plugin_dir_path(__DIR__)) . '/inc/shortcodes/new-cooperation-call-sc.php';
		$output = ob_get_clean();
		return $output;
	}
	else if ( $phdhub['type'] == 'edit') {
		include dirname(plugin_dir_path(__DIR__)) . '/inc/shortcodes/edit-cooperation-call-sc.php';
		$output = ob_get_clean();
		return $output;
	}
}
add_shortcode('cooperation_call_sc', 'phdhub_cooperation_call_shortcodes');

?>