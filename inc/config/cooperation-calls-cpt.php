<?php

defined('ABSPATH') or die;

/*
 * Display Custom Meta Fields for Cooperation Calls CPT
 */
function display_cooperation_calls_meta( $calls ) {
	/*
	 * Create the custom meta fields for Cooperation Calls CPT
	 */
	$institution = esc_attr( get_post_meta ($calls->ID, 'institution', true ) );
    $institution_label = esc_attr( get_post_meta( $calls->ID, 'institution_label', true ) );
    $faculty = esc_attr( get_post_meta( $calls->ID, 'faculty', true ) );
    $phd_program = esc_attr( get_post_meta( $calls->ID, 'phd_program', true ) );	 
	$company = esc_attr( get_post_meta ($calls->ID, 'company', true ) );	 
	$company_label = esc_attr( get_post_meta ($calls->ID, 'company_label', true ) );
    $project_coordinator = esc_attr( get_post_meta( $calls->ID, 'project_coordinator', true ) );
    $phd_opening = esc_attr( get_post_meta( $calls->ID, 'phd_opening', true ) );
    $short_info = esc_attr( get_post_meta( $calls->ID, 'short_info', true ) );
    $email_address = esc_attr( get_post_meta( $calls->ID, 'email_address', true ) );
	
	$info = 'call_info';
    $call_info = get_post_meta( $calls->ID, 'call_info', true );
?>
<div class="phd-cpt-box">
	<div class="uk-grid">
		<!-- Start Left Column -->
		<div class="uk-width-1-2">
			<div class="inner-settings-box">
				<input type="hidden" id="phd-id" value="<?php echo $calls->ID; ?>" />
				<!-- Start Meta Field: Company -->
				<p>
					<label><?php echo __('Company', 'phdhub-cpts'); ?></label>
					<input type="text" name="company_label" id="company-autocomplete" value="<?php echo $company_label; ?>" placeholder="<?php echo __('Search by typing company\'s name', 'phdhub-cpts'); ?>..." />
					<input type="hidden" name="company" value="<?php echo $company; ?>" />
				</p>
				<!-- End Meta Field: Company -->
				<p>
					<label><?php echo __('and/or', 'phdhub-cpts'); ?></label>
				</p>
				<!-- Start Meta Field: Institution -->
				<p>
					<label><?php echo __('Institution', 'phdhub-cpts'); ?></label>
					<input type="text" name="institution_label" id="institution-autocomplete" value="<?php echo $institution_label; ?>" placeholder="<?php echo __('Search by typing institution name', 'phdhub-cpts'); ?>..." />
					<input type="hidden" name="institution" value="<?php echo $institution; ?>" />
				</p>
				<!-- End Meta Field: Institution -->
				<!-- Start Meta Field: Faculty -->
				<p>
					<label><?php echo __('Faculty', 'phdhub-cpts'); ?></label>
					<select id="faculty" name="faculty">
						<option value="empty" <?php selected( $faculty, 'empty' ); ?>><?php echo __('Select Faculty'); ?></option>
						<?php institution_faculty_list(); ?>
					</select>
				</p>
				<!-- End Meta Field: Faculty -->
				<!-- Start Meta Field: PhD Program -->
				<p>
					<label><?php echo __('PhD Program', 'phdhub-cpts'); ?></label>
					<select id="phd-program" name="phd_program">
						<option value="empty" <?php selected( $phd_program, 'empty' ); ?>><?php echo __('Select PhD Program'); ?></option>
						<?php institution_phd_programs_list(); ?>
					</select>
				</p>
				<!-- End Meta Field: PhD Program -->
				<!-- Start Meta Field: PhD Opening -->
				<p>
					<label><?php echo __('PhD Opening', 'phdhub-cpts'); ?></label>
					<select id="phd-opening" name="phd_opening">
						<option value="empty" <?php selected( $phd_opening, 'empty' ); ?>><?php echo __('Select PhD Opening'); ?></option>
						<?php  institution_phd_openings_list(); ?>
					</select>
				</p>
				<!-- End Meta Field: PhD Opening -->
				<!-- Start Meta Field: Project Coordinator -->
				<p>
					<label><?php echo __('Project Coordinator', 'phdhub-cpts'); ?></label>
					<input type="text" size="80"  id="supervisor-autocomplete" name="project_coordinator" value="<?php echo $project_coordinator; ?>" placeholder="<?php echo __('Search by typing project coordinator\'s name'); ?>..." />
				</p>
				<!-- End Meta Field: Project Coordinator -->
				<!-- Start Meta Field: Short Info -->
				<p>
					<label><?php echo __('Short Info', 'phdhub-cpts'); ?></label>
					<textarea rows="5" name="short_info"><?php echo $short_info; ?></textarea>
				</p>
				<!-- End Meta Field: Short Info -->
				<!-- Start Meta Field: Email Address -->
				<p>
					<label><?php echo __('Email Address (for notifications about expressions of interest)', 'phdhub-cpts'); ?></label>
					<input type="email" size="80" name="email_address" value="<?php echo $email_address; ?>" />
				</p>
				<!-- End Meta Field: Short Info -->
			</div>
		</div>
		<!-- End Left Column -->
		<!-- Start Right Column -->
		<div class="uk-width-1-2 inner-settings-box-right">
			<!-- Start Meta Field: General Info -->
			<p>
				<label><?php echo __('General Info', 'phdhub-cpts'); ?></label>
			</p>
			<?php wp_editor( html_entity_decode(stripcslashes($call_info)), $info ); ?>
			<!-- End Meta Field: General Info -->
		</div>
		<!-- End Right Column -->
	</div>
</div>
<?php
}



/*
 * Save Custom Meta Fields of Cooperation Calls CPT
 */
function save_cooperation_calls_fields( $call_id, $call ) {
    if ( $call->post_type == 'cooperation-calls' ) {
		/* Save Meta Field: Company */
        if ( isset( $_POST['company'] )) {
            update_post_meta( $call->ID, 'company', $_POST['company'] );
        }
		/* Save Meta Field: Company Label */
        if ( isset( $_POST['company_label'] )) {
            update_post_meta( $call->ID, 'company_label', $_POST['company_label'] );
        }
		/* Save Meta Field: Short Info */
        if ( isset( $_POST['short_info'] )) {
            update_post_meta( $call->ID, 'short_info', $_POST['short_info'] );
        }
		/* Save Meta Field: Institution */
        if ( isset( $_POST['institution'] )) {
            update_post_meta( $call->ID, 'institution', $_POST['institution'] );
        }
		/* Save Meta Field: Institution Label */
        if ( isset( $_POST['institution_label'] )) {
            update_post_meta( $call->ID, 'institution_label', $_POST['institution_label'] );
        }
		/* Save Meta Field: Faculty */
        if ( isset( $_POST['faculty'] )) {
            update_post_meta( $call->ID, 'faculty', $_POST['faculty'] );
        }
		/* Save Meta Field: PhD Program */
        if ( isset( $_POST['phd_program'] )) {
            update_post_meta( $call->ID, 'phd_program', $_POST['phd_program'] );
        }
		/* Save Meta Field: PhD Opening */
        if ( isset( $_POST['phd_opening'] )) {
            update_post_meta( $call->ID, 'phd_opening', $_POST['phd_opening'] );
        }
		/* Save Meta Field: Project Coordinator */
        if ( isset( $_POST['project_coordinator'] )) {
            update_post_meta( $call->ID, 'project_coordinator', $_POST['project_coordinator'] );
        }
		/* Save Meta Field: Call Info */
        if ( isset( $_POST['call_info'] )) {
            update_post_meta( $call->ID, 'call_info', $_POST['call_info'] );
        }
		/* Save Meta Field: Email Address */
        if ( isset( $_POST['email_address'] )) {
            update_post_meta( $call->ID, 'email_address', $_POST['email_address'] );
        }
    }
}
add_action( 'save_post', 'save_cooperation_calls_fields', 10, 2 );

?>