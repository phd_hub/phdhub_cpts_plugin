<?php

defined('ABSPATH') or die;


/*
 * Display Custom Meta Fields for PhD Programs CPT
 */
function display_phd_program_meta( $phd_program ) {
	/*
	 * Create the custom meta fields for PhD Programs CPT
	 */
    $institution = esc_attr( get_post_meta( $phd_program->ID, 'institution', true ) );
    $institution_label = esc_attr( get_post_meta( $phd_program->ID, 'institution_label', true ) );
    $faculty = esc_attr( get_post_meta( $phd_program->ID, 'faculty', true ) );
    $website = esc_attr( get_post_meta( $phd_program->ID, 'website', true ) );
	
	$info = 'phd_program_info';
    $phd_program_info = get_post_meta( $phd_program->ID, 'phd_program_info', true );
?>
<div class="phd-cpt-box">
	<div class="uk-grid">
		<!-- Start Left Column -->
		<div class="uk-width-1-2">
			<div class="inner-settings-box">
				<!-- Start Meta Field: Institution -->
				<input type="hidden" id="phd-id" value="<?php echo $phd_program->ID; ?>" />
				<p>
					<label><?php echo __('Institution', 'phdhub-cpts'); ?></label>
					<input type="text" name="institution_label" id="institution-autocomplete" value="<?php echo $institution_label; ?>" placeholder="<?php echo __('Search by typing institution name', 'phdhub-cpts'); ?>..." required />
					<input type="hidden" name="institution" value="<?php echo $institution; ?>" required />
				</p>
				<!-- End Meta Field: Institution -->
				<!-- Start Meta Field: Faculty -->
				<p>
					<label><?php echo __('Faculty', 'phdhub-cpts'); ?></label>
					<select id="faculty" name="faculty">
						<option value="empty" <?php selected( $faculty, 'empty' ); ?>><?php echo __('Select Faculty'); ?></option>
						<?php institution_faculty_list(); ?>
					</select>
				</p>
				<!-- End Meta Field: Faculty -->
				<!-- Start Meta Field: Website -->
				<p>
					<label><?php echo __('Website', 'phdhub-cpts'); ?></label>
					<input type="text" size="80" name="website" value="<?php echo $website; ?>" />
				</p>
				<!-- End Meta Field: Website -->
			</div>
		</div>
		<!-- End Left Column -->
		<!-- Start Right Column -->
		<div class="uk-width-1-2 inner-settings-box-right">
			<!-- Start Meta Field: General Info -->
			<p>
				<label><?php echo __('General Info', 'phdhub-cpts'); ?></label>
			</p>
			<?php wp_editor( html_entity_decode(stripcslashes($phd_program_info)), $info ); ?>
			<!-- End Meta Field: General Info -->
		</div>
		<!-- End Right Column -->
	</div>
</div>
<?php
}



/*
 * Save Custom Meta Fields of PhD Programs CPT
 */
function save_phd_program_fields( $phd_program_id, $phd_program ) {
    if ( $phd_program->post_type == 'phd-programs' ) {
		/* Save Meta Field: Institution */
        if ( isset( $_POST['institution'] )) {
            update_post_meta( $phd_program->ID, 'institution', $_POST['institution'] );
        }
		/* Save Meta Field: Institution Label */
        if ( isset( $_POST['institution_label'] )) {
            update_post_meta( $phd_program->ID, 'institution_label', $_POST['institution_label'] );
        }
		/* Save Meta Field: Faculty */
        if ( isset( $_POST['faculty'] )) {
            update_post_meta( $phd_program->ID, 'faculty', $_POST['faculty'] );
        }
		/* Save Meta Field: Website */
        if ( isset( $_POST['website'] )) {
            update_post_meta( $phd_program->ID, 'website', $_POST['website'] );
        }
		/* Save Meta Field: General Info */
        if ( isset( $_POST['phd_program_info'] )) {
            update_post_meta( $phd_program->ID, 'phd_program_info', $_POST['phd_program_info'] );
        }
    }
}
add_action( 'save_post', 'save_phd_program_fields', 10, 2 );

?>