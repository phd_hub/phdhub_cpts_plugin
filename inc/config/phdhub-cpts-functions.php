<?php

defined('ABSPATH') or die;

/*
 * Register Page Template
 */
function phdhub_cpts_page_templates($templates) {
    $templates['contact-page.php'] = 'Contact Page';
    $templates['useful-info.php'] = 'Useful Info Page';

    return $templates;
}
add_filter ('theme_page_templates', 'phdhub_cpts_page_templates');

/*
 * Load Page Template
 */
function phdhub_cpts_redirect_page_templates($template) {
	if(  get_page_template_slug() === 'contact-page.php' ) {
        if ( $theme_file = locate_template( array( 'contact-page.php' ) ) ) {
            $template = $theme_file;
        } else {
            $template = dirname(plugin_dir_path(__DIR__)) . '/inc/pages/contact-page.php';
        }
    }

    /* if($template == '') {
        throw new \Exception('No template found');
    } */
    return $template;
}
add_filter ('template_include', 'phdhub_cpts_redirect_page_templates');

function phdhub_contact_page_meta_boxes() {
    global $post;
    if ( 'contact-page.php' == get_post_meta( $post->ID, '_wp_page_template', true ) ) {
        add_meta_box(
			'contact_form_meta',
            'Contact Form Settings',
            'display_contact_page_settings',
            'page',
            'advanced',
            'high'
		);
    }
}
add_action( 'add_meta_boxes_page', 'phdhub_contact_page_meta_boxes' );


function display_contact_page_settings( $page ) {
	$recipient_email = esc_attr( get_post_meta( $page->ID, 'recipient_email', true ) );
?>
<!-- Start Meta Field: Recipient's Email -->
<p class="contact-form-recipient">
	<label><?php echo __('Send emails to', 'phdhub-cpts'); ?></label>
	<input type="email" name="recipient_email" value="<?php echo $recipient_email; ?>" placeholder="<?php echo __('info@example.com', 'phdhub-cpts'); ?>" required />
</p>
<!-- End Meta Field: Recipient's Email -->
<?php
}

/*
 * Save Custom Meta Fields of PhD Openings CPT
 */
function save_contact_page_fields( $page_id, $page ) {
    if ( $page->post_type == 'page' ) {
		/* Save Meta Field: Recipient's Email */
        if ( isset( $_POST['recipient_email'] )) {
            update_post_meta( $page->ID, 'recipient_email', $_POST['recipient_email'] );
        }
    }
}
add_action( 'save_post', 'save_contact_page_fields', 10, 2 );


function phdhub_useful_info_page($template) {
    if(  get_page_template_slug() === 'useful-info.php' ) {
        if ( $theme_file = locate_template( array( 'useful-info.php' ) ) ) {
            $template = $theme_file;
        } else {
            $template = dirname(plugin_dir_path(__DIR__)) . '/inc/pages/useful-info.php';
        }
    }
    return $template;
}
add_filter ('template_include', 'phdhub_useful_info_page');

function phdhub_useful_info_page_meta_boxes() {
    global $post;
    if ( 'useful-info.php' == get_post_meta( $post->ID, '_wp_page_template', true ) ) {
        add_meta_box(
                        'useful_info_meta',
            'Useful Information Settings',
            'display_useful_info_page_settings',
            'page',
            'custom-metabox-holder',
            'high'
                );

    	remove_post_type_support('page', 'editor');
    }
}
add_action( 'add_meta_boxes_page', 'phdhub_useful_info_page_meta_boxes' );


function phdhub_custom_page_section( $post ) {	
	do_meta_boxes( null, 'custom-metabox-holder', $post );
}
add_action( 'edit_form_after_title', 'phdhub_custom_page_section' );


function display_useful_info_page_settings( $page ) {
	$practical = 'practical_info';
    $practical_info = get_post_meta( $page->ID, 'practical_info', true );
	$accomodation = 'accomodation_info';
    $accomodation_info = get_post_meta( $page->ID, 'accomodation_info', true );
	$entry = 'entry_conditions';
    $entry_conditions = get_post_meta( $page->ID, 'entry_conditions', true );
	$banking = 'banking_info';
    $banking_info = get_post_meta( $page->ID, 'banking_info', true );
	$insurance = 'insurance_info';
    $insurance_info = get_post_meta( $page->ID, 'insurance_info', true );
	$taxation = 'taxation_info';
    $taxation_info = get_post_meta( $page->ID, 'taxation_info', true );
	$phd = 'phd_regulations';
    $phd_regulations = get_post_meta( $page->ID, 'phd_regulations', true );
	$recognition = 'diplomas_recognition';
    $diplomas_recognition = get_post_meta( $page->ID, 'diplomas_recognition', true );
?>
<ul class="info-nav" data-uk-switcher="{connect:'#useful-info-nav'}">
    <li><a href="#"><?php echo __('Arriving', 'phdhub-cpts'); ?></a></li>
    <li><a href="#"><?php echo __('Living', 'phdhub-cpts'); ?></a></li>
    <li><a href="#"><?php echo __('Doing a PhD', 'phdhub-cpts'); ?></a></li>
</ul>

<!-- This is the container of the content items -->
<ul id="useful-info-nav" class="uk-switcher">
    <li>
        <div class="useful-info-section">
            <p>
                <label><?php echo __('Practical Info', 'phdhub-cpts'); ?></label>
            </p>
            <?php wp_editor( html_entity_decode(stripcslashes($practical_info)), $practical ); ?>
        </div>
        <div class="useful-info-section">
            <p>
                <label><?php echo __('Accomodation', 'phdhub-cpts'); ?></label>
            </p>
            <?php wp_editor( html_entity_decode(stripcslashes($accomodation_info)), $accomodation ); ?>
        </div>
        <div class="useful-info-section">
            <p>
                <label><?php echo __('Entry Conditions/Visa Specific Info', 'phdhub-cpts'); ?></label>
            </p>
            <?php wp_editor( html_entity_decode(stripcslashes($entry_conditions)), $entry ); ?>
        </div>
    </li>
    <li>
        <div class="useful-info-section">
            <p>
                <label><?php echo __('Banking Info', 'phdhub-cpts'); ?></label>
            </p>
            <?php wp_editor( html_entity_decode(stripcslashes($banking_info)), $banking ); ?>
        </div>
        <div class="useful-info-section">
            <p>
                <label><?php echo __('Insurance/Mediacal Care Info', 'phdhub-cpts'); ?></label>
            </p>
            <?php wp_editor( html_entity_decode(stripcslashes($insurance_info)), $insurance ); ?>
        </div>
        <div class="useful-info-section">
            <p>
                <label><?php echo __('Taxation/Salaries Info', 'phdhub-cpts'); ?></label>
            </p>
            <?php wp_editor( html_entity_decode(stripcslashes($taxation_info)), $taxation ); ?>
        </div>
    </li>
    <li>
        <div class="useful-info-section">
            <p>
                <label><?php echo __('PhD Regulations', 'phdhub-cpts'); ?></label>
            </p>
            <?php wp_editor( html_entity_decode(stripcslashes($phd_regulations)), $phd ); ?>
        </div>
        <div class="useful-info-section">
            <p>
                <label><?php echo __('Recognition of Diplomas', 'phdhub-cpts'); ?></label>
            </p>
            <?php wp_editor( html_entity_decode(stripcslashes($diplomas_recognition)), $recognition ); ?>
        </div>
    </li>
</ul>
<?php
}

/*
 * Save Custom Meta Fields of PhD Openings CPT
 */
function save_useful_info_page_fields( $page_id, $page ) {
    if ( $page->post_type == 'page' ) {
        if ( isset( $_POST['practical_info'] )) {
            update_post_meta( $page->ID, 'practical_info', $_POST['practical_info'] );
        }
        if ( isset( $_POST['accomodation_info'] )) {
            update_post_meta( $page->ID, 'accomodation_info', $_POST['accomodation_info'] );
        }
        if ( isset( $_POST['entry_conditions'] )) {
            update_post_meta( $page->ID, 'entry_conditions', $_POST['entry_conditions'] );
        }
        if ( isset( $_POST['banking_info'] )) {
            update_post_meta( $page->ID, 'banking_info', $_POST['banking_info'] );
        }
        if ( isset( $_POST['insurance_info'] )) {
            update_post_meta( $page->ID, 'insurance_info', $_POST['insurance_info'] );
        }
        if ( isset( $_POST['taxation_info'] )) {
            update_post_meta( $page->ID, 'taxation_info', $_POST['taxation_info'] );
        }
        if ( isset( $_POST['phd_regulations'] )) {
            update_post_meta( $page->ID, 'phd_regulations', $_POST['phd_regulations'] );
        }
        if ( isset( $_POST['diplomas_recognition'] )) {
            update_post_meta( $page->ID, 'diplomas_recognition', $_POST['diplomas_recognition'] );
        }
    }
}
add_action( 'save_post', 'save_useful_info_page_fields', 10, 2 );


/*
 * Get faculties based on selected institution
 */
add_action( 'wp_ajax_nopriv_institution_faculty_list', 'institution_faculty_list' );
add_action( 'wp_ajax_institution_faculty_list', 'institution_faculty_list' );
function institution_faculty_list( ) {
	$phd_id = (isset($_REQUEST['phd_id']) ? $_REQUEST['phd_id'] : null);
	$institution = (isset($_REQUEST['institution']) ? $_REQUEST['institution'] : null);
		//$current_screen = get_current_screen();
		global $pagenow;
		if ($pagenow == 'profile.php') {
			$faculty = get_the_author_meta('faculty', $phd_id );
		}
		else {
			$faculty = get_post_meta( $phd_id, 'faculty', true );
		}
		$args = array(
			'posts_per_page' => -1,
			'post_type' => 'faculties',
			'orderby'=> 'title', 
			'order' => 'ASC',
			'meta_query' => array(
				array(
					'key' => 'institution',
					'value' => $institution,
					'compare' => '='
				)
			)
		);
		$faculties = get_posts($args);
?>
<option value="empty" <?php selected( $faculty, 'empty'); ?>><?php echo __('Select Faculty', 'phdhub-cpts'); ?></option>
<?php
		foreach ($faculties as $faculty_item) {
?>
		<option value="<?php echo $faculty_item->ID; ?>" <?php selected( $faculty, $faculty_item->ID ); ?>><a href="<?php echo get_permalink($faculty_item->ID); ?>"><?php echo $faculty_item->post_title; ?></a></option>
<?php
		}
}


/*
 * Get phd programs based on selected institution
 */
add_action( 'wp_ajax_nopriv_institution_phd_programs_list', 'institution_phd_programs_list' );
add_action( 'wp_ajax_institution_phd_programs_list', 'institution_phd_programs_list' );
function institution_phd_programs_list( ) {
	$phd_id = (isset($_REQUEST['phd_id']) ? $_REQUEST['phd_id'] : null);
	$institution = (isset($_REQUEST['institution']) ? $_REQUEST['institution'] : null);
    	$phd_program = get_post_meta( $phd_id, 'phd_program', true );
		$args = array(
			'posts_per_page' => -1,
			'post_type' => 'phd-programs',
			'orderby'=> 'title', 
			'order' => 'ASC',
			'meta_query' => array(
				array(
					'key' => 'institution',
					'value' => $institution,
					'compare' => '='
				)
			)
		);
		$phd_programs = get_posts($args);
?>
<option value="empty" <?php selected( $phd_program, 'empty'); ?>><?php echo __('Select PhD Program', 'phdhub-cpts'); ?></option>
<?php
		foreach ($phd_programs as $phd_program_item) {
?>
		<option value="<?php echo $phd_program_item->ID; ?>" <?php selected( $phd_program, $phd_program_item->ID ); ?>><?php echo $phd_program_item->post_title; ?></option>
<?php
		}
}


/*
 * Get faculties based on selected institution
 */
add_action( 'wp_ajax_nopriv_institution_phd_openings_list', 'institution_phd_openings_list' );
add_action( 'wp_ajax_institution_phd_openings_list', 'institution_phd_openings_list' );
function institution_phd_openings_list( ) {
	$phd_id = (isset($_REQUEST['phd_id']) ? $_REQUEST['phd_id'] : null);
	$institution = (isset($_REQUEST['institution']) ? $_REQUEST['institution'] : null);
    	$phd_opening = get_post_meta( $phd_id, 'phd_opening', true );
		$args = array(
			'posts_per_page' => -1,
			'post_type' => 'phd-openings',
			'orderby'=> 'title', 
			'order' => 'ASC',
			'meta_query' => array(
				array(
					'key' => 'institution',
					'value' => $institution,
					'compare' => '='
				)
			)
		);
		$phd_openings = get_posts($args);
?>
<option value="empty" <?php selected( $phd_opening, 'empty' ); ?>><?php echo __('Select PhD Opening'); ?></option>
<?php	
		foreach ($phd_openings as $phd_opening_item) {
?>
		<option value="<?php echo $phd_opening_item->ID; ?>" <?php selected( $phd_opening, $phd_opening_item->ID ); ?>><?php echo $phd_opening_item->post_title; ?></option>
<?php
		}
}

/*
 * Get research teams based on selected institution
 */
add_action( 'wp_ajax_nopriv_institution_research_teams_list', 'institution_research_teams_list' );
add_action( 'wp_ajax_institution_research_teams_list', 'institution_research_teams_list' );
function institution_research_teams_list( ) {
	$phd_id = (isset($_REQUEST['phd_id']) ? $_REQUEST['phd_id'] : null);
	$institution = (isset($_REQUEST['institution']) ? $_REQUEST['institution'] : null);
    	$research_team = get_post_meta( $phd_id, 'research_team', true );
		$args = array(
			'posts_per_page' => -1,
			'post_type' => 'research-teams',
			'orderby'=> 'title', 
			'order' => 'ASC',
			'meta_query' => array(
				array(
					'key' => 'institution',
					'value' => $institution,
					'compare' => '='
				)
			)
		);
		$research_teams = get_posts($args);
	
?>
<option value="empty" <?php selected( $research_team, 'empty'); ?>><?php echo __('Select Research Team', 'phdhub-cpts'); ?></option>
<?php
		foreach ($research_teams as $research_team_item) {
?>
		<option value="<?php echo $research_team_item->ID; ?>" <?php selected( $research_team, $research_team_item->ID ); ?>><?php echo $research_team_item->post_title; ?></option>
<?php
		}
}


/* Custom Filters for PhDHub CPTs */
add_filter( 'phdhub_cpt_content', 'wptexturize'       );
add_filter( 'phdhub_cpt_content', 'convert_smilies'   );
add_filter( 'phdhub_cpt_content', 'convert_chars'     );
add_filter( 'phdhub_cpt_content', 'wpautop'           );
add_filter( 'phdhub_cpt_content', 'shortcode_unautop' );
add_filter( 'phdhub_cpt_content', 'do_shortcode'      );

/*
 * Set the file for every CPT's single page
 */
function get_phdhub_single_page_template($single_template) {
     global $post;

	 /* Set the file for Institution's single page */
     if ($post->post_type == 'institutions') {
          $single_template = dirname(plugin_dir_path(__DIR__)) . '/inc/pages/single-institutions.php';
     }
	 /* Set the file for Faculty's single page */
	 else if ($post->post_type == 'faculties') {
          $single_template = dirname(plugin_dir_path(__DIR__)) . '/inc/pages/single-faculties.php';
     }
	 /* Set the file for PhD Program's single page */
	 else if ($post->post_type == 'phd-programs') {
          $single_template = dirname(plugin_dir_path(__DIR__)) . '/inc/pages/single-phd-programs.php';
     }
	 /* Set the file for PhD Opening's single page */
	 else if ($post->post_type == 'phd-openings') {
          $single_template = dirname(plugin_dir_path(__DIR__)) . '/inc/pages/single-phd-openings.php';
     }
	 /* Set the file for Research Team's single page */
	 else if ($post->post_type == 'research-teams') {
          $single_template = dirname(plugin_dir_path(__DIR__)) . '/inc/pages/single-research-teams.php';
     }
	 /* Set the file for Company's single page */
	 else if ($post->post_type == 'companies') {
          $single_template = dirname(plugin_dir_path(__DIR__)) . '/inc/pages/single-companies.php';
     }
	 /* Set the file for Call for Cooperation's single page */
	 else if ($post->post_type == 'cooperation-calls') {
          $single_template = dirname(plugin_dir_path(__DIR__)) . '/inc/pages/single-cooperation-calls.php';
     }
     return $single_template;
}
add_filter( 'single_template', 'get_phdhub_single_page_template' );


/*
 * Define the file for phd opening's category / archive page
 */
function get_phdhub_archive_page_template( $archive_template ){	
	global $post;
	
	if( isset( $post ) ) {
		if( is_post_type_archive( 'phd-openings' )  || (($post->post_type == 'phd-openings') && (is_archive('phd-openings'))) ) {
			$archive_template = dirname(plugin_dir_path(__DIR__)) . '/inc/archives/archive-phd-openings.php';
		}
		else if( is_post_type_archive( 'institutions' ) || (($post->post_type == 'institutions') && (is_archive('institutions'))) ) {
			$archive_template = dirname(plugin_dir_path(__DIR__)) . '/inc/archives/archive-institutions.php';
		}
		else if( is_post_type_archive( 'companies' ) || (($post->post_type == 'companies') && (is_archive('companies'))) ) {
			$archive_template = dirname(plugin_dir_path(__DIR__)) . '/inc/archives/archive-companies.php';
		}
		else if( is_post_type_archive( 'cooperation-calls' ) || (($post->post_type == 'cooperation-calls') && (is_archive('cooperation-calls'))) ) {
			$archive_template = dirname(plugin_dir_path(__DIR__)) . '/inc/archives/archive-cooperation-calls.php';
		}
	}
	else {
		if( is_post_type_archive( 'phd-openings' ) ) {
			$archive_template = dirname(plugin_dir_path(__DIR__)) . '/inc/archives/archive-phd-openings.php';
		}
		else if( is_post_type_archive( 'institutions' ) ) {
			$archive_template = dirname(plugin_dir_path(__DIR__)) . '/inc/archives/archive-institutions.php';
		}
		else if( is_post_type_archive( 'companies' ) ) {
			$archive_template = dirname(plugin_dir_path(__DIR__)) . '/inc/archives/archive-companies.php';
		}
		else if( is_post_type_archive( 'cooperation-calls' ) ) {
			$archive_template = dirname(plugin_dir_path(__DIR__)) . '/inc/archives/archive-cooperation-calls.php';
		}
	}
    return $archive_template;
}
add_filter( 'archive_template', 'get_phdhub_archive_page_template' );



/* Create autocomplete field for institution */
function institution_autocomplete() {
	$args = array(
		'post_type' => 'institutions',
		'post_status' => 'publish',
		'posts_per_page'   => -1 // all posts
	);
 
	$posts = get_posts( $args );
 
		if( $posts ) :
			foreach( $posts as $k => $post ) {
				$source[$k]['ID'] = $post->ID;
				$source[$k]['label'] = $post->post_title;
				$source[$k]['value'] = $post->post_title;
				$source[$k]['saved_value'] = $post->post_name;
			}
 
	?>
	<script type="text/javascript">
		jQuery(document).ready(function($){
			var posts = <?php echo json_encode( array_values( $source ) ); ?>;
 
			jQuery( 'input[name="institution_label"]' ).autocomplete({
		        source: posts,
		        minLength: 2,
		        select: function(event, ui) {
					$('input[name="institution"]').val(ui.item.saved_value);
		        }
		    });
		});    
		
	</script>
	<?php
	endif;
}
add_action( 'admin_footer', 'institution_autocomplete', 100 );
add_action( 'wp_footer', 'institution_autocomplete' );

/* Create autocomplete field for company */
function company_autocomplete() {
	$args = array(
		'post_type' => 'companies',
		'post_status' => 'publish',
		'posts_per_page'   => -1 // all posts
	);
 
	$posts = get_posts( $args );
 
		if( $posts ) :
			foreach( $posts as $k => $post ) {
				$source[$k]['ID'] = $post->ID;
				$source[$k]['label'] = $post->post_title;
				$source[$k]['value'] = $post->post_title;
				$source[$k]['saved_value'] = $post->post_name;
			}
 
	?>
	<script type="text/javascript">
		jQuery(document).ready(function($){
			var posts = <?php echo json_encode( array_values( $source ) ); ?>;
 
			jQuery( 'input[name="company_label"]' ).autocomplete({
		        source: posts,
		        minLength: 2,
		        select: function(event, ui) {
					$('input[name="company"]').val(ui.item.saved_value);
		        }
		    });
		});    
		
	</script>
	<?php
	endif;
}
add_action( 'admin_footer', 'company_autocomplete' );
add_action( 'wp_footer', 'company_autocomplete' );


/* Create autocomplete field for supervisor */
function supervisor_autocomple() {
	$args = array(
		'role__in' => array( 'lead-researcher', 'assist-researcher', 'company-rep' )
	);
 
	$users = get_users( $args );
 
		if( $users ) :
			foreach( $users as $k => $user ) {
				$source[$k]['ID'] = $user->ID;
				$source[$k]['label'] = $user->last_name . ' ' . $user->first_name;
				$source[$k]['value'] = $user->last_name . ' ' . $user->first_name;
				$source[$k]['saved_value'] = $user->user_login;
			}
 
	?>
	<script type="text/javascript">
		jQuery(document).ready(function($){
			var posts = <?php echo json_encode( array_values( $source ) ); ?>;
 			
			jQuery( 'input[name="thesis_supervisor_label"]' ).autocomplete({
		        source: posts,
		        minLength: 2,
		        select: function(event, ui) {
					$('input[name="thesis_supervisor"]').val(ui.item.saved_value);
		        },
				response: function(event, ui) {
					if (!ui.content.length) {
						var noResult = { value:"",label:"No results found" };
						ui.content.push(noResult);
					}
				}
		    });
		});    
		jQuery(document).ready(function($){
			var posts = <?php echo json_encode( array_values( $source ) ); ?>;
 
			jQuery( 'input[name="project_coordinator"]' ).autocomplete({
		        source: posts,
		        minLength: 2,
		        select: function(event, ui) {
		        },
				response: function(event, ui) {
					if (!ui.content.length) {
						var noResult = { value:"",label:"No results found" };
						ui.content.push(noResult);
					}
				}
		    });
		});    
		
	</script>
	<?php
	endif;
}
add_action( 'admin_footer', 'supervisor_autocomple' );
add_action( 'wp_footer', 'supervisor_autocomple' );


/* Define which custom post types will be multilingual */
add_filter( 'pll_get_post_types', 'add_cpt_to_pll', 10, 2 );
function add_cpt_to_pll( $post_types, $is_settings ) {
    if ( $is_settings ) {
        unset( $post_types['institutions'] );
        unset( $post_types['faculties'] );
        unset( $post_types['phd-openings'] );
        unset( $post_types['phd-programs'] );
        unset( $post_types['phd-templates'] );
        unset( $post_types['cooperation-calls'] );
        unset( $post_types['companies'] );
        unset( $post_types['research-teams'] );
    } else {
        $post_types['institutions'] = 'institutions';
        $post_types['faculties'] = 'faculties';
        $post_types['phd-openings'] = 'phd-openings';
        $post_types['phd-programs'] = 'phd-programs';
        $post_types['phd-templates'] = 'phd-templates';
        $post_types['cooperation-calls'] = 'cooperation-calls';
        $post_types['companies'] = 'companies';
        $post_types['research-teams'] = 'research-teams';
    }
    return $post_types;
}

/* Define which custom taxonomies will be multilingual */
add_filter( 'pll_get_taxonomies', 'add_tax_to_pll', 10, 2 );
function add_tax_to_pll( $taxonomies, $is_settings ) {
    if ( $is_settings ) {
        unset( $taxonomies['fields-of-science'] );
        unset( $taxonomies['research-areas'] );
        unset( $taxonomies['regions'] );
        unset( $taxonomies['tags'] );
    } else {
        $taxonomies['fields-of-science'] = 'fields-of-science';
        $taxonomies['regions'] = 'regions';
        $taxonomies['tags'] = 'tags';
    }
    return $taxonomies;
}


/*
 * Register required plugins in order for this plugin to work correctly
 */
add_action( 'tgmpa_register', 'phdhub_cpts_register_required_plugins' );
function phdhub_cpts_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(

		// Include plugins from the WordPress Plugin Repository.
		array(
			'name'      => 'Polylang',
			'slug'      => 'polylang',
			'required'  => true,
			'force_activation' => true,
		),
		array(
			'name'      => 'User Role Editor',
			'slug'      => 'user-role-editor',
			'required'  => true,
			'force_activation' => true,
		),
		array(
			'name'      => 'WP Admin Category Search',
			'slug'      => 'admin-category-search',
			'required'  => true,
			'force_activation' => true,
		),
		array(
			'name'      => 'Favorites',
			'slug'      => 'favorites',
			'required'  => true,
			'force_activation' => true,
		),
		array(
			'name'      => 'All In One WP Security & Firewall',
			'slug'      => 'all-in-one-wp-security-and-firewall',
			'required'  => true,
			'force_activation' => true,
		),
		array(
			'name'      => 'OpenID',
			'slug'      => 'openid',
			'required'  => true,
			'force_activation' => true,
		),
		array(
			'name'      => 'WordPress Social Share, Social Login and Social Comments Plugin – Super Socializer',
			'slug'      => 'super-socializer',
			'required'  => true,
			'force_activation' => true,
		),
		array(
			'name'      => 'Two Factor Authentication',
			'slug'      => 'two-factor-authentication',
			'required'  => true,
			'force_activation' => true,
		),
		array(
			'name'      => 'Delete Me',
			'slug'      => 'delete-me',
			'required'  => true,
			'force_activation' => true,
		),
		array(
			'name'      => 'GDPR Data Request Form',
			'slug'      => 'gdpr-data-request-form',
			'required'  => true,
			'force_activation' => true,
		),
		array(
			'name'      => 'WP User Avatar',
			'slug'      => 'wp-user-avatar',
			'required'  => true,
			'force_activation' => true,
		),
		array(
			'name'      => 'Google XML Sitemaps',
			'slug'      => 'google-sitemap-generator',
			'required'  => true,
			'force_activation' => true,
		),
		array(
			'name'      => 'Expire Passwords',
			'slug'      => 'expire-passwords',
			'required'  => true,
			'force_activation' => true,
		),

	);

	/*
	 * Array of configuration settings. Amend each line as needed.
	 *
	 * TGMPA will start providing localized text strings soon. If you already have translations of our standard
	 * strings available, please help us make TGMPA even better by giving us access to these translations or by
	 * sending in a pull-request with .po file(s) with the translations.
	 *
	 * Only uncomment the strings in the config array if you want to customize the strings.
	 */
	$config = array(
		'id'           => 'phdhub-cpts',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'plugins.php',            // Parent menu slug.
		'capability'   => 'manage_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => false,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => true,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.

		/*
		'strings'      => array(
			'page_title'                      => __( 'Install Required Plugins', 'phdhub-cpts' ),
			'menu_title'                      => __( 'Install Plugins', 'phdhub-cpts' ),
			/* translators: %s: plugin name. * /
			'installing'                      => __( 'Installing Plugin: %s', 'phdhub-cpts' ),
			/* translators: %s: plugin name. * /
			'updating'                        => __( 'Updating Plugin: %s', 'phdhub-cpts' ),
			'oops'                            => __( 'Something went wrong with the plugin API.', 'phdhub-cpts' ),
			'notice_can_install_required'     => _n_noop(
				/* translators: 1: plugin name(s). * /
				'This theme requires the following plugin: %1$s.',
				'This theme requires the following plugins: %1$s.',
				'phdhub-cpts'
			),
			'notice_can_install_recommended'  => _n_noop(
				/* translators: 1: plugin name(s). * /
				'This theme recommends the following plugin: %1$s.',
				'This theme recommends the following plugins: %1$s.',
				'phdhub-cpts'
			),
			'notice_ask_to_update'            => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
				'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
				'phdhub-cpts'
			),
			'notice_ask_to_update_maybe'      => _n_noop(
				/* translators: 1: plugin name(s). * /
				'There is an update available for: %1$s.',
				'There are updates available for the following plugins: %1$s.',
				'phdhub-cpts'
			),
			'notice_can_activate_required'    => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following required plugin is currently inactive: %1$s.',
				'The following required plugins are currently inactive: %1$s.',
				'phdhub-cpts'
			),
			'notice_can_activate_recommended' => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following recommended plugin is currently inactive: %1$s.',
				'The following recommended plugins are currently inactive: %1$s.',
				'phdhub-cpts'
			),
			'install_link'                    => _n_noop(
				'Begin installing plugin',
				'Begin installing plugins',
				'phdhub-cpts'
			),
			'update_link' 					  => _n_noop(
				'Begin updating plugin',
				'Begin updating plugins',
				'phdhub-cpts'
			),
			'activate_link'                   => _n_noop(
				'Begin activating plugin',
				'Begin activating plugins',
				'phdhub-cpts'
			),
			'return'                          => __( 'Return to Required Plugins Installer', 'phdhub-cpts' ),
			'plugin_activated'                => __( 'Plugin activated successfully.', 'phdhub-cpts' ),
			'activated_successfully'          => __( 'The following plugin was activated successfully:', 'phdhub-cpts' ),
			/* translators: 1: plugin name. * /
			'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'phdhub-cpts' ),
			/* translators: 1: plugin name. * /
			'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'phdhub-cpts' ),
			/* translators: 1: dashboard link. * /
			'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'phdhub-cpts' ),
			'dismiss'                         => __( 'Dismiss this notice', 'phdhub-cpts' ),
			'notice_cannot_install_activate'  => __( 'There are one or more required or recommended plugins to install, update or activate.', 'phdhub-cpts' ),
			'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'phdhub-cpts' ),

			'nag_type'                        => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
		),
		*/
	);

	tgmpa( $plugins, $config );
}



/* Get Institutions Shortcode */
function phdhub_partners_shortcode($atts)
{
	$att = shortcode_atts( array(
		'title' => '',
    ), $atts );
	
    ob_start();
?>
<div id="institution-archive">
	<ul id="partner-filters" class="uk-subnav">
    	<li data-uk-filter=""><a href=""><?php echo __('All Partners', 'phdhub-cpts'); ?></a></li>
    	<li data-uk-filter="institutions"><a href=""><?php echo __('Institutions', 'phdhub-cpts'); ?></a></li>
    	<li data-uk-filter="companies"><a href=""><?php echo __('Enterprises', 'phdhub-cpts'); ?></a></li>
	</ul>
	<div data-uk-grid="{gutter: 20, controls: '#partner-filters'}">
<?php
    $blog_ids = wp_get_sites();
    foreach ($blog_ids as $key=>$current_blog) {
        // switch to each blog to get the posts
        switch_to_blog($current_blog['blog_id']);
        // fetch all the posts 
        $partners = get_posts(array( 'posts_per_page' => -1, 'post_type' => array('institutions', 'companies')));
?>
<?php
       foreach ($partners as $partner) {
       		$type = $partner->post_type;
			$city = esc_attr( get_post_meta( $partner->ID, 'city', true ) );
			$country = esc_attr( get_post_meta( $partner->ID, 'country', true ) );
?>
	<div class="uk-width-1-4" data-uk-filter="<?php echo $type; ?>">
		<div class="institution-item">
				<a href="<?php echo get_permalink( $partner->ID ); ?>">
					<?php
						/*
						 * Display institutions's thumbnail
						 */
						echo get_the_post_thumbnail($partner->ID, 'medium');
					?>
				</a>
				<p class="title">
						<a href="<?php echo get_permalink( $partner->ID ); ?>"><?php echo $partner->post_title; ?></a>
				</p>
				<?php
					if ( (!empty ($city)) && (!empty ($country)) ) {
				?>
				<p class="location">
					<?php echo $city . ', ' . $country; ?>
				</p>
				<?php
					}
				?>
		</div>
	</div>
<?php
       }
?>
<?php
        restore_current_blog();
    }
?>
	</div>
</div>
<?php
	$output = ob_get_clean();
	return $output;
}
add_shortcode('phdhub_partners', 'phdhub_partners_shortcode');



/* Get Calls for Cooperation Shortcode */
function phdhub_cooperation_calls_shortcode($atts)
{
	$att = shortcode_atts( array(
		'title' => '',
    ), $atts );
	
    ob_start();
?>
<div id="calls-archive-new">
    <style>
        article.page .entry-content {
            background: transparent;
            border-radius: 2px;
            box-shadow: none;
            padding: 0;
        }
    </style>
	<div data-uk-grid="{gutter: 20}">
<?php
    $blog_ids = wp_get_sites();
    foreach ($blog_ids as $key=>$current_blog) {
        // switch to each blog to get the posts
        switch_to_blog($current_blog['blog_id']);
        // fetch all the posts 
        
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

        $args = array( 'posts_per_page' => 15, 'post_type' => 'cooperation-calls', 'paged' => $paged );
        $call = new WP_Query( $args );

        if ( $call->have_posts() ) :
            while ( $call->have_posts() ) : $call->the_post(); 
                $call_id = get_the_ID();
                $short_info = esc_attr( get_post_meta( $call_id, 'short_info', true ) );
                $institution = esc_attr( get_post_meta( $call_id, 'institution', true ) );
                if ($institution) {
	                $institution_details = get_page_by_path($institution, '', 'institutions');
                	$institution_logo = esc_url( get_post_meta( $institution_details->ID, 'institution_logo', true ) );
                	$institution_city = esc_attr( get_post_meta( $institution_details->ID, 'city', true ) );
                	$institution_country = esc_attr( get_post_meta( $institution_details->ID, 'country', true ) );
                }
                $company = esc_attr( get_post_meta( $call_id, 'company', true ) );
                if ($company) {
               		$company_details = get_page_by_path($company, '', 'companies');
                	$company_logo = esc_url( get_post_meta( $company_details->ID, 'company_logo', true ) );
                	$company_city = esc_attr( get_post_meta( $company_details->ID, 'city', true ) );
                	$company_country = esc_attr( get_post_meta( $company_details->ID, 'country', true ) );
                }
?>
	<div class="uk-width-1-3">
		<div class="call-item">
			<p class="title">
				<a href="<?php echo get_permalink( $call_id ); ?>"><?php echo get_the_title($call_id); ?></a>
            </p>
            <?php
                if (!empty ($short_info)) {
            ?>
            <p class="short-info">
                <?php echo $short_info; ?>
            </p>
            <?php
				}
            ?>
            <p class="date">
                <i class="fa fa-calendar"></i>
                <?php the_date('d F Y'); ?>
            </p>
            <p class="call-info">
                <span>
                    <?php
				        // Institution's Logo
				        if ( $institution_logo != NULL ) {
                    ?>
				    <a href="<?php echo get_permalink( $institution_details ); ?>"><img src="<?php echo $institution_logo; ?>" alt="Logo"></a>
				    <?php
				        }
				    ?>
				    <a href="<?php echo get_permalink( $institution_details ); ?>">
				        <?php echo $institution_details->post_title; ?>
				        <span><?php echo $institution_city . ', ' . $institution_country; ?></span>
				    </a>
				</span>
				<span>
				    <?php
				        // Company's Logo
				        if ($company) {
				        	if ( $company_logo != NULL ) {
				    ?>
				    <a href="<?php echo get_permalink( $company_details ); ?>"><img src="<?php echo $company_logo; ?>" alt="Logo"></a>
				    <?php
				    		}
				        }
                        if (!empty ($company_city) || !empty ($company_country)) {
				    ?>
				    <a href="<?php echo get_permalink( $company_details ); ?>">
				        <?php echo $company_details->post_title; ?>
				        <span><?php echo $company_city . ', ' . $company_country; ?></span>
				    </a>
                    <?php
                        }
                    ?>
				</span>
            </p>
		</div>
	</div>
    <?php
        endwhile;
    ?>
    <div class="num-pagination">
        <?php
            global $wp_query;

            $big = 999999999;

            echo paginate_links( array(
				'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format' => '?paged=%#%',
				'current' => max( 1, get_query_var('paged') ),
				'total' => $wp_query->max_num_pages
            ) );
        ?>
    </div>
    <?php
        wp_reset_postdata();
        endif;
    ?>
    <?php
        restore_current_blog();
    }
?>
	</div>
</div>
<?php
	$output = ob_get_clean();
    
    $user_is_member = 0;
    $subsites = get_sites();
    if ( ! empty ( $subsites ) ) {
        foreach( $subsites as $subsite ) {
            $subsite_id = get_object_vars( $subsite )["blog_id"];
            $user_id = get_current_user_id();
            if ( is_user_member_of_blog( $user_id, $subsite_id ) ) {
				$user_is_member = 1;
            }
        }
    }
    if ( $user_is_member == 1) {
	   return $output;
    } else {
        echo __('Only a member of the local hubs can view the content of this page!', 'phdhub-cpts');
    }
}
add_shortcode('phdhub_calls', 'phdhub_cooperation_calls_shortcode');




/* Get PhD Offers Shortcode */
function phdhub_offers_shortcode($atts)
{
	$att = shortcode_atts( array(
		'title' => '',
    ), $atts );
	
    ob_start();
?>
<div id="phd-opening-archive-new">
	
	<?php
		$blog_id = get_current_blog_id();
		if ($blog_id != '1') {
	?>
		<div class="uk-grid" style="margin-bottom: 40px;">
			<div class="uk-width-1-2">
			</div>
			<div class="uk-width-1-2 offers-filters">
				<a href="<?php echo site_url(); ?>/phd-openings"><?php echo __('Local Offers', 'phdhub-cpts'); ?></a>
				<a class="current-offers-page" href="<?php echo site_url(); ?>/phd-offers"><?php echo __('All Offers', 'phdhub-cpts'); ?></a>
			</div>
		</div>
	<?php
		}
	?>
	
    <style>
        article.page .entry-content {
            background: transparent;
            border-radius: 2px;
            box-shadow: none;
            padding: 0;
        }
    </style>
	<div data-uk-grid="{gutter: 20}">
<?php
    $blog_ids = wp_get_sites();
    foreach ($blog_ids as $key=>$current_blog) {
        // switch to each blog to get the posts
        switch_to_blog($current_blog['blog_id']);
        // fetch all the posts 
        
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

        $args = array( 'posts_per_page' => -1, 'post_type' => 'phd-openings', 'paged' => $paged );
        $offer = new WP_Query( $args );

        if ( $offer->have_posts() ) :
            while ( $offer->have_posts() ) : $offer->the_post(); 
                $post_id = get_the_ID();
                $short_info = substr( get_post_meta( $post_id, 'phd_opening_info', true ), 0, 200 );
                $institution = esc_attr( get_post_meta( $post_id, 'institution', true ) );
                $institution_details = get_page_by_path($institution, '', 'institutions');
                $institution_logo = esc_url( get_post_meta( $institution_details->ID, 'institution_logo', true ) );
                $institution_city = esc_attr( get_post_meta( $institution_details->ID, 'city', true ) );
                $institution_country = esc_attr( get_post_meta( $institution_details->ID, 'country', true ) );
                $terms = get_the_terms( $post_id , 'fields-of-science' );
?>
	<div class="uk-width-1-3">
		<div class="phd-opening-item">
			<p class="title">
				<a href="<?php echo get_permalink( $post_id ); ?>"><?php echo get_the_title($post_id); ?></a>
            </p>
            <?php
				if (!empty ($short_info)) {
            ?>
            <p class="short-info">
                <?php echo strip_tags($short_info) . '...'; ?>
            </p>
            <?php
				}
                if ($terms) {
            ?>
            <p class="field-of-science">
				<i class="fa fa-folder-o"></i>
                <?php
                    foreach ( $terms as $term) {
                ?>
                <a href="<?php echo get_term_link($term->term_id); ?>"><?php echo $term->name; ?></a>
                <?php
				    }
                ?>
            </p>
            <?php
                }
            ?>
            <p class="date">
				<i class="fa fa-calendar"></i>
				<?php echo get_the_date('d F Y'); ?>
            </p>
            <p class=" phd-opening-info">
				<?php
				    // PhD Opening's Image
				    if ( $institution_logo != NULL ) {
                ?>
				<a href="<?php echo get_permalink( $institution_details ); ?>"><img src="<?php echo $institution_logo; ?>" alt="Logo"></a>
				<?php
				    }
				?>
				<a href="<?php echo get_permalink( $institution_details ); ?>">
					<?php echo $institution_details->post_title; ?>
				    <span><?php echo $institution_city . ', ' . $institution_country; ?></span>
				</a>
            </p>
		</div>
	</div>
    <?php
        endwhile;
        wp_reset_postdata();
        endif;
    ?>
    <?php
    	    restore_current_blog();
   		}
	?>
	</div>
	<div class="num-pagination">
        <?php
        	
            global $wp_query;

            $big = 999999999;

            echo paginate_links( array(
				'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format' => '?paged=%#%',
				'current' => max( 1, get_query_var('paged') ),
				'total' => $wp_query->max_num_pages
            ) );
        ?>
    </div>
</div>
<?php
	$output = ob_get_clean();
	return $output;
}
add_shortcode('phdhub_offers', 'phdhub_offers_shortcode');




function phdhub_local_hubs_shortcode( $atts ) {
	$att = shortcode_atts( array(
		'title' => '',
    ), $atts );
	
    ob_start();
	$subsites = get_sites();
	if ( ! empty ( $subsites ) ) {
		echo '<ul class="subsites-list">';
		foreach( $subsites as $subsite ) {
			$subsite_id = get_object_vars( $subsite )["blog_id"];
			$subsite_name = get_blog_details( $subsite_id )->blogname;
			$subsite_link = get_blog_details( $subsite_id )->siteurl;
			$central_hub_label = '';
			if ($subsite_id == 1) {
				$central_hub_label = ' - (' . __('Central Hub', 'phdhub-cpts') . ')'; 
			}
			echo '<li class="site-' . $subsite_id . '"><a href="' . $subsite_link . '">' . $subsite_name . '</a>' . $central_hub_label . '</li>';
		}
		echo '</ul>';
	}
	$output = ob_get_clean();
	return $output;
}
add_shortcode('phdhub_local_hubs', 'phdhub_local_hubs_shortcode');
