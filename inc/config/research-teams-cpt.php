<?php

defined('ABSPATH') or die;

/*
 * Display Custom Meta Fields for Research Teams CPT
 */
function display_research_teams_meta( $team ) {
	/*
	 * Create the custom meta fields for Research Teams CPT
	 */
    $logo = esc_attr( get_post_meta( $team->ID, 'research_team_logo', true ) );
    $institution = esc_attr( get_post_meta( $team->ID, 'institution', true ) );
    $institution_label = esc_attr( get_post_meta( $team->ID, 'institution_label', true ) );
    $faculty = esc_attr( get_post_meta( $team->ID, 'faculty', true ) );
    $phd_program = esc_attr( get_post_meta( $team->ID, 'phd_program', true ) );
    $website = esc_attr( get_post_meta( $team->ID, 'website', true ) );
	
	$info = 'team_info';
    $team_info = get_post_meta( $team->ID, 'team_info', true );
?>
<div class="phd-cpt-box">
	<div class="uk-grid">
		<!-- Start Left Column -->
		<div class="uk-width-1-2">
			<div class="inner-settings-box">
				<input type="hidden" id="phd-id" value="<?php echo $team->ID; ?>" />
				<!-- Start Meta Field: Logo -->
				<p>
					<label for="research_team_logo">
						<input id="research_team_logo" type="text" size="100" name="research_team_logo" value="<?php echo $logo; ?>" placeholder="<?php echo __('Logo', 'phdhub-cpts'); ?>" /><input id="research_team_logo_button" class="button" type="button" value="<?php echo __('Upload', 'phdhub-cpts'); ?>" />
					</label>
				</p>
				<!-- End Meta Field: Logo -->
				<!-- Start Meta Field: Institution -->
				<p>
					<label><?php echo __('Institution', 'phdhub-cpts'); ?></label>
					<input type="text" name="institution_label" id="institution-autocomplete" value="<?php echo $institution_label; ?>" placeholder="<?php echo __('Search by typing institution name', 'phdhub-cpts'); ?>..." required />
					<input type="hidden" name="institution" value="<?php echo $institution; ?>" required />
				</p>
				<!-- End Meta Field: Institution -->
				<!-- Start Meta Field: Faculty -->
				<p>
					<label><?php echo __('Faculty', 'phdhub-cpts'); ?></label>
					<select id="faculty" name="faculty">
						<option value="empty" <?php selected( $faculty, 'empty' ); ?>><?php echo __('Select Faculty'); ?></option>
						<?php institution_faculty_list(); ?>
					</select>
				</p>
				<!-- End Meta Field: Faculty -->
				<!-- Start Meta Field: PhD Program -->
				<p>
					<label><?php echo __('PhD Program', 'phdhub-cpts'); ?></label>
					<select id="phd-program" name="phd_program">
						<option value="empty" <?php selected( $phd_program, 'empty' ); ?>><?php echo __('Select PhD Program'); ?></option>
						<?php institution_phd_programs_list(); ?>
					</select>
				</p>
				<!-- End Meta Field: PhD Program -->
				<!-- Start Meta Field: Website -->
				<p>
					<label><?php echo __('Website', 'phdhub-cpts'); ?></label>
					<input type="text" size="80" name="website" value="<?php echo $website; ?>" />
				</p>
				<!-- End Meta Field: Website -->
			</div>
		</div>
		<!-- End Left Column -->
		<!-- Start Right Column -->
		<div class="uk-width-1-2 inner-settings-box-right">
			<!-- Start Meta Field: General Info -->
			<p>
				<label><?php echo __('General Info', 'phdhub-cpts'); ?></label>
			</p>
			<?php wp_editor( html_entity_decode(stripcslashes($team_info)), $info ); ?>
			<!-- End Meta Field: General Info -->
		</div>
		<!-- End Right Column -->
	</div>
</div>
<?php
}



/*
 * Save Custom Meta Fields of Research Teams CPT
 */
function save_research_teams_fields( $team_id, $team ) {
    if ( $team->post_type == 'research-teams' ) {
		/* Save Meta Field: Logo */
        if ( isset( $_POST['research_team_logo'] )) {
            update_post_meta( $team->ID, 'research_team_logo', $_POST['research_team_logo'] );
        }
		/* Save Meta Field: Institution */
        if ( isset( $_POST['institution'] )) {
            update_post_meta( $team->ID, 'institution', $_POST['institution'] );
        }
		/* Save Meta Field: Institution Label */
        if ( isset( $_POST['institution_label'] )) {
            update_post_meta( $team->ID, 'institution_label', $_POST['institution_label'] );
        }
		/* Save Meta Field: Faculty */
        if ( isset( $_POST['faculty'] )) {
            update_post_meta( $team->ID, 'faculty', $_POST['faculty'] );
        }
		/* Save Meta Field: PhD Program */
        if ( isset( $_POST['phd_program'] )) {
            update_post_meta( $team->ID, 'phd_program', $_POST['phd_program'] );
        }
		/* Save Meta Field: Website */
        if ( isset( $_POST['website'] )) {
            update_post_meta( $team->ID, 'website', $_POST['website'] );
        }
		/* Save Meta Field: General Info */
        if ( isset( $_POST['team_info'] )) {
            update_post_meta( $team->ID, 'team_info', $_POST['team_info'] );
        }
    }
}
add_action( 'save_post', 'save_research_teams_fields', 10, 2 );

?>