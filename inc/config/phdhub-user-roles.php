<?php

defined('ABSPATH') or die;

if( get_role('author') ){
      remove_role( 'author' );
}
if( get_role('editor') ){
      remove_role( 'editor' );
}

/* Add Lead-Researcher Role */
// remove_role('lead-researcher');
add_role(
    'lead-researcher',
    __( 'Lead Researcher' ),
    array(
		'read' => true,
		'manage_categories' => true,
        'delete_phd_openings' => true,
        'delete_published_phd_openings' => true, 
        'edit_phd_openings' => true, 
        'edit_published_phd_openings' => true, 
        'publish_phd_openings' => true, 
        'read_private_phd_openings' => true, 
        'delete_phd_templates' => true,
        'delete_published_phd_templates' => true, 
        'edit_phd_templates' => true, 
        'edit_published_phd_templates' => true, 
        'publish_phd_templates' => true, 
        'read_private_phd_templates' => true, 
        'delete_cooperation_calls' => true,
        'delete_published_cooperation_calls' => true, 
        'edit_cooperation_calls' => true, 
        'edit_published_cooperation_calls' => true, 
        'publish_cooperation_calls' => true, 
        'read_private_cooperation_calls' => true, 
        'delete_research_teams' => true,
        'delete_published_research_teams' => true, 
        'edit_research_teams' => true, 
        'edit_published_research_teams' => true, 
        'publish_research_teams' => true, 
        'read_private_research_teams' => true, 
		'upload_files' => true,
    )
);

/* Add Assist-Researcher Role */
// remove_role('assist-researcher');
add_role(
    'assist-researcher',
    __( 'Assist Researcher' ),
    array(
		'read' => true,
        'delete_cooperation_calls' => true,
        'delete_published_cooperation_calls' => true, 
        'edit_cooperation_calls' => true, 
        'edit_published_cooperation_calls' => true, 
        'publish_cooperation_calls' => true, 
        'read_private_cooperation_calls' => true, 
		'upload_files' => true,
		'manage_categories' => true,
    )
);

/* Add Admin-Staff Role */
// remove_role('admin-staff');
add_role(
    'admin-staff',
    __( 'Admin Staff' ),
    array(
		'read' => true,
		'manage_categories' => true,
        'delete_institutions' => true,
        'delete_published_institutions' => true, 
        'edit_institutions' => true, 
        'edit_published_institutions' => true, 
        'publish_institutions' => true, 
        'read_private_institutions' => true, 
        'delete_faculties' => true,
        'delete_published_faculties' => true, 
        'edit_faculties' => true, 
        'edit_published_faculties' => true, 
        'publish_faculties' => true, 
        'read_private_faculties' => true, 
        'delete_phd_programs' => true,
        'delete_published_phd_programs' => true, 
        'edit_phd_programs' => true, 
        'edit_published_phd_programs' => true, 
        'publish_phd_programs' => true, 
        'read_private_phd_programs' => true, 
        'delete_phd_openings' => true,
        'delete_published_phd_openings' => true, 
        'edit_phd_openings' => true, 
        'edit_published_phd_openings' => true, 
        'publish_phd_openings' => true, 
        'read_private_phd_openings' => true, 
        'delete_phd_templates' => true,
        'delete_published_phd_templates' => true, 
        'edit_phd_templates' => true, 
        'edit_published_phd_templates' => true, 
        'publish_phd_templates' => true, 
        'read_private_phd_templates' => true, 
	'upload_files' => true,
	'delete_posts' => true,
	'delete_private_posts' => true,
	'delete_published_posts' => true,
	'edit_posts' => true,
	'edit_private_posts' => true,
	'edit_published_posts' => true,
	'publish_posts' => true,
	'read_private_posts' => true,
	'edit_pages' => true,
	'edit_published_pages' => true,
	'delete_pages' => true,
	'delete_private_pages' => true,
	'delete_published_pages' => true,
	'edit_private_pages' => true,
	'publish_pages' => true,
	'read_private_pages' => true,
	'create_users' => true,
	'edit_users' => true,
	'delete_users' => true,
	'list_users' => true,
	'edit_theme_options' => true,
	'edit_themes' => true,
	'ure_edit_roles' => true,
	'ure_create_roles' => true,
	'ure_reset_roles' => true,
	'ure_manage_options' => true,
	'ure_delete_roles' => true,
    )
);

/* Add Company-Rep Role */
// remove_role('company-rep');
add_role(
    'company-rep',
    __( 'Company Rep' ),
    array(
		'read' => true,
	'manage_categories' => true,
        'delete_cooperation_calls' => true,
        'delete_published_cooperation_calls' => true, 
        'edit_cooperation_calls' => true, 
        'edit_published_cooperation_calls' => true, 
        'publish_cooperation_calls' => true, 
        'read_private_cooperation_calls' => true, 
        'delete_companies' => true,
        'delete_published_companies' => true, 
        'edit_companies' => true, 
        'edit_published_companies' => true, 
        'publish_companies' => true, 
        'read_private_companies' => true, 
		'upload_files' => true,
    )
);

?>
