<?php

defined('ABSPATH') or die;

/* 
 * Register Institutions CPT
 */
function create_institutions_cpt() {
    register_post_type( 'institutions',
        array(
            'labels' => array(
                'name' => __('Institutions', 'phdhub-cpts'),
                'singular_name' => __('Institution', 'phdhub-cpts'),
                'add_new' => __('Add New', 'phdhub-cpts'),
                'add_new_item' => __('Add New Institution', 'phdhub-cpts'),
                'edit' => __('Edit', 'phdhub-cpts'),
                'edit_item' => __('Edit Institution', 'phdhub-cpts'),
                'new_item' => __('New Institution', 'phdhub-cpts'),
                'view' => __('View', 'phdhub-cpts'),
                'view_item' => __('View Institution', 'phdhub-cpts'),
                'search_items' => __('Search Institutions', 'phdhub-cpts'),
                'not_found' => __('No Institutions found', 'phdhub-cpts'),
                'not_found_in_trash' => __('No Institutions found in Trash', 'phdhub-cpts'),
                'parent' => __('Parent Institution', 'phdhub-cpts')
            ),
 
            'public' => true,
            'menu_position' => 26,
            'supports' => array( 'title', 'thumbnail' ),
            'taxonomies' => array( 'regions' ),
			'show_in_rest' => true,
            'menu_icon' => 'dashicons-admin-home',
            'has_archive' => true,
			'capability_type' => array('institution', 'institutions'),
			'map_meta_cap' => true
        )
    );
}
add_action( 'init', 'create_institutions_cpt' );



/* 
 * Register Faculties CPT
 */
function create_faculties_cpt() {
    register_post_type( 'faculties',
        array(
            'labels' => array(
                'name' => __('Faculties', 'phdhub-cpts'),
                'singular_name' => __('Faculty', 'phdhub-cpts'),
                'add_new' => __('Add New', 'phdhub-cpts'),
                'add_new_item' => __('Add New Faculty', 'phdhub-cpts'),
                'edit' => __('Edit', 'phdhub-cpts'),
                'edit_item' => __('Edit Faculty', 'phdhub-cpts'),
                'new_item' => __('New Faculty', 'phdhub-cpts'),
                'view' => __('View', 'phdhub-cpts'),
                'view_item' => __('View Faculty', 'phdhub-cpts'),
                'search_items' => __('Search Faculties', 'phdhub-cpts'),
                'not_found' => __('No Faculties found', 'phdhub-cpts'),
                'not_found_in_trash' => __('No Faculties found in Trash', 'phdhub-cpts'),
                'parent' => __('Parent Faculty', 'phdhub-cpts')
            ),
 
            'public' => true,
		//	'show_in_menu' => 'edit.php?post_type=institutions',
            'menu_position' => 26,
            'supports' => array( 'title', 'thumbnail' ),
            'taxonomies' => array( '' ),
			'show_in_rest' => true,
            'menu_icon' => 'dashicons-admin-multisite',
            'has_archive' => true,
			'capability_type' => array('faculty', 'faculties'),
			'map_meta_cap' => true
        )
    );
}
add_action( 'init', 'create_faculties_cpt' );

/* 
 * Register PhD Openings CPT
 */
function create_phd_openings_cpt() {
    register_post_type( 'phd-openings',
        array(
            'labels' => array(
                'name' => __('PhD Offers', 'phdhub-cpts'),
                'singular_name' => __('PhD Offer', 'phdhub-cpts'),
                'add_new' => __('Add New', 'phdhub-cpts'),
                'add_new_item' => __('Add New PhD Offer', 'phdhub-cpts'),
                'edit' => __('Edit', 'phdhub-cpts'),
                'edit_item' => __('Edit PhD Offer', 'phdhub-cpts'),
                'new_item' => __('New PhD Offer', 'phdhub-cpts'),
                'view' => __('View', 'phdhub-cpts'),
                'view_item' => __('View PhD Offer', 'phdhub-cpts'),
                'search_items' => __('Search PhD Offers', 'phdhub-cpts'),
                'not_found' => __('No PhD Offers found', 'phdhub-cpts'),
                'not_found_in_trash' => __('No PhD Offers found in Trash', 'phdhub-cpts'),
                'parent' => __('Parent PhD Offer', 'phdhub-cpts')
            ),
 
            'public' => true,
            'menu_position' => 25,
            'supports' => array( 'title', 'thumbnail' ),
            'taxonomies' => array( 'fields-of-science', 'research-areas' ),
			'show_in_rest' => true,
            'menu_icon' => 'dashicons-clipboard',
            'has_archive' => true,
			'capability_type' => array('phd_opening', 'phd_openings'),
			'map_meta_cap' => true
        )
    );
	
	register_taxonomy( 
		'research-areas', //taxonomy 
		'phd-openings', //post-type
		array( 
			'hierarchical'  => false, 
			'label'         => __( 'Research Areas','phdhub-cpts'), 
			'singular_name' => __( 'Research Area', 'phdhub-cpts' ), 
			'rewrite'       => true, 
			'query_var'     => true,
			'capabilities' => array(
				'manage_terms' => 'manage_categories',
				'edit_terms'   => 'manage_categories',
				'delete_terms' => 'manage_categories',
				'assign_terms' => 'manage_categories',
			),
		)
	);
	
	register_taxonomy( 
		'skills', //taxonomy 
		'phd-openings', //post-type
		array( 
			'hierarchical'  => false, 
			'label'         => __( 'Qualifications','phdhub-cpts'), 
			'singular_name' => __( 'Qualification', 'phdhub-cpts' ), 
			'rewrite'       => true, 
			'query_var'     => true,
			'capabilities' => array(
				'manage_terms' => 'manage_categories',
				'edit_terms'   => 'manage_categories',
				'delete_terms' => 'manage_categories',
				'assign_terms' => 'manage_categories',
			),
		)
	);
	
	register_taxonomy(
        'regions',
        'phd-openings',
        array(
            'labels' => array(
                'name' => __('Regions', 'phdhub-cpts'),
                'add_new_item' => __('Add New Region', 'phdhub-cpts'),
                'new_item_name' => __("New Region", 'phdhub-cpts')
            ),
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true,
			'capabilities' => array(
				'manage_terms' => 'manage_categories',
				'edit_terms'   => 'manage_categories',
				'delete_terms' => 'manage_categories',
				'assign_terms' => 'manage_categories',
			),
        )
    );
}
add_action( 'init', 'create_phd_openings_cpt' );


/* 
 * Register PhD Programs CPT
 */
function create_phd_programs_cpt() {
    register_post_type( 'phd-programs',
        array(
            'labels' => array(
                'name' => __('PhD Programs', 'phdhub-cpts'),
                'singular_name' => __('PhD Program', 'phdhub-cpts'),
                'add_new' => __('Add New', 'phdhub-cpts'),
                'add_new_item' => __('Add New PhD Program', 'phdhub-cpts'),
                'edit' => __('Edit', 'phdhub-cpts'),
                'edit_item' => __('Edit PhD Program', 'phdhub-cpts'),
                'new_item' => __('New PhD Program', 'phdhub-cpts'),
                'view' => __('View', 'phdhub-cpts'),
                'view_item' => __('View PhD Program', 'phdhub-cpts'),
                'search_items' => __('Search PhD Programs', 'phdhub-cpts'),
                'not_found' => __('No PhD Programs found', 'phdhub-cpts'),
                'not_found_in_trash' => __('No PhD Programs found in Trash', 'phdhub-cpts'),
                'parent' => __('Parent PhD Program', 'phdhub-cpts')
            ),
 
            'public' => true,
            'menu_position' => 26,
            'supports' => array( 'title', 'thumbnail' ),
            'taxonomies' => array( 'fields-of-science', 'research-areas' ),
			'show_in_rest' => true,
            'menu_icon' => 'dashicons-welcome-learn-more',
            'has_archive' => true,
			'capability_type' => array('phd_program', 'phd_programs'),
			'map_meta_cap' => true
        )
    );
}
add_action( 'init', 'create_phd_programs_cpt' );

/* 
 * Register PhD Templates CPT
 */
function create_phd_templates_cpt() {
    register_post_type( 'phd-templates',
        array(
            'labels' => array(
                'name' => __('PhD Templates', 'phdhub-cpts'),
                'singular_name' => __('PhD Template', 'phdhub-cpts'),
                'add_new' => __('Add New', 'phdhub-cpts'),
                'add_new_item' => __('Add New PhD Template', 'phdhub-cpts'),
                'edit' => __('Edit', 'phdhub-cpts'),
                'edit_item' => __('Edit PhD Template', 'phdhub-cpts'),
                'new_item' => __('New PhD Template', 'phdhub-cpts'),
                'view' => __('View', 'phdhub-cpts'),
                'view_item' => __('View PhD Template', 'phdhub-cpts'),
                'search_items' => __('Search PhD Templates', 'phdhub-cpts'),
                'not_found' => __('No PhD Templates found', 'phdhub-cpts'),
                'not_found_in_trash' => __('No PhD Templates found in Trash', 'phdhub-cpts'),
                'parent' => __('Parent PhD Template', 'phdhub-cpts')
            ),
 
            'public' => true,
			'show_in_menu' => 'edit.php?post_type=phd-openings',
            'menu_position' => null,
            'supports' => array( 'title' ),
            'taxonomies' => array( '' ),
            'has_archive' => false,
			'capability_type' => array('phd_template', 'phd_templates'),
			'map_meta_cap' => true
        )
    );
}
add_action( 'init', 'create_phd_templates_cpt' );

/*
 * Register Research Area Custom Taxonomy
 */
function register_research_area() {
    register_taxonomy(
        'fields-of-science',
        'phd-openings',
        array(
            'labels' => array(
                'name' => __('Fields of Science', 'phdhub-cpts'),
                'add_new_item' => __('Add New Field of Science', 'phdhub-cpts'),
                'new_item_name' => __("New Field of Science", 'phdhub-cpts')
            ),
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true,
			'capabilities' => array(
				'manage_terms' => 'manage_categories',
				'edit_terms'   => 'manage_categories',
				'delete_terms' => 'manage_categories',
				'assign_terms' => 'manage_categories',
			),
        )
    );
}
add_action('init', 'register_research_area', 0);


/*
 * Register Research Area Custom Taxonomy
 */
function register_user_qualifications() {
    register_taxonomy(
        'qualifications',
        'user',
        array(
            'labels' => array(
                'name' => __('Qualifications', 'phdhub-cpts'),
                'add_new_item' => __('Add New Qualification', 'phdhub-cpts'),
                'new_item_name' => __("New Qualification", 'phdhub-cpts')
            ),
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => false,
        )
    );
}
add_action('init', 'register_user_qualifications', 0);

function add_user_taxonomy_admin_page() {
    $tax = get_taxonomy( 'qualifications' );
    if (!is_object($tax) OR is_wp_error($tax)) 
        return;
    add_users_page(
        esc_attr( $tax->labels->menu_name ),
        esc_attr( $tax->labels->menu_name ),
        $tax->cap->manage_terms,
        'edit-tags.php?taxonomy=' . $tax->name
    );
}
add_action( 'admin_menu', 'add_user_taxonomy_admin_page');

function filter_user_taxonomy_admin_page_parent_file( $parent_file = '' ) {
    global $pagenow;

    /**
     * Only filter the parent if we are on a the taxonomy screen for 
     */
    if ( ! empty( $_GET['taxonomy'] ) && ($_GET['taxonomy'] == 'qualifications') && $pagenow == 'edit-tags.php' ) {
        $parent_file = 'users.php';
    }

    return $parent_file;
}
add_filter( 'parent_file', 'filter_user_taxonomy_admin_page_parent_file');

/*
 * Register Qualifications Custom Taxonomy
 */
function register_qualifications() {
    register_taxonomy(
        'qualifications',
        'user',
		array( 
			'labels' => array(
				'name'		=> 'Qualifications',
				'singular_name'	=> 'Qualification',
				'menu_name'	=> 'Qualifications',
				'search_items'	=> 'Search Qualifications',
				'popular_items' => 'Popular Qualifications',
				'all_items'	=> 'All Qualifications',
				'edit_item'	=> 'Edit Qualification',
				'update_item'	=> 'Update Qualification',
				'add_new_item'	=> 'Add New Qualification',
				'new_item_name'	=> 'New Qualification',
			),
			'hierarchical'  => false, 
			'update_count_callback' => function() {
				return; //important
			}
		)
    );
}
add_action('init', 'register_research_area', 0);

/* 
 * Register Calls for Cooperation CPT
 */
function create_phd_calls_for_cooperation_cpt() {
    register_post_type( 'cooperation-calls',
        array(
            'labels' => array(
                'name' => __('Cooperation Calls', 'phdhub-cpts'),
                'singular_name' => __('Call for Cooperation', 'phdhub-cpts'),
                'add_new' => __('Add New', 'phdhub-cpts'),
                'add_new_item' => __('Add New Call for Cooperation', 'phdhub-cpts'),
                'edit' => __('Edit', 'phdhub-cpts'),
                'edit_item' => __('Edit Call for Cooperation', 'phdhub-cpts'),
                'new_item' => __('New Call for Cooperation', 'phdhub-cpts'),
                'view' => __('View', 'phdhub-cpts'),
                'view_item' => __('View Call for Cooperation', 'phdhub-cpts'),
                'search_items' => __('Search Calls for Cooperation', 'phdhub-cpts'),
                'not_found' => __('No Calls for Cooperation found', 'phdhub-cpts'),
                'not_found_in_trash' => __('No Calls for Cooperation found in Trash', 'phdhub-cpts'),
                'parent' => __('Parent Call for Cooperation', 'phdhub-cpts')
            ),
 
            'public' => true,
            'menu_position' => 26,
            'supports' => array( 'title', 'thumbnail' ),
            'taxonomies' => array( 'fields-of-science', 'research-areas' ),
			'show_in_rest' => true,
            'menu_icon' => 'dashicons-megaphone',
            'has_archive' => true,
			'capability_type' => array('cooperation_call', 'cooperation_calls'),
			'map_meta_cap' => true
        )
    );
}
add_action( 'init', 'create_phd_calls_for_cooperation_cpt' );


/* 
 * Register Research Teams CPT
 */
function create_phd_research_teams_cpt() {
    register_post_type( 'research-teams',
        array(
            'labels' => array(
                'name' => __('Research Teams', 'phdhub-cpts'),
                'singular_name' => __('Research Team', 'phdhub-cpts'),
                'add_new' => __('Add New', 'phdhub-cpts'),
                'add_new_item' => __('Add New Research Team', 'phdhub-cpts'),
                'edit' => __('Edit', 'phdhub-cpts'),
                'edit_item' => __('Edit Research Team', 'phdhub-cpts'),
                'new_item' => __('New Research Team', 'phdhub-cpts'),
                'view' => __('View', 'phdhub-cpts'),
                'view_item' => __('View Research Team', 'phdhub-cpts'),
                'search_items' => __('Search Research Teams', 'phdhub-cpts'),
                'not_found' => __('No Research Teams found', 'phdhub-cpts'),
                'not_found_in_trash' => __('No Research Teams found in Trash', 'phdhub-cpts'),
                'parent' => __('Parent Research Team', 'phdhub-cpts')
            ),
 
            'public' => true,
            'menu_position' => 26,
            'supports' => array( 'title', 'thumbnail' ),
            'taxonomies' => array( 'research-areas', 'fields-of-science' ),
			'show_in_rest' => true,
            'menu_icon' => 'dashicons-groups',
            'has_archive' => true,
			'capability_type' => array('research_team', 'research_teams'),
			'map_meta_cap' => true
        )
    );
}
add_action( 'init', 'create_phd_research_teams_cpt' );


/* 
 * Register Companies CPT
 */
function create_companies_cpt() {
    register_post_type( 'companies',
        array(
            'labels' => array(
                'name' => __('Companies', 'phdhub-cpts'),
                'singular_name' => __('Company', 'phdhub-cpts'),
                'add_new' => __('Add New', 'phdhub-cpts'),
                'add_new_item' => __('Add New Company', 'phdhub-cpts'),
                'edit' => __('Edit', 'phdhub-cpts'),
                'edit_item' => __('Edit Company', 'phdhub-cpts'),
                'new_item' => __('New Company', 'phdhub-cpts'),
                'view' => __('View', 'phdhub-cpts'),
                'view_item' => __('View Companies', 'phdhub-cpts'),
                'search_items' => __('Search Companies', 'phdhub-cpts'),
                'not_found' => __('No Companies found', 'phdhub-cpts'),
                'not_found_in_trash' => __('No Companies found in Trash', 'phdhub-cpts'),
                'parent' => __('Parent Company', 'phdhub-cpts')
            ),
 
            'public' => true,
            'menu_position' => 26,
            'supports' => array( 'title', 'thumbnail' ),
            'taxonomies' => array( 'regions', 'fields-of-science', 'research-areas' ),
			'show_in_rest' => true,
            'menu_icon' => 'dashicons-building',
            'has_archive' => true,
			'capability_type' => array('company', 'companies'),
			'map_meta_cap' => true
        )
    );
}
add_action( 'init', 'create_companies_cpt' );


/*
 * Add meta box for the above CPTs
 */
function phdhub_cpts_meta() {
    add_meta_box( 'phd_opening_meta',
        'PhD Opening Details',
        'display_phd_opening_meta',
        'phd-openings', 'normal', 'high'
    );
    add_meta_box( 'institution_meta',
        'Institution Details',
        'display_institution_meta',
        'institutions', 'normal', 'high'
    );
    add_meta_box( 'faculty_meta',
        'Faculty Details',
        'display_faculty_meta',
        'faculties', 'normal', 'high'
    );
    add_meta_box( 'phd_program_meta',
        'PhD Program Details',
        'display_phd_program_meta',
        'phd-programs', 'normal', 'high'
    );
    add_meta_box( 'cooperation_calls_meta',
        'Call for Cooperation Details',
        'display_cooperation_calls_meta',
        'cooperation-calls', 'normal', 'high'
    );
    add_meta_box( 'research_teams_meta',
        'Research Team Details',
        'display_research_teams_meta',
        'research-teams', 'normal', 'high'
    );
    add_meta_box( 'phd_template_meta',
        'PhD Template Details',
        'display_phd_template_meta',
        'phd-templates', 'normal', 'high'
    );
    add_meta_box( 'company_meta',
        'Company Details',
        'display_company_meta',
        'companies', 'normal', 'high'
    );
}
add_action( 'admin_init', 'phdhub_cpts_meta' );

?>
