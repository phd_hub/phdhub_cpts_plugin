<?php

defined('ABSPATH') or die;


/* 
 * Add user meta fields
 */
add_action('show_user_profile', 'phdhub_user_profile_fields');
add_action('edit_user_profile', 'phdhub_user_profile_fields');

function phdhub_user_profile_fields( $user ) {
		$fb_link = esc_url( get_the_author_meta( 'fb_link', $user->ID ) );
		$twitter_link = esc_url( get_the_author_meta( 'twitter_link', $user->ID ) );
		$linkedin_link = esc_url( get_the_author_meta( 'linkedin_link', $user->ID ) );
		$gplus_link = esc_url( get_the_author_meta( 'gplus_link', $user->ID ) );
?>
	<h2><?php echo __('Social Networks', 'phdhub-cpts'); ?></h2>
    <table class="form-table">
        <tr>
            <th>
                <label for="fb-link"><?php _e( 'Facebook' ); ?></label>
            </th>
            <td>
                <input type="url" name="fb_link" id="fb-link" value="<?php echo $fb_link; ?>" class="regular-text" />
            </td>
        </tr>
        <tr>
            <th>
                <label for="fb-link"><?php _e( 'Twitter' ); ?></label>
            </th>
            <td>
                <input type="url" name="twitter_link" id="twitter-link" value="<?php echo $twitter_link; ?>" class="regular-text" />
            </td>
        </tr>
        <tr>
            <th>
                <label for="fb-link"><?php _e( 'LinkedIn' ); ?></label>
            </th>
            <td>
                <input type="url" name="linkedin_link" id="linkedin-link" value="<?php echo $linkedin_link; ?>" class="regular-text" />
            </td>
        </tr>
        <tr>
            <th>
                <label for="fb-link"><?php _e( 'Google+' ); ?></label>
            </th>
            <td>
                <input type="url" name="gplus_link" id="gplus-link" value="<?php echo $gplus_link; ?>" class="regular-text" />
            </td>
        </tr>
    </table>
<?php
	if ( in_array( 'lead-researcher', (array) $user->roles ) || in_array( 'assist-researcher', (array) $user->roles ) ) {
		$institution = esc_attr( get_the_author_meta( 'institution', $user->ID ) );
		$faculty = esc_attr( get_the_author_meta( 'faculty', $user->ID ) );
		$research_team = esc_attr( get_the_author_meta( 'research_team', $user->ID ) );
		$studies = esc_attr( get_the_author_meta( 'studies', $user->ID ) );
		$professional_experience = esc_attr( get_the_author_meta( 'professional_experience', $user->ID ) );
		$research_interests = esc_attr( get_the_author_meta( 'research_interests', $user->ID ) );
		$fields_of_science_list = get_the_author_meta( 'fields_of_science', $user->ID );
		if ($fields_of_science_list == NULL) {
			$fields_of_science_list = array();
		}
		$research_areas_list = get_the_author_meta( 'research_areas', $user->ID );
		if ($research_areas_list == NULL) {
			$research_areas_list = array();
		}
		$qualifications_list = get_the_author_meta( 'skills', $user->ID );
		if ($qualifications_list == NULL) {
			$qualifications_list = array();
		}
?>
	<h2><?php echo __('More Info', 'phdhub-cpts'); ?></h2>
    <table class="form-table">
        <tr>
            <th>
                <label for="institution"><?php echo __( 'Institution', 'phdhub-cpts' ); ?></label>
            </th>
            <td>
                <input type="text" name="institution" id="institution-autocomplete" value="<?php echo $institution; ?>" placeholder="<?php echo __('Search by typing institution name', 'phdhub-cpts'); ?>..." class="regular-text" />
				<input type="hidden" id="phd-id" value="<?php echo $user->ID; ?>">
            </td>
        </tr>
        <tr>
            <th>
                <label for="faculty"><?php echo __( 'Faculty', 'phdhub-cpts' ); ?></label>
            </th>
            <td>
                <select id="faculty" name="faculty">
						<option value="empty" <?php selected( $faculty, 'empty' ); ?>><?php echo __('Select an institution before selecting a faculty', 'phdhub-cpts'); ?></option>
						<?php institution_faculty_list( ); ?>
				</select>
            </td>
        </tr>
        <tr>
            <th>
                <label for="research_team"><?php echo __( 'Research Team', 'phdhub-cpts' ); ?></label>
            </th>
            <td>
                <select id="research-team" name="research_team">
						<option value="empty" <?php selected( $research_team, 'empty' ); ?>><?php echo __('Select an institution before selecting a research team', 'phdhub-cpts'); ?></option>
						<?php institution_research_teams_list( ); ?>
				</select>
            </td>
        </tr>
        <tr>
            <th>
                <label for="studies"><?php echo __( 'Studies', 'phdhub-cpts' ); ?></label>
            </th>
            <td>
				<textarea name="studies" rows="5"><?php echo $studies; ?></textarea>
            </td>
        </tr>
        <tr>
            <th>
                <label for="professional_experience"><?php echo __( 'Professional Experience', 'phdhub-cpts' ); ?></label>
            </th>
            <td>
				<textarea name="professional_experience" rows="5"><?php echo $professional_experience; ?></textarea>
            </td>
        </tr>
        <tr>
            <th>
                <label for="field_of_science"><?php echo __( 'Fields of Science', 'phdhub-cpts' ); ?></label>
            </th>
			<td>
				<div id="fields-of-science">
					<span class="field-info">
						<?php echo __('I am interested in the following Fields of Science', 'phdhub-up'); ?>
					</span>
					<div class="uk-grid fields-of-science-list">
						<div class="uk-width-1-2">
							<ul data-uk-switcher="{connect:'#fos-list'}">
								<?php 
									$hiterms = get_terms("fields-of-science", array("hide_empty" => false, "parent" => 0));
									foreach($hiterms as $key => $hiterm) {
								?>
								<li>
									<a href=""><?php echo $hiterm->name; ?></a>
								</li>
								<?php
									}
								?>
							</ul>
						</div>
						<div class="uk-width-1-2">
							<ul id="fos-list" class="uk-switcher">
								<?php 
									foreach($hiterms as $key => $hiterm) {
								?>
								<li>
									<span class="checkbox-row">
										<input type="checkbox" name="fields_of_science[]" value="<?php echo $hiterm->term_id; ?>" <?php checked(in_array($hiterm->term_id, $fields_of_science_list)); ?>><?php echo $hiterm->name; ?>
									</span>
									<?php
										$loterms = get_terms("fields-of-science", array("hide_empty" => false, "parent" => $hiterm->term_id));
										foreach($loterms as $key => $loterm) {
											$loterms2 = get_terms("fields-of-science", array("hide_empty" => false, "parent" => $loterm->term_id));
									?>
									<span class="checkbox-row">
										<input type="checkbox" class="child" name="fields_of_science[]" value="<?php echo $loterm->term_id; ?>" <?php checked(in_array($loterm->term_id, $fields_of_science_list)); ?>><?php echo $loterm->name; ?>
									</span>
									<?php 
										if ($loterms2) {
									?>
										<ul class="children-fos">
											<?php
												foreach ($loterms2 as $key => $loterm2) {
											?>
											<li>
												<span class="checkbox-row">
													<input type="checkbox" class="child" name="fields_of_science[]" value="<?php echo $loterm2->term_id; ?>" <?php checked(in_array($loterm2->term_id, $fields_of_science_list)); ?>><?php echo $loterm2->name; ?>
												</span>
											</li>
											<?php
												}
											?>
										</ul>
									<?php 
											}
										}
									?>
								</li>
								<?php
									}
								?>
							</ul>
						</div>
					</div>
				</div>
			</td>
        </tr>
		<tr>
			<th>
				<label for="research_areas"><?php echo __('Research Areas', 'phdhub-cpts'); ?></label>
			</th>
			<td>
				<div id="research-areas">
					<span class="field-info">
						<?php echo __('I am interested in the following Research Areas', 'phdhub-up'); ?>
					</span>
					<div class="research-areas-list">
						<ul class="ra-list">
							<?php 
								$hiterms = get_terms("research-areas", array("hide_empty" => false));
								foreach($hiterms as $key => $hiterm) {
							?>
							<li>
								<span class="checkbox-row">
									<input type="checkbox" name="research_areas[]" value="<?php echo $hiterm->term_id; ?>" <?php checked(in_array($hiterm->term_id, $research_areas_list)); ?>><?php echo $hiterm->name; ?>
								</span>
							</li>
							<?php
								}
							?>
						</ul>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<th>
				<label for="research_areas"><?php echo __('Qualifications', 'phdhub-cpts'); ?></label>
			</th>
			<td>
				<div id="qualifications">
					<span class="field-info">
						<?php echo __('Select your qualifications from the following list', 'phdhub-up'); ?>
					</span>
					<div class="research-areas-list">
						<ul class="ra-list">
							<?php 
								$hiterms = get_terms("skills", array("hide_empty" => false));
								foreach($hiterms as $key => $hiterm) {
							?>
							<li>
								<span class="checkbox-row">
									<input type="checkbox" name="skills[]" value="<?php echo $hiterm->term_id; ?>" <?php checked(in_array($hiterm->term_id, $qualifications_list)); ?>><?php echo $hiterm->name; ?>
								</span>
							</li>
							<?php
								}
							?>
						</ul>
					</div>
				</div>
			</td>
		</tr>
    </table>
<?php
	}
	if ( in_array( 'company-rep', (array) $user->roles ) ) {
		$company = esc_attr( get_the_author_meta( 'company', $user->ID ) );
?>
	<h2><?php echo __('More Info', 'phdhub-cpts'); ?></h2>
    <table class="form-table">
        <tr>
            <th>
                <label for="company"><?php echo __( 'Company', 'phdhub-cpts' ); ?></label>
            </th>
            <td>
                <input type="text" name="company" id="company-autocomplete" value="<?php echo $company; ?>" placeholder="<?php echo __('Search by typing company\'s name', 'phdhub-cpts'); ?>..." class="regular-text" />
            </td>
        </tr>
	</table>
<?php
	}
	if ( in_array( 'admin-staff', (array) $user->roles ) ) {
		$institution = esc_attr( get_the_author_meta( 'institution', $user->ID ) );
		$faculty = esc_attr( get_the_author_meta( 'faculty', $user->ID ) );
?>
	<h2><?php echo __('More Info', 'phdhub-cpts'); ?></h2>
    <table class="form-table">
        <tr>
            <th>
                <label for="institution"><?php echo __( 'Institution', 'phdhub-cpts' ); ?></label>
            </th>
            <td>
                <input type="text" name="institution" id="institution-autocomplete" value="<?php echo $institution; ?>" placeholder="<?php echo __('Search by typing institution name', 'phdhub-cpts'); ?>..." class="regular-text" />
				<input type="hidden" id="phd-id" value="<?php echo $user->ID; ?>">
            </td>
        </tr>
        <tr>
            <th>
                <label for="faculty"><?php echo __( 'Faculty', 'phdhub-cpts' ); ?></label>
            </th>
            <td>
                <select id="faculty" name="faculty">
						<option value="empty" <?php selected( $faculty, 'empty' ); ?>><?php echo __('Select an institution before selecting a faculty', 'phdhub-cpts'); ?></option>
						<?php institution_faculty_list( ); ?>
				</select>
            </td>
        </tr>
	</table>
<?php
	}
	if ( in_array( 'subscriber', (array) $user->roles ) ) {
		$studies = esc_attr( get_the_author_meta( 'studies', $user->ID ) );
		$fields_of_science_list = get_the_author_meta( 'fields_of_science', $user->ID );
		if ($fields_of_science_list == NULL) {
			$fields_of_science_list = array();
		}
		$research_areas_list = get_the_author_meta( 'research_areas', $user->ID );
		if ($research_areas_list == NULL) {
			$research_areas_list = array();
		}
		$qualifications_list = get_the_author_meta( 'skills', $user->ID );
		if ($qualifications_list == NULL) {
			$qualifications_list = array();
		}
?>
	<h2><?php echo __('More Info', 'phdhub-cpts'); ?></h2>
    <table class="form-table">
        <tr>
            <th>
                <label for="studies"><?php echo __( 'Studies', 'phdhub-cpts' ); ?></label>
            </th>
            <td>
				<textarea name="studies" rows="5"><?php echo $studies; ?></textarea>
            </td>
        </tr>
		<tr>
			<th>
				<label for="fields_of_science"><?php echo __('Fields of Science', 'phdhub-cpts'); ?></label>
			</th>
			<td>
				<div id="fields-of-science">
					<span class="field-info">
						<?php echo __('I am interested in the following Fields of Science', 'phdhub-up'); ?>
					</span>
					<div class="uk-grid fields-of-science-list">
						<div class="uk-width-1-2">
							<ul data-uk-switcher="{connect:'#fos-list'}">
								<?php 
									$hiterms = get_terms("fields-of-science", array("hide_empty" => false, "parent" => 0));
									foreach($hiterms as $key => $hiterm) {
								?>
								<li>
									<a href=""><?php echo $hiterm->name; ?></a>
								</li>
								<?php
									}
								?>
							</ul>
						</div>
						<div class="uk-width-1-2">
							<ul id="fos-list" class="uk-switcher">
								<?php 
									foreach($hiterms as $key => $hiterm) {
								?>
								<li>
									<span class="checkbox-row">
										<input type="checkbox" name="fields_of_science[]" value="<?php echo $hiterm->term_id; ?>" <?php checked(in_array($hiterm->term_id, $fields_of_science_list)); ?>><?php echo $hiterm->name; ?>
									</span>
									<?php
										$loterms = get_terms("fields-of-science", array("hide_empty" => false, "parent" => $hiterm->term_id));
										foreach($loterms as $key => $loterm) {
											$loterms2 = get_terms("fields-of-science", array("hide_empty" => false, "parent" => $loterm->term_id));
									?>
									<span class="checkbox-row">
										<input type="checkbox" class="child" name="fields_of_science[]" value="<?php echo $loterm->term_id; ?>" <?php checked(in_array($loterm->term_id, $fields_of_science_list)); ?>><?php echo $loterm->name; ?>
									</span>
									<?php 
										if ($loterms2) {
									?>
										<ul class="children-fos">
											<?php
												foreach ($loterms2 as $key => $loterm2) {
											?>
											<li>
												<span class="checkbox-row">
													<input type="checkbox" class="child" name="fields_of_science[]" value="<?php echo $loterm2->term_id; ?>" <?php checked(in_array($loterm2->term_id, $fields_of_science_list)); ?>><?php echo $loterm2->name; ?>
												</span>
											</li>
											<?php
												}
											?>
										</ul>
									<?php 
											}
										}
									?>
								</li>
								<?php
									}
								?>
							</ul>
						</div>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<th>
				<label for="research_areas"><?php echo __('Research Areas', 'phdhub-cpts'); ?></label>
			</th>
			<td>
				<div id="research-areas">
					<span class="field-info">
						<?php echo __('I am interested in the following Research Areas', 'phdhub-up'); ?>
					</span>
					<div class="research-areas-list">
						<ul class="ra-list">
							<?php 
								$hiterms = get_terms("research-areas", array("hide_empty" => false));
								foreach($hiterms as $key => $hiterm) {
							?>
							<li>
								<span class="checkbox-row">
									<input type="checkbox" name="research_areas[]" value="<?php echo $hiterm->term_id; ?>" <?php checked(in_array($hiterm->term_id, $research_areas_list)); ?>><?php echo $hiterm->name; ?>
								</span>
							</li>
							<?php
								}
							?>
						</ul>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<th>
				<label for="research_areas"><?php echo __('Qualifications', 'phdhub-cpts'); ?></label>
			</th>
			<td>
				<div id="qualifications">
					<span class="field-info">
						<?php echo __('Select your qualifications from the following list', 'phdhub-up'); ?>
					</span>
					<div class="research-areas-list">
						<ul class="ra-list">
							<?php 
								$hiterms = get_terms("skills", array("hide_empty" => false));
								foreach($hiterms as $key => $hiterm) {
							?>
							<li>
								<span class="checkbox-row">
									<input type="checkbox" name="skills[]" value="<?php echo $hiterm->term_id; ?>" <?php checked(in_array($hiterm->term_id, $qualifications_list)); ?>><?php echo $hiterm->name; ?>
								</span>
							</li>
							<?php
								}
							?>
						</ul>
					</div>
				</div>
			</td>
		</tr>
	</table>
<?php
	}
}

add_action( 'personal_options_update', 'update_phdhub_extra_profile_fields' ); 
add_action( 'edit_user_profile_update', 'update_phdhub_extra_profile_fields' );

function update_phdhub_extra_profile_fields( $user_id ) {
    if ( current_user_can( 'edit_user', $user_id ) ) {
        update_user_meta( $user_id, 'fb_link', $_POST['fb_link'] );
        update_user_meta( $user_id, 'twitter_link', $_POST['twitter_link'] );
        update_user_meta( $user_id, 'linkedin_link', $_POST['linkedin_link'] );
        update_user_meta( $user_id, 'gplus_link', $_POST['gplus_link'] );
		
		if ( isset( $_POST['institution'] ) ) {
			update_user_meta( $user_id, 'institution', $_POST['institution'] );
		}
		if ( isset( $_POST['faculty'] ) ) {
			update_user_meta( $user_id, 'faculty', $_POST['faculty'] );
		}
		if ( isset( $_POST['research_team'] ) ) {
			update_user_meta( $user_id, 'research_team', $_POST['research_team'] );
		}
		if ( isset( $_POST['studies'] ) ) {
			update_user_meta( $user_id, 'studies', $_POST['studies'] );
		}
		if ( isset( $_POST['professional_experience'] ) ) {
			update_user_meta( $user_id, 'professional_experience', $_POST['professional_experience'] );
		}
		if ( isset( $_POST['research_interests'] ) ) {
			update_user_meta( $user_id, 'research_interests', $_POST['research_interests'] );
		}
		if ( isset( $_POST['fields_of_science'] ) ) {
			update_user_meta( $user_id, 'fields_of_science', $_POST['fields_of_science'] );
		}
		if ( $_POST['fields_of_science'] == NULL ) {
			delete_user_meta( $user_id, 'fields_of_science' );
		}
		if ( isset( $_POST['research_areas'] ) ) {
			update_user_meta( $user_id, 'research_areas', $_POST['research_areas'] );
		}
		if ( $_POST['research_areas'] == NULL ) {
			delete_user_meta( $user_id, 'research_areas' );
		}
		if ( isset( $_POST['skills'] ) ) {
			update_user_meta( $user_id, 'skills', $_POST['skills'] );
		}
		if ( $_POST['skills'] == NULL ) {
			delete_user_meta( $user_id, 'skills' );
		}
		if ( isset( $_POST['company'] ) ) {
			update_user_meta( $user_id, 'company', $_POST['company'] );
		}
	}
}

?>