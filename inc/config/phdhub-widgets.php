<?php
/*
 * PhD Hub Widgets
 *
 * Custom widget for PhD Hub's CPTs
 */

defined('ABSPATH') or die;

/*
 * Create PhD Hub Counter widget 
 * It displays the number of entries for every CPT
 */
class phdhub_counter extends WP_Widget {

	function __construct() {
		parent::__construct(
		'phdhub_counter', 
		__('PhD Hub Counter', 'phdhub-cpts'), 
		array( 'description' => __( 'Display the number of entries for every CPT', 'phdhub-cpts' ), ) 
		);
	}

	/*
	 * The frontend section of the widget
	 */
	public function widget( $widget_args, $instance ) {
		
        extract( $widget_args );
		
		echo $widget_args['before_widget'];
?>
<div class="uk-grid phdhub-counter">
	<div class="uk-width-1-5">
		<div class="counter-box">
			<?php
				/*
				 * Get the number of available PhD Offers
				 */
				$phd_openings_args = array(
					'posts_per_page' => -1,
					'post_type' => 'phd-openings'
				);
				$phd_openings = count( get_posts( $phd_openings_args ) );
			?>
			<h4>
				<a href="<?php echo site_url(); ?>/phd-openings"><?php echo $phd_openings; ?></a>
			</h4>
			<p><?php echo __('PhD Offers'); ?></p>
		</div>
	</div>
	<div class="uk-width-1-5">
		<div class="counter-box">
			<?php
				/*
				 * Get the number of available Calls for Cooperation
				 */
				$calls_args = array(
					'posts_per_page' => -1,
					'post_type' => 'cooperation-calls'
				);
				$calls = count( get_posts( $calls_args ) );
			?>
			<h4>
				<a href="<?php echo site_url(); ?>/cooperation-calls"><?php echo $calls; ?></a>
			</h4>
			<p><?php echo __('Calls for Cooperation'); ?></p>
		</div>
	</div>
	<div class="uk-width-1-5">
		<div class="counter-box">
			<?php
				/*
				 * Get the number of available Institutions
				 */
				$institutions_args = array(
					'posts_per_page' => -1,
					'post_type' => 'institutions'
				);
				$institutions = count( get_posts( $institutions_args ) );
			?>
			<h4>
				<a href="<?php echo site_url(); ?>/partners"><?php echo $institutions; ?></a>
			</h4>
			<p><?php echo __('Institutions'); ?></p>
		</div>
	</div>
	<div class="uk-width-1-5">
		<div class="counter-box">
			<?php
				/*
				 * Get the number of available Companies
				 */
				$companies_args = array(
					'posts_per_page' => -1,
					'post_type' => 'companies'
				);
				$companies = count( get_posts( $companies_args ) );
			?>
			<h4>
				<a href="<?php echo site_url(); ?>/partners"><?php echo $companies; ?></a>
			</h4>
			<p><?php echo __('Enterprises'); ?></p>
		</div>
	</div>
	<div class="uk-width-1-5">
		<div class="counter-box">
			<?php
				/*
				 * Get the number of available Research Areas
				 */
				$terms = get_terms( array(
					'taxonomy' => 'fields-of-science',
					'hide_empty' => false,
				) );
			?>
			<h4><?php echo count($terms); ?></h4>
			<p><?php echo __('Fields of Science'); ?></p>
		</div>
	</div>
</div>
<?php
		echo $widget_args['after_widget'];
	}

	/*
	 * The backend section of the widget
	 */
	public function form( $instance ) { }
	
	/*
	 * Update widget's settings
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		return $instance;
	}

}


/*
 * Create PhD Hub Central Counter widget 
 * It displays the number of entries for every CPT
 */
class phdhub_central_counter extends WP_Widget {

	function __construct() {
		parent::__construct(
		'phdhub_central_counter', 
		__('PhD Hub Central Counter', 'phdhub-cpts'), 
		array( 'description' => __( 'Display the number of entries for every CPT', 'phdhub-cpts' ), ) 
		);
	}

	/*
	 * The frontend section of the widget
	 */
	public function widget( $widget_args, $instance ) {
		
        extract( $widget_args );
		
		echo $widget_args['before_widget'];
        
        $phd_offers_num = 0;
        $calls_num = 0;
        $institutions_num = 0;
        $companies_num = 0;
        
    $blog_ids = wp_get_sites();
    foreach ($blog_ids as $key=>$current_blog) {
        // switch to each blog to get the posts
        switch_to_blog($current_blog['blog_id']);
        
        $phd_openings_args = array('posts_per_page' => -1, 'post_type' => 'phd-openings' );
        $phd_offers_num += count( get_posts( $phd_openings_args ) );
        
        $calls_args = array('posts_per_page' => -1, 'post_type' => 'cooperation-calls');
        $calls_num += count( get_posts( $calls_args ) );
        
        $institutions_args = array('posts_per_page' => -1, 'post_type' => 'institutions');
        $institutions_num += count( get_posts( $institutions_args ) );
        
        $companies_args = array('posts_per_page' => -1, 'post_type' => 'companies');
        $companies_num += count( get_posts( $companies_args ) );
        
        restore_current_blog();
    }
?>
<div class="uk-grid phdhub-counter">
	<div class="uk-width-1-5">
		<div class="counter-box">
			<h4>
				<a href="<?php echo site_url(); ?>/phd-offers"><?php echo $phd_offers_num; ?></a>
			</h4>
			<p><?php echo __('PhD Offers'); ?></p>
		</div>
	</div>
	<div class="uk-width-1-5">
		<div class="counter-box">
			<h4>
				<a href="<?php echo site_url(); ?>/find-your-partners"><?php echo $calls_num; ?></a>
			</h4>
			<p><?php echo __('Calls for Cooperation'); ?></p>
		</div>
	</div>
	<div class="uk-width-1-5">
		<div class="counter-box">
			<h4>
				<a href="<?php echo site_url(); ?>/partners"><?php echo $institutions_num; ?></a>
			</h4>
			<p><?php echo __('Institutions'); ?></p>
		</div>
	</div>
	<div class="uk-width-1-5">
		<div class="counter-box">
			<h4>
				<a href="<?php echo site_url(); ?>/partners"><?php echo $companies_num; ?></a>
			</h4>
			<p><?php echo __('Enterprises'); ?></p>
		</div>
	</div>
	<div class="uk-width-1-5">
		<div class="counter-box">
			<?php
				/*
				 * Get the number of available Research Areas
				 */
				$terms = get_terms( array(
					'taxonomy' => 'fields-of-science',
					'hide_empty' => false,
				) );
			?>
			<h4><?php echo count($terms); ?></h4>
			<p><?php echo __('Fields of Science'); ?></p>
		</div>
	</div>
</div>
<?php
		echo $widget_args['after_widget'];
	}

	/*
	 * The backend section of the widget
	 */
	public function form( $instance ) { }
	
	/*
	 * Update widget's settings
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		return $instance;
	}

}



/*
 * Create PhD Hub's User Area widget 
 * It displays the user area of the PhD Hub platform
 */
class phdhub_user_area extends WP_Widget {

	function __construct() {
		parent::__construct(
		'phdhub_user_area', 
		__('PhD Hub - User Area', 'phdhub-cpts'), 
		array( 'description' => __( 'Display the user area of the PhD Hub platform', 'phdhub-cpts' ), ) 
		);
	}

	/*
	 * The frontend section of the widget
	 */
	public function widget( $widget_args, $instance ) {
		
        extract( $widget_args );
		
		echo $widget_args['before_widget'];
		
		if ( is_user_logged_in() ) {
			$current_user = wp_get_current_user();
            $user = wp_get_current_user();
?>
<?php
    if ( user_can( $user->ID, 'publish_institutions' ) || user_can( $user->ID, 'publish_faculties' ) || user_can( $user->ID, 'publish_research_teams' ) || user_can( $user->ID, 'publish_phd_programs' ) || user_can( $user->ID, 'publish_companies' ) || user_can( $user->ID, 'publish_phd_openings' ) || user_can( $user->ID, 'publish_cooperation_calls' ) ) {
?>
<div class="user-area phdhub-forms-btn uk-hidden-small" data-uk-dropdown>
    <div>
        <i class="fa fa-plus"></i>
    </div>
    <div class="uk-dropdown">
		<ul class="user-menu">
            <?php
                if ( user_can( $user->ID, 'publish_institutions' ) ) {
            ?>
			<li><a href="<?php echo site_url() . '/profile/new-institution'; ?>"><?php echo __('Add new institution', 'phdhub-cpts'); ?></a></li>
            <?php
                }
                if ( user_can( $user->ID, 'publish_faculties' ) ) {
            ?>
			<li><a href="<?php echo site_url() . '/profile/new-faculty'; ?>"><?php echo __('Add new faculty', 'phdhub-cpts'); ?></a></li>
            <?php
                }
                if ( user_can( $user->ID, 'publish_research_teams' ) ) {
            ?>
			<li><a href="<?php echo site_url() . '/profile/new-research-team'; ?>"><?php echo __('Add new research team', 'phdhub-cpts'); ?></a></li>
            <?php
                }
                if ( user_can( $user->ID, 'publish_phd_programs' ) ) {
            ?>
			<li><a href="<?php echo site_url() . '/profile/new-phd-program'; ?>"><?php echo __('Add new PhD program', 'phdhub-cpts'); ?></a></li>
            <?php
                }
                if ( user_can( $user->ID, 'publish_companies' ) ) {
            ?>
			<li><a href="<?php echo site_url() . '/profile/new-company'; ?>"><?php echo __('Add new company', 'phdhub-cpts'); ?></a></li>
            <?php
                }
                if ( user_can( $user->ID, 'publish_phd_openings' ) ) {
            ?>
			<li><a href="<?php echo site_url() . '/profile/new-phd-offer'; ?>"><?php echo __('Add new PhD offer', 'phdhub-cpts'); ?></a></li>
            <?php
                }
                if ( user_can( $user->ID, 'publish_cooperation_calls' ) ) {
            ?>
			<li><a href="<?php echo site_url() . '/profile/new-call-for-cooperation'; ?>"><?php echo __('Add new call for cooperation', 'phdhub-cpts'); ?></a></li>
            <?php
                }
            ?>
		</ul>
	</div>
</div>
<?php
    }
?>
<div class="user-area uk-hidden-small" data-uk-dropdown>
	<div>
		<?php echo __('Welcome', 'phdhub-cpts') . ', '; ?>
		<a href="<?php echo site_url() . '/profile'; ?>"><?php echo $current_user->user_login; ?></a>
	</div>
	<div class="uk-dropdown">
		<ul class="user-menu">
			<li><a href="<?php echo site_url() . '/profile'; ?>"><?php echo __('My Profile', 'phdhub-cpts'); ?></a></li>
			<li><a href="<?php echo site_url() . '/account-settings'; ?>"><?php echo __('Account Settings', 'phdhub-cpts'); ?></a></li>
			<li><a href="<?php echo site_url() . '/password-settings'; ?>"><?php echo __('Password Settings', 'phdhub-cpts'); ?></a></li>
		</ul>
		<ul class="user-logout">
			<li><a href="<?php echo wp_logout_url(); ?>"><?php echo __('Logout', 'phdhub-cpts'); ?></a></li>
		</ul>
	</div>
</div>
<div class="user-nav uk-visible-small">
	<a href="#user-area" data-uk-offcanvas><span class="fa fa-user-circle"></span><span class="uk-hidden"><?php echo __('User Area', 'phdhub-cpts'); ?></span></a>
</div>
<div id="user-area" class="uk-offcanvas">
	<div class="uk-offcanvas-bar uk-offcanvas-bar-flip">
		<ul class="user-menu">
			<li><a href="<?php echo site_url() . '/profile'; ?>"><?php echo __('My Profile', 'phdhub-cpts'); ?></a></li>
			<li><a href="<?php echo site_url() . '/account-settings'; ?>"><?php echo __('Account Settings', 'phdhub-cpts'); ?></a></li>
		</ul>
		<ul class="user-logout">
			<li><a href="<?php echo wp_logout_url(); ?>"><?php echo __('Logout', 'phdhub-cpts'); ?></a></li>
		</ul>
	</div>
</div>
<?php	
		}
		else {
?>
<div class="user-area uk-hidden-small">
	<a href="<?php echo site_url() . '/wp-login.php'; ?>"><?php echo __('Login', 'phdhub-cpts'); ?></a> or <a href="<?php echo site_url() . '/wp-login.php?action=register'; ?>"><?php echo __('Register', 'phdhub-cpts'); ?></a>
</div>
<div class="user-nav uk-visible-small">
	<a href="#user-area-offcanvas" data-uk-offcanvas><span class="fa fa-user-circle"></span><span class="uk-hidden"><?php echo __('User Area', 'phdhub-cpts'); ?></span></a>
</div>
<div id="user-area-offcanvas" class="uk-offcanvas">
	<div class="uk-offcanvas-bar uk-offcanvas-bar-flip">
		<ul class="user-menu">
			<li><a href="<?php echo site_url() . '/wp-login.php'; ?>"><?php echo __('Login', 'phdhub-cpts'); ?></a></li>
			<li><a href="<?php echo site_url() . '/wp-login.php?action=register'; ?>"><?php echo __('Register', 'phdhub-cpts'); ?></a></li>
		</ul>
	</div>
</div>
<?php	
		}
		
		echo $widget_args['after_widget'];
	}

	/*
	 * The backend section of the widget
	 */
	public function form( $instance ) { }
	
	/*
	 * Update widget's settings
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		return $instance;
	}

}



/*
 * Create PhD Hub Research Areas widget 
 * It displays the available Research Areas as a list or grid
 */
class phdhub_research_areas extends WP_Widget {

	function __construct() {
		parent::__construct(
		'phdhub_research_areas', 
		__('PhD Hub - Research Areas', 'phdhub-cpts'), 
		array( 'description' => __( 'Display the available research areas as a list or grid', 'phdhub-cpts' ), ) 
		);
	}

	/*
	 * The frontend section of the widget
	 */
	public function widget( $widget_args, $instance ) {
		
		$title = apply_filters( 'widget_title', $instance['title'] );
		$widget_description = $instance['widget_description'];
		$style = $instance['style'];
		$empty_research_areas = $instance['empty_research_areas'];
		
        extract( $widget_args );
		
		echo $widget_args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $widget_args['before_title'];
		
			echo $title;
		
			echo $widget_args['after_title'];
		}
		
		if ( !empty($widget_description) ) {
	?>
		<p class="widget-description">
			<?php echo $widget_description; ?>
		</p>
	<?php
		}
		
		/*
		 * Include style based on the widget's settings
		 */
		if ($style == 'list-three-columns') {
			include dirname(plugin_dir_path(__DIR__)) . '/inc/widgets/research-areas/list-three-columns.php';
		}
		else if ($style == 'list-four-columns') {
			include dirname(plugin_dir_path(__DIR__)) . '/inc/widgets/research-areas/list-four-columns.php';
		}
		else if ($style == 'grid-four-columns') {
			include dirname(plugin_dir_path(__DIR__)) . '/inc/widgets/research-areas/grid-four-columns.php';
		}
		else if ($style == 'grid-five-columns') {
			include dirname(plugin_dir_path(__DIR__)) . '/inc/widgets/research-areas/grid-five-columns.php';
		}
		echo $widget_args['after_widget'];
	}

	/*
	 * The backend section of the widget
	 */
	public function form( $instance ) {
		$title = isset( $instance['title'] ) ? esc_attr( $instance[ 'title' ] ) : ' ';
		$description =  isset( $instance['widget_description'] ) ? esc_attr( $instance[ 'widget_description' ] ) : ' ';	
		$style =  isset( $instance['style'] ) ? $instance[ 'style' ] : ' ';	
		$empty_research_areas =  isset( $instance['empty_research_areas'] ) ? $instance[ 'empty_research_areas' ] : ' ';	
?>
<p>
	<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Widget Title:', 'phdhub-cpts' ); ?></label> 
	<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<p>
	<label for="<?php echo $this->get_field_id( 'widget_description' ); ?>"><?php _e( 'Description:', 'phdhub-cpts' ); ?></label> 
	<textarea class="widefat" id="<?php echo $this->get_field_id( 'widget_description' ); ?>" name="<?php echo $this->get_field_name( 'widget_description' ); ?>"><?php echo esc_attr( $description ); ?></textarea>
</p>
<p>
	<label for="<?php echo $this->get_field_id( 'style' ); ?>"><?php _e( 'Style:', 'phdhub-cpts' ); ?></label> 
	<select class="widefat" type="text" id="<?php echo $this->get_field_id( 'style' ); ?>" name="<?php echo $this->get_field_name( 'style' ); ?>">
		<option value="list-three-columns" <?php echo ($style == 'list-three-columns')?'selected':''; ?>>List - Three Columns</option>
		<option value="list-four-columns" <?php echo ($style == 'list-four-columns')?'selected':''; ?>>List - Four Columns</option>
		<option value="grid-four-columns" <?php echo ($style == 'grid-four-columns')?'selected':''; ?>>Grid - Four Columns</option>
		<option value="grid-five-columns" <?php echo ($style == 'grid-five-columns')?'selected':''; ?>>Grid - Five Columns</option>
	</select>
</p>
<p>
	<label for="<?php echo $this->get_field_id( 'empty_research_areas' ); ?>"><?php _e( 'Hide empty research areas:', 'phdhub-cpts' ); ?></label> 
	<select class="widefat" type="text" id="<?php echo $this->get_field_id( 'empty_research_areas' ); ?>" name="<?php echo $this->get_field_name( 'empty_research_areas' ); ?>">
		<option value="no" <?php echo ($empty_research_areas == 'no')?'selected':''; ?>>No</option>
		<option value="yes" <?php echo ($empty_research_areas == 'yes')?'selected':''; ?>>Yes</option>
	</select>
</p>
<?php 
	}
	
	
	/*
	 * Update widget's settings
	 */
	public function update( $new_instance, $old_instance ) {
		
		$instance = $old_instance;
    
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['widget_description'] = strip_tags( $new_instance['widget_description'] );
		$instance['style'] = $new_instance['style'];
		$instance['empty_research_areas'] = $new_instance['empty_research_areas'];
		
		return $instance;
	}

}


/*
 * Create PhD Hub PhD Openings widget 
 * It displays the available PhD Openings as a list or grid
 */
class phdhub_phd_openings extends WP_Widget {

	function __construct() {
		parent::__construct(
		'phdhub_phd_openings', 
		__('PhD Hub - Latest PhD Openings', 'phdhub-cpts'), 
		array( 'description' => __( 'Display the available PhD Openings as a list or grid', 'phdhub-cpts' ), ) 
		);
	}

	/*
	 * The frontend section of the widget
	 */
	public function widget( $widget_args, $instance ) {
		
		$title = apply_filters( 'widget_title', $instance['title'] );
		$widget_description = $instance['widget_description'];
		$number_of_openings = $instance['number_of_openings'];
		$style = $instance['style'];
		
        extract( $widget_args );
		
		echo $widget_args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $widget_args['before_title'];
		
			echo $title;
		
			echo $widget_args['after_title'];
		}
		
		if ( !empty($widget_description) ) {
	?>
		<p class="widget-description">
			<?php echo $widget_description; ?>
		</p>
	<?php
		}
		
		/*
		 * Include style based on the widget's settings
		 */
		if ($style == 'grid-two-columns') {
			include dirname(plugin_dir_path(__DIR__)) . '/inc/widgets/phd-openings/grid-two-columns.php';
		}
		else if ($style == 'grid-three-columns') {
			include dirname(plugin_dir_path(__DIR__)) . '/inc/widgets/phd-openings/grid-three-columns.php';
		}
		else if ($style == 'slider-three-columns') {
			include dirname(plugin_dir_path(__DIR__)) . '/inc/widgets/phd-openings/slider-three-columns.php';
		}
		else if ($style == 'slider-three-columns-central') {
			include dirname(plugin_dir_path(__DIR__)) . '/inc/widgets/phd-openings/slider-three-columns-central.php';
		}
		echo $widget_args['after_widget'];
	}

	/*
	 * The backend section of the widget
	 */
	public function form( $instance ) {
		$title = isset( $instance['title'] ) ? esc_attr( $instance[ 'title' ] ) : ' ';
		$description =  isset( $instance['widget_description'] ) ? esc_attr( $instance[ 'widget_description' ] ) : ' ';	
		$style =  isset( $instance['style'] ) ? $instance[ 'style' ] : ' ';	
		$number_of_openings =  isset( $instance['number_of_openings'] ) ? $instance[ 'number_of_openings' ] : ' ';	
?>
<p>
	<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Widget Title:', 'phdhub-cpts' ); ?></label> 
	<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<p>
	<label for="<?php echo $this->get_field_id( 'widget_description' ); ?>"><?php _e( 'Description:', 'phdhub-cpts' ); ?></label> 
	<textarea class="widefat" id="<?php echo $this->get_field_id( 'widget_description' ); ?>" name="<?php echo $this->get_field_name( 'widget_description' ); ?>"><?php echo esc_attr( $description ); ?></textarea>
</p>
<p>
	<label for="<?php echo $this->get_field_id( 'number_of_openings' ); ?>"><?php _e( 'Number of PhD Openings:', 'phdhub-cpts' ); ?></label> 
	<input class="widefat" id="<?php echo $this->get_field_id( 'number_of_openings' ); ?>" name="<?php echo $this->get_field_name( 'number_of_openings' ); ?>" type="number" value="<?php echo esc_attr( $number_of_openings ); ?>" />
</p>
<p>
	<label for="<?php echo $this->get_field_id( 'style' ); ?>"><?php _e( 'Style:', 'phdhub-cpts' ); ?></label> 
	<select class="widefat" type="text" id="<?php echo $this->get_field_id( 'style' ); ?>" name="<?php echo $this->get_field_name( 'style' ); ?>">
		<option value="grid-two-columns" <?php echo ($style == 'grid-two-columns')?'selected':''; ?>>Grid - Two Columns</option>
		<option value="grid-three-columns" <?php echo ($style == 'grid-three-columns')?'selected':''; ?>>Grid - Three Columns</option>
		<option value="slider-three-columns" <?php echo ($style == 'slider-three-columns')?'selected':''; ?>>Slider - Three Columns</option>
		<option value="slider-three-columns-central" <?php echo ($style == 'slider-three-columns-central')?'selected':''; ?>>Slider - Three Columns (Central Hub)</option>
	</select>
</p>
<?php 
	}
	
	
	/*
	 * Update widget's settings
	 */
	public function update( $new_instance, $old_instance ) {
		
		$instance = $old_instance;
    
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['widget_description'] = strip_tags( $new_instance['widget_description'] );
		$instance['number_of_openings'] = $new_instance['number_of_openings'];
		$instance['style'] = $new_instance['style'];
		
		return $instance;
	}

}


/*
 * Register the widgets
 */
function phdhub_widget_registration() {
	register_widget( 'phdhub_counter' );
	register_widget( 'phdhub_central_counter' );
	register_widget( 'phdhub_user_area' );
	register_widget( 'phdhub_research_areas' );
	register_widget( 'phdhub_phd_openings' );
}
add_action( 'widgets_init', 'phdhub_widget_registration' );

?>
