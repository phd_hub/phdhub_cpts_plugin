<?php

defined('ABSPATH') or die;

/* 
 * Enqueue CSS & JS Files for PhD Hub CPTs Plugin - Frontend
 */
function phdhub_cpts_scripts() {
		wp_enqueue_script( 'jquery-ui-core' );
		wp_enqueue_script( 'jquery-ui-autocomplete' );
    
		wp_register_style('phdhub-uikit', plugins_url( PHDHUB_CPTS_PLUGIN_DIR . '/assets/css/uikit.css'));
		wp_enqueue_style('phdhub-uikit');
		wp_register_style('phdhub-uikit-slider', plugins_url( PHDHUB_CPTS_PLUGIN_DIR . '/assets/css/slider.min.css'));
		wp_enqueue_style('phdhub-uikit-slider');
		wp_register_style('phdhub-uikit-slidenav', plugins_url( PHDHUB_CPTS_PLUGIN_DIR . '/assets/css/slidenav.min.css'));
		wp_enqueue_style('phdhub-uikit-slidenav');
		wp_register_style('phdhub-fontawesome', plugins_url( PHDHUB_CPTS_PLUGIN_DIR . '/assets/css/font-awesome.css'));
		wp_enqueue_style('phdhub-fontawesome');
		wp_register_style('phdhub-cpts-style', plugins_url( PHDHUB_CPTS_PLUGIN_DIR . '/assets/css/style.css'));
		wp_enqueue_style('phdhub-cpts-style');
    
		wp_register_style('phdhub-cpts-admin', plugins_url( PHDHUB_CPTS_PLUGIN_DIR . '/assets/css/phdhub-cpts-admin.css'));
		wp_enqueue_style('phdhub-cpts-admin');
		
		wp_register_style( 'leaflet-css-file', 'https://unpkg.com/leaflet@1.3.4/dist/leaflet.css' );
		wp_enqueue_style( 'leaflet-css-file' );
		wp_script_add_data( 'leaflet-css-file', array( 'integrity', 'crossorigin' ) , array( 'sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA==', '' ) );
    
		wp_enqueue_script( 'phdhub-uikit', plugins_url( PHDHUB_CPTS_PLUGIN_DIR . '/assets/js/uikit.min.js'), array('jquery'));
		wp_enqueue_script( 'phdhub-custom-scripts', plugins_url( PHDHUB_CPTS_PLUGIN_DIR . '/assets/js/scripts.js'), array('jquery'));
		wp_enqueue_script( 'phdhub-uikit-slideset', plugins_url( PHDHUB_CPTS_PLUGIN_DIR . '/assets/js/slideset.min.js'));
		wp_register_script( 'leaflet-js-file', 'https://unpkg.com/leaflet@1.3.4/dist/leaflet.js' );
		wp_enqueue_script( 'leaflet-js-file' );
		wp_script_add_data( 'leaflet-js-file', array( 'integrity', 'crossorigin' ) , array( 'sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA==', '' ) );
    
		wp_enqueue_script( 'phdhub-admin-scripts', plugins_url( PHDHUB_CPTS_PLUGIN_DIR . '/assets/js/admin-scripts.js'), array('jquery') , '1.0', true);
		
		wp_localize_script( 'phdhub-admin-scripts', 'adminscripts', array(
			'ajax_url' => admin_url( 'admin-ajax.php' )
		)); 
}
add_action( 'wp_enqueue_scripts', 'phdhub_cpts_scripts' );

/* 
 * Enqueue CSS & JS Files for PhD Hub CPTs Plugin - Backend (Admin)
 */
function phdhub_cpts_admin_scripts() {
		wp_enqueue_script( 'jquery-ui-core' );
		wp_enqueue_script( 'jquery-ui-autocomplete' );
		wp_register_style('phdhub-uikit', plugins_url( PHDHUB_CPTS_PLUGIN_DIR . '/assets/css/uikit.css'));
		wp_enqueue_style('phdhub-uikit');
		wp_register_style('phdhub-cpts-admin', plugins_url( PHDHUB_CPTS_PLUGIN_DIR . '/assets/css/phdhub-cpts-admin.css'));
		wp_enqueue_style('phdhub-cpts-admin');
		wp_enqueue_script( 'phdhub-uikit', plugins_url( PHDHUB_CPTS_PLUGIN_DIR . '/assets/js/uikit.min.js'));
		wp_enqueue_script( 'phdhub-admin-scripts', plugins_url( PHDHUB_CPTS_PLUGIN_DIR . '/assets/js/admin-scripts.js'), array('jquery') , '1.0', true);
						  
		wp_localize_script( 'phdhub-admin-scripts', 'adminscripts', array(
			'ajax_url' => admin_url( 'admin-ajax.php' )
		)); 

}
add_action( 'admin_enqueue_scripts', 'phdhub_cpts_admin_scripts' );

?>