<?php

defined('ABSPATH') or die;

/*
 * Display Custom Meta Fields for Companies CPT
 */
function display_company_meta( $company ) {
	/*
	 * Create the custom meta fields for Companies CPT
	 */
    $logo = esc_url( get_post_meta( $company->ID, 'company_logo', true ) );
    $vat_number = esc_attr( get_post_meta( $company->ID, 'vat_number', true ) );
    $ceo = esc_attr( get_post_meta( $company->ID, 'ceo', true ) );
    $establishment = esc_attr( get_post_meta( $company->ID, 'establishment', true ) );
    $employees = esc_attr( get_post_meta( $company->ID, 'employees', true ) );
    $country = esc_attr( get_post_meta( $company->ID, 'country', true ) );
    $city = esc_attr( get_post_meta( $company->ID, 'city', true ) );
	$longitude = esc_attr( get_post_meta( $company->ID, 'longitude', true ) );
	$latitude = esc_attr( get_post_meta( $company->ID, 'latitude', true ) );
	$gmap_url = esc_url( get_post_meta( $company->ID, 'gmap_url', true ) );
	$bing_map_url = esc_url( get_post_meta( $company->ID, 'bing_map_url', true ) );
	$yahoo_map_url = esc_url( get_post_meta( $company->ID, 'yahoo_map_url', true ) );
    $website = esc_attr( get_post_meta( $company->ID, 'website', true ) );
	
    $address = esc_attr( get_post_meta( $company->ID, 'address', true ) );
    $postcode = esc_attr( get_post_meta( $company->ID, 'postcode', true ) );
    $email = esc_attr( get_post_meta( $company->ID, 'email', true ) );
    $phone_numbers = esc_attr( get_post_meta( $company->ID, 'phone_numbers', true ) );
    $fax = esc_attr( get_post_meta( $company->ID, 'fax', true ) );
	$contact = 'contact_info';
    $contact_info = get_post_meta( $company->ID, 'contact_info', true );
	
	$info = 'company_info';
    $company_info = get_post_meta( $company->ID, 'company_info', true );
?>
<div class="phd-cpt-box">
	<div class="uk-grid">
		<!-- Start Left Column -->
		<div class="uk-width-1-2">
			<div class="inner-settings-box">
				<!-- Start Meta Field: Logo -->
				<p>
					<label for="company_logo">
						<input id="company_logo" type="text" size="100" name="company_logo" value="<?php echo $logo; ?>" placeholder="<?php echo __('Logo'); ?>" /><input id="company_logo_button" class="button" type="button" value="<?php echo __('Upload', 'phdhub-cpts'); ?>" />
					</label>
				</p>
				<!-- End Meta Field: Logo -->
				<!-- Start Meta Field: CEO -->
				<p>
					<label><?php echo __('CEO', 'phdhub-cpts'); ?></label>
					<input type="text" size="80" name="ceo" value="<?php echo $ceo; ?>" />
				</p>
				<!-- End Meta Field: CEO -->
				<!-- Start Meta Field: Establishment -->
				<p>
					<label><?php echo __('Established in (Year)', 'phdhub-cpts'); ?></label>
					<input type="text" size="80" name="establishment" value="<?php echo $establishment; ?>" />
				</p>
				<!-- End Meta Field: Establishment -->
				<!-- Start Meta Field: Employees -->
				<p>
					<label><?php echo __('Number of Employees', 'phdhub-cpts'); ?></label>
					<input type="text" size="80" name="employees" value="<?php echo $employees; ?>" />
				</p>
				<!-- End Meta Field: Employees -->
				<!-- Start Meta Field: Country -->
				<p>
					<label><?php echo __('Country', 'phdhub-cpts'); ?></label>
					<input type="text" size="80" name="country" value="<?php echo $country; ?>" />
				</p>
				<!-- End Meta Field: Country -->
				<!-- Start Meta Field: City -->
				<p>
					<label><?php echo __('City', 'phdhub-cpts'); ?></label>
					<input type="text" size="80" name="city" value="<?php echo $city; ?>" />
				</p>
				<!-- End Meta Field: City -->
				<!-- Start Meta Field: 	Address -->
				<p>
					<label><?php echo __('Address'); ?></label>
					<input type="text" size="160" name="address" value="<?php echo $address; ?>" />
				</p>
				<!-- End Meta Field: Address -->
				<!-- Start Meta Field: 	Postcode -->
				<p>
					<label><?php echo __('Postcode'); ?></label>
					<input type="text" size="80" name="postcode" value="<?php echo $postcode; ?>" />
				</p>
				<!-- End Meta Field: Postcode -->
				<!-- Start Meta Field: 	Email -->
				<p>
					<label><?php echo __('Email'); ?></label>
					<input type="email" size="80" name="email" value="<?php echo $email; ?>" />
				</p>
				<!-- End Meta Field: Email -->
				<!-- Start Meta Field: 	Phone Numbers -->
				<p>
					<label><?php echo __('Phone Numbers'); ?></label>
					<input type="tel" size="80" name="phone_numbers" value="<?php echo $phone_numbers; ?>" />
				</p>
				<!-- End Meta Field: Phone Numbers -->
				<!-- Start Meta Field: 	Fax -->
				<p>
					<label><?php echo __('Fax'); ?></label>
					<input type="tel" size="80" name="fax" value="<?php echo $fax; ?>" />
				</p>
				<!-- End Meta Field: Fax -->
				<!-- Start Meta Field: VAT Number -->
				<p>
					<label><?php echo __('VAT Number', 'phdhub-cpts'); ?></label>
					<input type="text" size="80" name="vat_number" value="<?php echo $vat_number; ?>" />
				</p>
				<!-- End Meta Field: VAT Number -->
				<!-- Start Meta Field: Website -->
				<p>
					<label><?php echo __('Website', 'phdhub-cpts'); ?></label>
					<input type="url" size="80" name="website" value="<?php echo $website; ?>" />
				</p>
				<!-- End Meta Field: Website -->
			</div>
			<div class="inner-settings-box">
				<p>
					<label><?php echo __('Location', 'phdhub-cpts'); ?></label>
					<label><?php echo __('Add the longitude and latitude of the location or a URL of a map.', 'phdhub-cpts'); ?></label>
				</p>
				<div class="uk-grid">
					<div class="uk-width-1-2">
						<p>
							<label><?php echo __('Longitude', 'phdhub-cpts'); ?></label>
							<input type="text" size="80" name="longitude" value="<?php echo $longitude; ?>" />
						</p>
					</div>
					<div class="uk-width-1-2">
						<p>
							<label><?php echo __('Latitude', 'phdhub-cpts'); ?></label>
							<input type="text" size="80" name="latitude" value="<?php echo $latitude; ?>" />
						</p>
					</div>
				</div>
				<p>
					<label><?php echo __('Google Map URL'); ?></label>
					<input type="url" name="gmap_url" value="<?php echo $gmap_url; ?>" />
				</p>
				<p>
					<label><?php echo __('Bing Map URL'); ?></label>
					<input type="url" name="bing_map_url" value="<?php echo $bing_map_url; ?>" />
				</p>
				<p>
					<label><?php echo __('Yahoo Map URL'); ?></label>
					<input type="url" name="yahoo_map_url" value="<?php echo $yahoo_map_url; ?>" />
				</p>
			</div>
		</div>
		<!-- End Left Column -->
		<!-- Start Right Column -->
		<div class="uk-width-1-2 inner-settings-box-right">
			<!-- Start Meta Field: General Info -->
			<p>
				<label><?php echo __('General Info', 'phdhub-cpts'); ?></label>
			</p>
			<?php wp_editor( html_entity_decode(stripcslashes($company_info)), $info ); ?>
			<!-- End Meta Field: General Info -->
			
			<!-- Start Meta Field: Contact Info -->
			<p style="margin-top: 30px">
				<label><?php echo __('Additional Contact Info', 'phdhub-cpts'); ?></label>
			</p>
			<?php wp_editor( html_entity_decode(stripcslashes($contact_info)), $contact ); ?>
			<!-- End Meta Field: Contact Info -->
		</div>
		<!-- End Right Column -->
	</div>
</div>
<?php
}



/*
 * Save Custom Meta Fields of Companies CPT
 */
function save_companies_fields( $company_id, $company ) {
    if ( $company->post_type == 'companies' ) {
		/* Save Meta Field: Logo */
        if ( isset( $_POST['company_logo'] )) {
            update_post_meta( $company->ID, 'company_logo', $_POST['company_logo'] );
        }
		/* Save Meta Field: CEO */
        if ( isset( $_POST['ceo'] )) {
            update_post_meta( $company->ID, 'ceo', $_POST['ceo'] );
        }
		/* Save Meta Field: Establishment */
        if ( isset( $_POST['establishment'] )) {
            update_post_meta( $company->ID, 'establishment', $_POST['establishment'] );
        }
		/* Save Meta Field: Employees */
        if ( isset( $_POST['employees'] )) {
            update_post_meta( $company->ID, 'employees', $_POST['employees'] );
        }
		/* Save Meta Field: Country */
        if ( isset( $_POST['country'] )) {
            update_post_meta( $company->ID, 'country', $_POST['country'] );
        }
		/* Save Meta Field: City */
        if ( isset( $_POST['city'] )) {
            update_post_meta( $company->ID, 'city', $_POST['city'] );
        }
		/* Save Meta Field: Website */
        if ( isset( $_POST['website'] )) {
            update_post_meta( $company->ID, 'website', $_POST['website'] );
        }
		/* Save Meta Field: VAT Number */
        if ( isset( $_POST['vat_number'] )) {
            update_post_meta( $company->ID, 'vat_number', $_POST['vat_number'] );
        }
		/* Save Meta Field: Contact Info */
        if ( isset( $_POST['contact_info'] )) {
            update_post_meta( $company->ID, 'contact_info', $_POST['contact_info'] );
        }
		/* Save Meta Field: General Info */
        if ( isset( $_POST['company_info'] )) {
            update_post_meta( $company->ID, 'company_info', $_POST['company_info'] );
        }
		/* Save Meta Field: Address */
        if ( isset( $_POST['address'] )) {
            update_post_meta( $company->ID, 'address', $_POST['address'] );
        }
		/* Save Meta Field: Postcode */
        if ( isset( $_POST['postcode'] )) {
            update_post_meta( $company->ID, 'postcode', $_POST['postcode'] );
        }
		/* Save Meta Field: Email */
        if ( isset( $_POST['email'] )) {
            update_post_meta( $company->ID, 'email', $_POST['email'] );
        }
		/* Save Meta Field: Phone Numbers */
        if ( isset( $_POST['phone_numbers'] )) {
            update_post_meta( $company->ID, 'phone_numbers', $_POST['phone_numbers'] );
        }
		/* Save Meta Field: Fax */
        if ( isset( $_POST['fax'] )) {
            update_post_meta( $company->ID, 'fax', $_POST['fax'] );
        }
		/* Save Meta Field: Longitude */
        if ( isset( $_POST['longitude'] )) {
            update_post_meta( $company->ID, 'longitude', $_POST['longitude'] );
        }
		/* Save Meta Field: Latitude */
        if ( isset( $_POST['latitude'] )) {
            update_post_meta( $company->ID, 'latitude', $_POST['latitude'] );
        }
		/* Save Meta Field: Google Map URL */
        if ( isset( $_POST['gmap_url'] )) {
            update_post_meta( $company->ID, 'gmap_url', $_POST['gmap_url'] );
			if ($_POST['gmap_url'] != NULL) {
				$pts = explode('/',$_POST['gmap_url']);
				$cords = explode(',',$pts[6]);
				$lon = str_replace('@','',$cords[0]);
				$lat = $cords[1];
				update_post_meta( $company->ID, 'longitude', $lon );
				update_post_meta( $company->ID, 'latitude', $lat );
			}
        }
		/* Save Meta Field: Bing Map URL */
        if ( isset( $_POST['bing_map_url'] )) {
            update_post_meta( $company->ID, 'bing_map_url', $_POST['bing_map_url'] );
			if ($_POST['bing_map_url'] != NULL) {
				$pts2 = explode('=', $_POST['bing_map_url']);
				$cords2 = str_replace('~',',',$pts2[2]);
				$cords2 = explode('&', $cords2);
				$cords2 = explode(',', $cords2[0]);
				$lon2 = $cords2[0];
				$lat2 = $cords2[1];
				update_post_meta( $company->ID, 'longitude', $lon2 );
				update_post_meta( $company->ID, 'latitude', $lat2 );
			}
        }
		/* Save Meta Field: Yahoo Map URL */
        if ( isset( $_POST['yahoo_map_url'] )) {
            update_post_meta( $company->ID, 'yahoo_map_url', $_POST['yahoo_map_url'] );
			if ($_POST['yahoo_map_url'] != NULL) {
				$url3 = parse_url($_POST['yahoo_map_url']);
				$pts3 = explode('&', $url3['query']);
				$lon3 = explode('=', $pts3[1]);
				$lon3 = $lon3[1];
				$lat3 = explode('=', $pts3[0]);
				$lat3 = $lat3[1];
				update_post_meta( $company->ID, 'longitude', $lon3 );
				update_post_meta( $company->ID, 'latitude', $lat3 );
			}
        }
    }
}
add_action( 'save_post', 'save_companies_fields', 10, 2 );

?>