<?php

defined('ABSPATH') or die;

/*
 * Display Custom Meta Fields for Faculties CPT
 */
function display_faculty_meta( $faculty ) {
	/*
	 * Create the custom meta fields for Faculties CPT
	 */
    $logo = esc_attr( get_post_meta( $faculty->ID, 'faculty_logo', true ) );
    $institution = esc_attr( get_post_meta( $faculty->ID, 'institution', true ) );
    $institution_label = esc_attr( get_post_meta( $faculty->ID, 'institution_label', true ) );
    $professors = esc_attr( get_post_meta( $faculty->ID, 'professors', true ) );
    $students = esc_attr( get_post_meta( $faculty->ID, 'students', true ) );
    $website = esc_attr( get_post_meta( $faculty->ID, 'website', true ) );
	
    $address = esc_attr( get_post_meta( $faculty->ID, 'address', true ) );
    $postcode = esc_attr( get_post_meta( $faculty->ID, 'postcode', true ) );
    $email = esc_attr( get_post_meta( $faculty->ID, 'email', true ) );
    $phone_numbers = esc_attr( get_post_meta( $faculty->ID, 'phone_numbers', true ) );
    $fax = esc_attr( get_post_meta( $faculty->ID, 'fax', true ) );
	$contact = 'contact_info';
    $contact_info = get_post_meta( $faculty->ID, 'contact_info', true );
	
	$info = 'faculty_info';
    $faculty_info = get_post_meta( $faculty->ID, 'faculty_info', true );
?>
<div class="phd-cpt-box">
	<div class="uk-grid">
		<!-- Start Left Column -->
		<div class="uk-width-1-2">
			<div class="inner-settings-box">
				<!-- Start Meta Field: Logo -->
				<p>
					<label for="faculty_logo">
						<input id="faculty_logo" type="text" size="100" name="faculty_logo" value="<?php echo $logo; ?>"  placeholder="<?php echo __('Logo'); ?>" /><input id="faculty_logo_button" class="button" type="button" value="<?php echo __('Upload', 'phdhub-cpts'); ?>" />
					</label>
				</p>
				<!-- End Meta Field: Logo -->
				<!-- Start Meta Field: Institution -->
				<p>
					<label><?php echo __('Institution', 'phdhub-cpts'); ?></label>
					<input type="text" name="institution_label" id="institution-autocomplete" value="<?php echo $institution_label; ?>" placeholder="<?php echo __('Search by typing institution name', 'phdhub-cpts'); ?>..." required />
					<input type="hidden" name="institution" value="<?php echo $institution; ?>" required />
				</p>
				<!-- End Meta Field: Institution -->
				<!-- Start Meta Field: Professors -->
				<p>
					<label><?php echo __('Number of Professors', 'phdhub-cpts'); ?></label>
					<input type="text" size="80" name="professors" value="<?php echo $professors; ?>" />
				</p>
				<!-- End Meta Field: Professors -->
				<!-- Start Meta Field: Students -->
				<p>
					<label><?php echo __('Number of Students', 'phdhub-cpts'); ?></label>
					<input type="text" size="80" name="students" value="<?php echo $students; ?>" />
				</p>
				<!-- End Meta Field: Students -->
				<!-- Start Meta Field: Website -->
				<p>
					<label><?php echo __('Website', 'phdhub-cpts'); ?></label>
					<input type="text" size="80" name="website" value="<?php echo $website; ?>" />
				</p>
				<!-- End Meta Field: Website -->
				<!-- Start Meta Field: 	Address -->
				<p>
					<label><?php echo __('Address'); ?></label>
					<input type="text" size="160" name="address" value="<?php echo $address; ?>" />
				</p>
				<!-- End Meta Field: Address -->
				<!-- Start Meta Field: 	Postcode -->
				<p>
					<label><?php echo __('Postcode'); ?></label>
					<input type="text" size="80" name="postcode" value="<?php echo $postcode; ?>" />
				</p>
				<!-- End Meta Field: Postcode -->
				<!-- Start Meta Field: 	Email -->
				<p>
					<label><?php echo __('Email'); ?></label>
					<input type="text" size="80" name="email" value="<?php echo $email; ?>" />
				</p>
				<!-- End Meta Field: Email -->
				<!-- Start Meta Field: 	Phone Numbers -->
				<p>
					<label><?php echo __('Phone Numbers'); ?></label>
					<input type="text" size="80" name="phone_numbers" value="<?php echo $phone_numbers; ?>" />
				</p>
				<!-- End Meta Field: Phone Numbers -->
				<!-- Start Meta Field: 	Fax -->
				<p>
					<label><?php echo __('Fax'); ?></label>
					<input type="text" size="80" name="fax" value="<?php echo $fax; ?>" />
				</p>
				<!-- End Meta Field: Fax -->
			</div>
		</div>
		<!-- End Left Column -->
		<!-- Start Right Column -->
		<div class="uk-width-1-2 inner-settings-box-right">
			<!-- Start Meta Field: General Info -->
			<p>
				<label><?php echo __('General Info', 'phdhub-cpts'); ?></label>
			</p>
			<?php wp_editor( html_entity_decode(stripcslashes($faculty_info)), $info ); ?>
			<!-- End Meta Field: General Info -->
			
			<!-- Start Meta Field: Contact Info -->
			<p>
				<label><?php echo __('Additional Contact Info', 'phdhub-cpts'); ?></label>
			</p>
			<?php wp_editor( html_entity_decode(stripcslashes($contact_info)), $contact ); ?>
			<!-- End Meta Field: Contact Info -->
		</div>
		<!-- End Right Column -->
	</div>
</div>
<?php
}



/*
 * Save Custom Meta Fields of Faculties CPT
 */
function save_faculty_fields( $faculty_id, $faculty ) {
    if ( $faculty->post_type == 'faculties' ) {
		/* Save Meta Field: Logo */
        if ( isset( $_POST['faculty_logo'] )) {
            update_post_meta( $faculty->ID, 'faculty_logo', $_POST['faculty_logo'] );
        }
		/* Save Meta Field: Institution */
        if ( isset( $_POST['institution'] )) {
            update_post_meta( $faculty->ID, 'institution', $_POST['institution'] );
        }
		/* Save Meta Field: Institution Label */
        if ( isset( $_POST['institution_label'] )) {
            update_post_meta( $faculty->ID, 'institution_label', $_POST['institution_label'] );
        }
		/* Save Meta Field: Professors */
        if ( isset( $_POST['professors'] )) {
            update_post_meta( $faculty->ID, 'professors', $_POST['professors'] );
        }
		/* Save Meta Field: Students */
        if ( isset( $_POST['students'] )) {
            update_post_meta( $faculty->ID, 'students', $_POST['students'] );
        }
		/* Save Meta Field: Website */
        if ( isset( $_POST['website'] )) {
            update_post_meta( $faculty->ID, 'website', $_POST['website'] );
        }
		/* Save Meta Field: Faculty Info */
        if ( isset( $_POST['faculty_info'] )) {
            update_post_meta( $faculty->ID, 'faculty_info', $_POST['faculty_info'] );
        }
		/* Save Meta Field: Contact Info */
        if ( isset( $_POST['contact_info'] )) {
            update_post_meta( $faculty->ID, 'contact_info', $_POST['contact_info'] );
        }
		/* Save Meta Field: Address */
        if ( isset( $_POST['address'] )) {
            update_post_meta( $faculty->ID, 'address', $_POST['address'] );
        }
		/* Save Meta Field: Postcode */
        if ( isset( $_POST['postcode'] )) {
            update_post_meta( $faculty->ID, 'postcode', $_POST['postcode'] );
        }
		/* Save Meta Field: Email */
        if ( isset( $_POST['email'] )) {
            update_post_meta( $faculty->ID, 'email', $_POST['email'] );
        }
		/* Save Meta Field: Phone Numbers */
        if ( isset( $_POST['phone_numbers'] )) {
            update_post_meta( $faculty->ID, 'phone_numbers', $_POST['phone_numbers'] );
        }
		/* Save Meta Field: Fax */
        if ( isset( $_POST['fax'] )) {
            update_post_meta( $faculty->ID, 'fax', $_POST['fax'] );
        }
    }
}
add_action( 'save_post', 'save_faculty_fields', 10, 2 );

?>