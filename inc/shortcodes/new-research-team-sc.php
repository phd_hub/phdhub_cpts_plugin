<?php
	/*
	 * PhDHub CPTs plugin - Shortcodes
	 * Frontend form: Create Research Team Profile
	 */
	defined('ABSPATH') or die;


	if ( isset( $_POST['submitted'] )) {
		
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		require_once( ABSPATH . 'wp-admin/includes/media.php' );

		if ( trim( $_POST['postTitle'] ) === '' ) {
			$postTitleError = 'Please enter a title.';
			$hasError = true;
		}
		
		$post_information = array(
			'post_title' => wp_strip_all_tags( $_POST['team_name'] ),
			'post_type' => 'research-teams',
			'post_status' => 'publish',
		);

		$post_id = wp_insert_post( $post_information );
		$attachment_id = media_handle_upload('team_bg',$post_id);
		set_post_thumbnail($post_id, $attachment_id); 
	}


	$info = 'team_info';
    $team_info = '';


	$user = wp_get_current_user();
	if ( is_user_logged_in() ) {
        if ( user_can( $user->ID, 'publish_research_teams' ) ) {
?>
<form id="research_teams_meta" class="phdhub-form-sc" method="POST" enctype="multipart/form-data">
    <div class="phd-cpt-box">
        <div class="uk-grid">
            <!-- Start Left Column -->
            <div class="uk-width-1-2">
                <div class="inner-settings-box">
                    <p>
                        <label><?php echo __('Research Team\'s Name', 'phdhub-cpts'); ?></label>
                        <input type="text" size="80" name="team_name" />
                    </p>
                    <!-- Start Meta Field: Logo and Image -->
                    <p>
                        <label for="team_bg"><?php echo __('Background Image', 'phdhub-cpts'); ?>*</label>
                        <input type="file" name="team_bg" multiple="false">
                    </p>
                    <p>
                        <label for="research_team_logo">
                            <input id="research_team_logo" type="text" size="100" name="research_team_logo" placeholder="<?php echo __('Logo', 'phdhub-cpts'); ?>" /><input id="research_team_logo_button" class="button" type="button" value="<?php echo __('Upload', 'phdhub-cpts'); ?>" />
                        </label>
                    </p>
                    <!-- End Meta Field: Logo -->
                    <!-- Start Meta Field: Institution -->
                    <p>
                        <label><?php echo __('Institution', 'phdhub-cpts'); ?></label>
                        <input type="text" name="institution_label" id="institution-autocomplete" placeholder="<?php echo __('Search by typing institution name', 'phdhub-cpts'); ?>..." required />
                        <input type="hidden" name="institution" value="<?php echo $institution; ?>" required />
                    </p>
                    <!-- End Meta Field: Institution -->
                    <!-- Start Meta Field: Faculty -->
                    <p>
                        <label><?php echo __('Faculty', 'phdhub-cpts'); ?></label>
                        <select id="faculty" name="faculty">
                            <option value="empty" <?php selected( $faculty, 'empty' ); ?>><?php echo __('Select Faculty'); ?></option>
                            <?php institution_faculty_list(); ?>
                        </select>
                    </p>
                    <!-- End Meta Field: Faculty -->
                    <!-- Start Meta Field: PhD Program -->
                    <p>
                        <label><?php echo __('PhD Program', 'phdhub-cpts'); ?></label>
                        <select id="phd-program" name="phd_program">
                            <option value="empty" <?php selected( $phd_program, 'empty' ); ?>><?php echo __('Select PhD Program'); ?></option>
                            <?php institution_phd_programs_list(); ?>
                        </select>
                    </p>
                    <!-- End Meta Field: PhD Program -->
                    <!-- Start Meta Field: Website -->
                    <p>
                        <label><?php echo __('Website', 'phdhub-cpts'); ?></label>
                        <input type="text" size="80" name="website" />
                    </p>
                    <!-- End Meta Field: Website -->
                </div>
            </div>
            <!-- End Left Column -->
            <!-- Start Right Column -->
            <div class="uk-width-1-2 inner-settings-box-right">
                <!-- Start Meta Field: General Info -->
                <p>
                    <label><?php echo __('General Info', 'phdhub-cpts'); ?></label>
                </p>
                <?php wp_editor( html_entity_decode(stripcslashes($team_info)), $info ); ?>
                <!-- End Meta Field: General Info -->
            </div>
            <!-- End Right Column -->
        </div>
    </div>
    
    <input type="hidden" name="submitted" id="submitted" value="true" />
    <button type="submit" class="submit-button"><?php echo __('Submit', 'phdhub-cpts') ?></button>
</form>
<?php
        } else {
            wp_redirect( home_url() );
            exit;
        }
    } else {
        wp_redirect( home_url() );
        exit;
    }
?>