<?php
	/*
	 * PhDHub CPTs plugin - Shortcodes
	 * Frontend form: Create PhD Program Page
	 */
	defined('ABSPATH') or die;


	if ( isset( $_POST['submitted'] )) {
		
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		require_once( ABSPATH . 'wp-admin/includes/media.php' );

		if ( trim( $_POST['postTitle'] ) === '' ) {
			$postTitleError = 'Please enter a title.';
			$hasError = true;
		}
		
		$post_information = array(
			'post_title' => wp_strip_all_tags( $_POST['program_title'] ),
			'post_type' => 'phd-programs',
			'post_status' => 'publish',
		);

		$post_id = wp_insert_post( $post_information );
		$attachment_id = media_handle_upload('program_bg',$post_id);
		set_post_thumbnail($post_id, $attachment_id); 
	}


	$info = 'phd_program_info';
    $phd_program_info = '';

	$user = wp_get_current_user();
	if ( is_user_logged_in() ) {
        if ( user_can( $user->ID, 'publish_phd_programs' ) ) {
?>
<form id="phd_program_meta" class="phdhub-form-sc" method="POST" enctype="multipart/form-data">
    <div class="phd-cpt-box">
        <div class="uk-grid">
            <!-- Start Left Column -->
            <div class="uk-width-1-2">
                <div class="inner-settings-box">
                    <p>
                        <label><?php echo __('PhD Program\'s Title', 'phdhub-cpts'); ?>*</label>
                        <input type="text" size="80" name="program_title" />
                    </p>
                    <!-- Start Meta Field: Background Image -->
                    <p>
                        <label for="program_bg"><?php echo __('Background Image', 'phdhub-cpts'); ?>*</label>
                        <input type="file" name="program_bg" multiple="false">
                    </p>
                    <!-- Start Meta Field: Institution -->
                    <p>
                        <label><?php echo __('Institution', 'phdhub-cpts'); ?>*</label>
                        <input type="text" name="institution_label" id="institution-autocomplete" placeholder="<?php echo __('Search by typing institution name', 'phdhub-cpts'); ?>..." required />
                        <input type="hidden" name="institution" value="<?php echo $institution; ?>" required />
                    </p>
                    <!-- End Meta Field: Institution -->
                    <!-- Start Meta Field: Faculty -->
                    <p>
                        <label><?php echo __('Faculty', 'phdhub-cpts'); ?>*</label>
                        <select id="faculty" name="faculty">
                            <option value="empty" <?php selected( $faculty, 'empty' ); ?>><?php echo __('Select Faculty'); ?></option>
                            <?php institution_faculty_list(); ?>
                        </select>
                    </p>
                    <!-- End Meta Field: Faculty -->
                    <!-- Start Meta Field: Website -->
                    <p>
                        <label><?php echo __('Website', 'phdhub-cpts'); ?></label>
                        <input type="text" size="80" name="website" />
                    </p>
                    <!-- End Meta Field: Website -->
                </div>
            </div>
            <!-- End Left Column -->
            <!-- Start Right Column -->
            <div class="uk-width-1-2 inner-settings-box-right">
                <!-- Start Meta Field: General Info -->
                <p>
                    <label><?php echo __('General Info', 'phdhub-cpts'); ?></label>
                </p>
                <?php wp_editor( html_entity_decode(stripcslashes($phd_program_info)), $info ); ?>
                <!-- End Meta Field: General Info -->
            </div>
            <!-- End Right Column -->
        </div>
    </div>
    
    <input type="hidden" name="submitted" id="submitted" value="true" />
    <button type="submit" class="submit-button"><?php echo __('Submit', 'phdhub-cpts') ?></button>
</form>
<?php
        } else {
            wp_redirect( home_url() );
            exit;
        }
    } else {
        wp_redirect( home_url() );
        exit;
    }
?>