<?php
	/*
	 * PhDHub CPTs plugin - Shortcodes
	 * Frontend form: Create Call for Cooperation Page
	 */
	defined('ABSPATH') or die;


	if ( isset( $_POST['submitted'] )) {
		
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		require_once( ABSPATH . 'wp-admin/includes/media.php' );

		if ( trim( $_POST['postTitle'] ) === '' ) {
			$postTitleError = 'Please enter a title.';
			$hasError = true;
		}
		
		$post_information = array(
			'post_title' => wp_strip_all_tags( $_POST['call_title'] ),
			'post_type' => 'cooperation-calls',
			'post_status' => 'publish',
		);

		$post_id = wp_insert_post( $post_information );
	}


	$info = 'call_info';
    $call_info = '';


	$user = wp_get_current_user();
	if ( is_user_logged_in() ) {
        if ( user_can( $user->ID, 'publish_cooperation_calls' ) ) {
?>
<form id="cooperation_calls_meta" class="phdhub-form-sc" method="POST" enctype="multipart/form-data">
    <div class="phd-cpt-box">
        <div class="uk-grid">
            <!-- Start Left Column -->
            <div class="uk-width-1-2">
                <div class="inner-settings-box">
                    <p>
                        <label><?php echo __('Call for Cooperation Title', 'phdhub-cpts'); ?></label>
                        <input type="text" name="faculty_name" />
                    </p>
                    <!-- Start Meta Field: Company -->
                    <p>
                        <label><?php echo __('Company', 'phdhub-cpts'); ?></label>
                        <input type="text" name="company_label" id="company-autocomplete" placeholder="<?php echo __('Search by typing company\'s name', 'phdhub-cpts'); ?>..." required />
                        <input type="hidden" name="company" value="<?php echo $company; ?>" />
                    </p>
                    <!-- End Meta Field: Company -->
                    <p>
                        <label><?php echo __('and/or', 'phdhub-cpts'); ?></label>
                    </p>
                    <!-- Start Meta Field: Institution -->
                    <p>
                        <label><?php echo __('Institution', 'phdhub-cpts'); ?></label>
                        <input type="text" name="institution_label" id="institution-autocomplete" placeholder="<?php echo __('Search by typing institution name', 'phdhub-cpts'); ?>..." />
                        <input type="hidden" name="institution" value="<?php echo $institution; ?>" />
                    </p>
                    <!-- End Meta Field: Institution -->
                    <!-- Start Meta Field: Faculty -->
                    <p>
                        <label><?php echo __('Faculty', 'phdhub-cpts'); ?></label>
                        <select id="faculty" name="faculty">
                            <option value="empty" <?php selected( $faculty, 'empty' ); ?>><?php echo __('Select Faculty'); ?></option>
                            <?php institution_faculty_list(); ?>
                        </select>
                    </p>
                    <!-- End Meta Field: Faculty -->
                    <!-- Start Meta Field: PhD Program -->
                    <p>
                        <label><?php echo __('PhD Program', 'phdhub-cpts'); ?></label>
                        <select id="phd-program" name="phd_program">
                            <option value="empty" <?php selected( $phd_program, 'empty' ); ?>><?php echo __('Select PhD Program'); ?></option>
                            <?php institution_phd_programs_list(); ?>
                        </select>
                    </p>
                    <!-- End Meta Field: PhD Program -->
                    <!-- Start Meta Field: PhD Opening -->
                    <p>
                        <label><?php echo __('PhD Opening', 'phdhub-cpts'); ?></label>
                        <select id="phd-opening" name="phd_opening">
                            <option value="empty" <?php selected( $phd_opening, 'empty' ); ?>><?php echo __('Select PhD Opening'); ?></option>
                            <?php  institution_phd_openings_list(); ?>
                        </select>
                    </p>
                    <!-- End Meta Field: PhD Opening -->
                    <!-- Start Meta Field: Project Coordinator -->
                    <p>
                        <label><?php echo __('Project Coordinator', 'phdhub-cpts'); ?></label>
                        <input type="text" size="80"  id="supervisor-autocomplete" name="project_coordinator" placeholder="<?php echo __('Search by typing project coordinator\'s name'); ?>..." />
                    </p>
                    <!-- End Meta Field: Project Coordinator -->
                    <!-- Start Meta Field: Short Info -->
                    <p>
                        <label><?php echo __('Short Info', 'phdhub-cpts'); ?></label>
                        <textarea rows="5" name="short_info"></textarea>
                    </p>
                    <!-- End Meta Field: Short Info -->
                    <!-- Start Meta Field: Email Address -->
                    <p>
                        <label><?php echo __('Email Address (for notifications about expressions of interest)', 'phdhub-cpts'); ?></label>
                        <input type="email" size="80" name="email_address" />
                    </p>
                    <!-- End Meta Field: Short Info -->
                </div>
            </div>
            <!-- End Left Column -->
            <!-- Start Right Column -->
            <div class="uk-width-1-2 inner-settings-box-right">
                <!-- Start Meta Field: General Info -->
                <p>
                    <label><?php echo __('General Info', 'phdhub-cpts'); ?></label>
                </p>
                <?php wp_editor( html_entity_decode(stripcslashes($call_info)), $info ); ?>
                <!-- End Meta Field: General Info -->
            </div>
            <!-- End Right Column -->
        </div>
    </div>
    
    <input type="hidden" name="submitted" id="submitted" value="true" />
    <button type="submit" class="submit-button"><?php echo __('Submit', 'phdhub-cpts') ?></button>
</form>
<?php
        } else {
            wp_redirect( home_url() );
            exit;
        }
    } else {
        wp_redirect( home_url() );
        exit;
    }
?>