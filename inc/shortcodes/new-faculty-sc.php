<?php
	/*
	 * PhDHub CPTs plugin - Shortcodes
	 * Frontend form: Create Faculty Profile
	 */
	defined('ABSPATH') or die;


	if ( isset( $_POST['submitted'] )) {
		
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		require_once( ABSPATH . 'wp-admin/includes/media.php' );

		if ( trim( $_POST['postTitle'] ) === '' ) {
			$postTitleError = 'Please enter a title.';
			$hasError = true;
		}
		
		$post_information = array(
			'post_title' => wp_strip_all_tags( $_POST['faculty_name'] ),
			'post_type' => 'faculties',
			'post_status' => 'publish',
		);

		$post_id = wp_insert_post( $post_information );
		$attachment_id = media_handle_upload('faculty_bg',$post_id);
		set_post_thumbnail($post_id, $attachment_id); 
	}


	$contact = 'contact_info';
    $contact_info = '';
	$info = 'faculty_info';
    $faculty_info = '';


	$user = wp_get_current_user();
	if ( is_user_logged_in() ) {
        if ( user_can( $user->ID, 'publish_faculties' ) ) {
?>
<form id="faculty_meta" class="phdhub-form-sc" method="POST" enctype="multipart/form-data">
    <div class="phd-cpt-box">
        <div class="uk-grid">
            <!-- Start Left Column -->
            <div class="uk-width-1-2">
                <div class="inner-settings-box">
                    <p>
                        <label><?php echo __('Faculty\'s Name', 'phdhub-cpts'); ?></label>
                        <input type="text" size="80" name="faculty_name" />
                    </p>
                    <!-- Start Meta Field: Logo and Image -->
                    <p>
                        <label for="faculty_bg"><?php echo __('Background Image', 'phdhub-cpts'); ?>*</label>
                        <input type="file" name="faculty_bg" multiple="false">
                    </p>
                    <p>
                        <label for="faculty_logo">
                            <input id="faculty_logo" type="text" size="100" name="faculty_logo" placeholder="<?php echo __('Logo'); ?>*" /><input id="faculty_logo_button" class="button" type="button" value="<?php echo __('Upload', 'phdhub-cpts'); ?>" />
                        </label>
                    </p>
                    <!-- End Meta Field: Logo -->
                    <!-- Start Meta Field: Institution -->
                    <p>
                        <label><?php echo __('Institution', 'phdhub-cpts'); ?>*</label>
                        <input type="text" name="institution_label" id="institution-autocomplete" placeholder="<?php echo __('Search by typing institution name', 'phdhub-cpts'); ?>..." required />
                        <input type="hidden" name="institution" required />
                    </p>
                    <!-- End Meta Field: Institution -->
                    <!-- Start Meta Field: Professors -->
                    <p>
                        <label><?php echo __('Number of Professors', 'phdhub-cpts'); ?></label>
                        <input type="text" size="80" name="professors" />
                    </p>
                    <!-- End Meta Field: Professors -->
                    <!-- Start Meta Field: Students -->
                    <p>
                        <label><?php echo __('Number of Students', 'phdhub-cpts'); ?></label>
                        <input type="text" size="80" name="students" />
                    </p>
                    <!-- End Meta Field: Students -->
                    <!-- Start Meta Field: Website -->
                    <p>
                        <label><?php echo __('Website', 'phdhub-cpts'); ?></label>
                        <input type="text" size="80" name="website" />
                    </p>
                    <!-- End Meta Field: Website -->
                    <!-- Start Meta Field: 	Address -->
                    <p>
                        <label><?php echo __('Address'); ?></label>
                        <input type="text" size="160" name="address" />
                    </p>
                    <!-- End Meta Field: Address -->
                    <!-- Start Meta Field: 	Postcode -->
                    <p>
                        <label><?php echo __('Postcode'); ?></label>
                        <input type="text" size="80" name="postcode" />
                    </p>
                    <!-- End Meta Field: Postcode -->
                    <!-- Start Meta Field: 	Email -->
                    <p>
                        <label><?php echo __('Email'); ?></label>
                        <input type="text" size="80" name="email" />
                    </p>
                    <!-- End Meta Field: Email -->
                    <!-- Start Meta Field: 	Phone Numbers -->
                    <p>
                        <label><?php echo __('Phone Numbers'); ?></label>
                        <input type="text" size="80" name="phone_numbers" />
                    </p>
                    <!-- End Meta Field: Phone Numbers -->
                    <!-- Start Meta Field: 	Fax -->
                    <p>
                        <label><?php echo __('Fax'); ?></label>
                        <input type="text" size="80" name="fax" />
                    </p>
                    <!-- End Meta Field: Fax -->
                </div>
            </div>
            <!-- End Left Column -->
            <!-- Start Right Column -->
            <div class="uk-width-1-2 inner-settings-box-right">
                <!-- Start Meta Field: General Info -->
                <p>
                    <label><?php echo __('General Info', 'phdhub-cpts'); ?></label>
                </p>
                <?php wp_editor( html_entity_decode(stripcslashes($faculty_info)), $info ); ?>
                <!-- End Meta Field: General Info -->

                <!-- Start Meta Field: Contact Info -->
                <p>
                    <label><?php echo __('Additional Contact Info', 'phdhub-cpts'); ?></label>
                </p>
                <?php wp_editor( html_entity_decode(stripcslashes($contact_info)), $contact ); ?>
                <!-- End Meta Field: Contact Info -->
            </div>
            <!-- End Right Column -->
        </div>
    </div>
    
    <input type="hidden" name="submitted" id="submitted" value="true" />
    <button type="submit" class="submit-button"><?php echo __('Submit', 'phdhub-cpts') ?></button>
</form>
<?php
        } else {
            wp_redirect( home_url() );
            exit;
        }
    } else {
        wp_redirect( home_url() );
        exit;
    }
?>