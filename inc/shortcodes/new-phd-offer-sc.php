<?php
	/*
	 * PhDHub CPTs plugin - Shortcodes
	 * Frontend form: Create PhD Offer Page
	 */
	defined('ABSPATH') or die;


	if ( isset( $_POST['submitted'] )) {
		
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		require_once( ABSPATH . 'wp-admin/includes/media.php' );

		if ( trim( $_POST['postTitle'] ) === '' ) {
			$postTitleError = 'Please enter a title.';
			$hasError = true;
		}
		
		$post_information = array(
			'post_title' => wp_strip_all_tags( $_POST['offer_title'] ),
			'post_type' => 'phd-openings',
			'post_status' => 'publish',
		);

		$post_id = wp_insert_post( $post_information );
	}


	$info = 'phd_opening_info';
    $phd_opening_info = '';


	$user = wp_get_current_user();
	if ( is_user_logged_in() ) {
        if ( user_can( $user->ID, 'publish_phd_openings' ) ) {
?>
<form id="phd_opening_meta" class="phdhub-form-sc" method="POST" enctype="multipart/form-data">
    <div class="phd-cpt-box">
        <ul id="phdhub-cpt-nav" data-uk-switcher="{connect: '#offer-settings'}">
            <li><a href=""><?php echo __('Description', 'phdhub-cpts'); ?></a></li>
            <li><a href=""><?php echo __('Basic Details', 'phdhub-cpts'); ?></a></li>
            <li><a href=""><?php echo __('More Info', 'phdhub-cpts'); ?></a></li>
        </ul>

        <ul id="offer-settings" class="uk-switcher">
            <li>
                <p>
                    <label><?php echo __('PhD Offer\'s Title', 'phdhub-cpts'); ?>*</label>
                    <input type="text" name="offer_title" />
                </p>
                <?php wp_editor( html_entity_decode(stripcslashes($phd_opening_info)), $info ); ?>
            </li>
            <li>
                <div class="uk-grid">
                    <div class="uk-width-1-2">
                        <div class="inner-settings-box">
                            <input type="hidden" id="phd-id" value="<?php echo $phd_opening->ID; ?>" />
                            <!-- Start Meta Field: Institution -->
                            <p>
                                <label><?php echo __('Institution', 'phdhub-cpts'); ?>*</label>
                                <input type="text" name="institution_label" id="institution-autocomplete" placeholder="<?php echo __('Search by typing institution name', 'phdhub-cpts'); ?>..." required />
                                <input type="hidden" name="institution" value="<?php echo $institution; ?>" required />
                            </p>
                            <!-- End Meta Field: Institution -->
                            <!-- Start Meta Field: Faculty -->
                            <p>
                                <label><?php echo __('Faculty', 'phdhub-cpts'); ?>*</label>
                                <select id="faculty" name="faculty">
                                    <option value="empty" <?php selected( $faculty, 'empty' ); ?>><?php echo __('Select an institution before selecting a faculty', 'phdhub-cpts'); ?></option>
                                    <?php institution_faculty_list( ); ?>
                                </select>
                            </p>
                            <!-- End Meta Field: Faculty -->
                            <!-- Start Meta Field: PhD Program -->
                            <p>
                                <label><?php echo __('PhD Program', 'phdhub-cpts'); ?></label>
                                <select id="phd-program" name="phd_program">
                                    <option value="empty" <?php selected( $phd_program, 'empty' ); ?>><?php echo __('Select PhD Program', 'phdhub-cpts'); ?></option>
                                    <?php institution_phd_programs_list( ); ?>
                                </select>
                            </p>
                            <!-- End Meta Field: PhD Program -->
                            <!-- Start Meta Field: Research Team -->
                            <p>
                                <label><?php echo __('Research Team', 'phdhub-cpts'); ?></label>
                                <select id="research-team" name="research_team">
                                    <option value="empty" <?php selected( $research_team, 'empty' ); ?>><?php echo __('Select Research Team', 'phdhub-cpts'); ?></option>
                                    <?php institution_research_teams_list( ); ?>
                                </select>
                            </p>
                            <!-- End Meta Field: Research Team -->
                        </div>
                    </div>
                    <div class="uk-width-1-2">
                        <div class="inner-settings-box">
                            <!-- Start Meta Field: Thesis Coordinator -->
                            <p>
                                <label><?php echo __('Thesis Supervisor', 'phdhub-cpts'); ?>*</label>
                                <input type="text" name="thesis_supervisor_label" id="supervisor-autocomplete" placeholder="<?php echo __('Search by typing supervisor\'s name', 'phdhub-cpts'); ?>..." required />
                                <input type="hidden" name="thesis_supervisor" value="<?php echo $thesis_supervisor; ?>" required />
                            </p>
                            <!-- End Meta Field: Thesis Coordinator -->
                            <div class="uk-grid">
                                <div class="uk-width-1-2">
                                    <!-- Start Meta Field: Duration -->
                                    <p>
                                        <label><?php echo __('Duration (in months)', 'phdhub-cpts'); ?>*</label>
                                        <input type="number" min="1" step="1" name="duration" />
                                    </p>
                                    <!-- End Meta Field: Duration -->
                                </div>
                                <div class="uk-width-1-2">
                                    <!-- Start Meta Field: Duration Type -->
                                    <p>
                                        <label><?php echo __('Display duration to users as', 'phdhub-cpts'); ?></label>
                                        <select name="duration_type">
                                            <option value="months"><?php echo 'Months'; ?></option>
                                            <option value="years"><?php echo 'Years'; ?></option>
                                        </select>
                                    </p>
                                    <!-- End Meta Field: Duration Type -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="uk-grid">
                    <div class="uk-width-1-2">
                        <div class="inner-settings-box">
                            <div class="uk-grid">
                                <!-- Start Inner Left Column -->
                                <div class="uk-width-1-2">
                                    <!-- Start Meta Field: Tuition Fees -->
                                    <p>
                                        <label><?php echo __('Tuition Fees', 'phdhub-cpts'); ?>*</label>
                                        <input type="number" step="1" min="1" name="tuition_fees" />
                                    </p>
                                    <!-- End Meta Field: Tuition Fees -->
                                </div>
                                <!-- End Inner Left Column -->
                                <!-- Start Inner Middle Column -->
                                <div class="uk-width-1-2">
                                    <!-- Start Meta Field: Tuition Fees Currency -->
                                    <p>
                                        <label><?php echo __('Currency', 'phdhub-cpts'); ?>*</label>
                                        <select name="tuition_fees_currency">
                                            <option value="eur"><?php echo '€'; ?></option>
                                            <option value="bgn"><?php echo 'лв'; ?></option>
                                            <option value="gpb"><?php echo '£'; ?></option>
                                            <option value="hrk"><?php echo 'kn'; ?></option>
                                            <option value="czk"><?php echo 'Kč'; ?></option>
                                            <option value="dkk"><?php echo 'kr'; ?></option>
                                            <option value="huf"><?php echo 'Ft'; ?></option>
                                            <option value="pln"><?php echo 'zł'; ?></option>
                                            <option value="ron"><?php echo 'Leu'; ?></option>
                                            <option value="chf"><?php echo 'Fr.'; ?></option>
                                        </select>
                                    </p>
                                    <!-- End Meta Field: Tuition Fees -->
                                </div>
                                <!-- End Inner Middle Column -->
                                <div class="uk-width-1-1">
                                    <!-- Start Meta Field: Master Degree -->
                                    <p>
                                        <label><?php echo __('Master Degree', 'phdhub-cpts'); ?>*</label>
                                        <select name="master_degree">
                                            <option value="empty"><?php echo __('Select', 'phdhub-cpts'); ?></option>
                                            <option value="required"><?php echo __('Required', 'phdhub-cpts'); ?></option>
                                            <option value="optional"><?php echo __('Optional', 'phdhub-cpts'); ?></option>
                                        </select>
                                    </p>
                                    <!-- End Meta Field: Master Degree -->
                                </div>
                                <div class="uk-width-1-1">
                                    <!-- Start Meta Field: Full/Part Time -->
                                    <p>
                                        <label><?php echo __('Full/Part Time', 'phdhub-cpts'); ?>*</label>
                                        <select name="type">
                                            <option value="empty"><?php echo __('Select', 'phdhub-cpts'); ?></option>
                                            <option value="full-time"><?php echo __('Full Time', 'phdhub-cpts'); ?></option>
                                            <option value="part-time"><?php echo __('Part Time', 'phdhub-cpts'); ?></option>
                                        </select>
                                    </p>
                                    <!-- End Meta Field: Full/Part Time -->
                                </div>
                                <!-- Start Inner Right Column -->
                                <div class="uk-width-1-1">
                                    <!-- Start Meta Field: Funded or Not -->
                                    <p>
                                        <label><?php echo __('Funded', 'phdhub-cpts'); ?>*</label>
                                        <select name="funded">
                                            <option value="empty"><?php echo __('Select', 'phdhub-cpts'); ?></option>
                                            <option value="partial"><?php echo __('Partial Funded', 'phdhub-cpts'); ?></option>
                                            <option value="full"><?php echo __('Full Funded', 'phdhub-cpts'); ?></option>
                                            <option value="no"><?php echo __('Not Funded', 'phdhub-cpts'); ?></option>
                                        </select>
                                    </p>
                                    <!-- End Meta Field: Funded or Not -->
                                </div>
                                <!-- End Inner Right Column -->
                            </div>  
                        </div>
                    </div>
                    <div class="uk-width-1-2">
                        <div class="inner-settings-box">
                            <div class="uk-grid">
                                <!-- Start Inner Left Column -->
                                <div class="uk-width-1-2">
                                    <!-- Start Meta Field: Starting Date -->
                                    <p>
                                        <label><?php echo __('Starting Date', 'phdhub-cpts'); ?>*</label>
                                        <input type="date" name="starting_date" />
                                    </p>
                                    <!-- End Meta Field: Starting Date-->
                                </div>
                                <!-- End Inner Left Column -->
                                <!-- Start Inner Right Column -->
                                <div class="uk-width-1-2">
                                    <!-- Start Meta Field: Deadline to Apply -->
                                    <p>
                                        <label><?php echo __('Deadline to Apply', 'phdhub-cpts'); ?>*</label>
                                        <input type="date" name="deadline" />
                                    </p>
                                    <!-- End Meta Field: Deadline to Apply -->
                                </div>
                                <!-- End Inner Right Column -->
                            </div>
                            <!-- Start Meta Field: Apply Now link -->
                            <p>
                                <label><?php echo __('Apply Now Link', 'phdhub-cpts'); ?></label>
                                <input type="url" size="80" name="link" />
                            </p>
                            <!-- End Meta Field: Apply Now link -->
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    
    <input type="hidden" name="submitted" id="submitted" value="true" />
    <button type="submit" class="submit-button"><?php echo __('Submit', 'phdhub-cpts') ?></button>
</form>
<?php
        } else {
            wp_redirect( home_url() );
            exit;
        }
    } else {
        wp_redirect( home_url() );
        exit;
    }
?>