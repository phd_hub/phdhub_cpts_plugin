<?php
	/*
	 * PhDHub CPTs plugin - Shortcodes
	 * Frontend form: Create Company Profile
	 */
	defined('ABSPATH') or die;


	if ( isset( $_POST['submitted'] )) {
		
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		require_once( ABSPATH . 'wp-admin/includes/media.php' );

		if ( trim( $_POST['postTitle'] ) === '' ) {
			$postTitleError = 'Please enter a title.';
			$hasError = true;
		}
		
		$post_information = array(
			'post_title' => wp_strip_all_tags( $_POST['company_name'] ),
			'post_type' => 'companies',
			'post_status' => 'publish',
		);

		$post_id = wp_insert_post( $post_information );
		$attachment_id = media_handle_upload('company_bg',$post_id);
		set_post_thumbnail($post_id, $attachment_id); 
	}


	$contact = 'contact_info';
    $contact_info = '';
	$info = 'company_info';
    $company_info = '';

	$user = wp_get_current_user();
	if ( is_user_logged_in() ) {
        if ( user_can( $user->ID, 'publish_companies' ) ) {
?>
<form id="company_meta" class="phdhub-form-sc" method="POST" enctype="multipart/form-data">
    <div class="phd-cpt-box">
        <div class="uk-grid">
            <!-- Start Left Column -->
            <div class="uk-width-1-2">
                <div class="inner-settings-box">
                    <p>
                        <label><?php echo __('Company\'s Name', 'phdhub-cpts'); ?>*</label>
                        <input type="text" size="80" name="company_name" />
                    </p>
                    <!-- Start Meta Field: Logo and Image -->
                    <p>
                        <label for="faculty_bg"><?php echo __('Background Image', 'phdhub-cpts'); ?>*</label>
                        <input type="file" name="company_bg" multiple="false">
                    </p>
                    <p>
                        <label for="company_logo">
                            <input id="company_logo" type="text" size="100" name="company_logo" placeholder="<?php echo __('Logo'); ?>*" /><input id="company_logo_button" class="button" type="button" value="<?php echo __('Upload', 'phdhub-cpts'); ?>" />
                        </label>
                    </p>
                    <!-- End Meta Field: Logo -->
                    <!-- Start Meta Field: CEO -->
                    <p>
                        <label><?php echo __('CEO', 'phdhub-cpts'); ?></label>
                        <input type="text" size="80" name="ceo" />
                    </p>
                    <!-- End Meta Field: CEO -->
                    <!-- Start Meta Field: Establishment -->
                    <p>
                        <label><?php echo __('Established in (Year)', 'phdhub-cpts'); ?></label>
                        <input type="text" size="80" name="establishment" />
                    </p>
                    <!-- End Meta Field: Establishment -->
                    <!-- Start Meta Field: Employees -->
                    <p>
                        <label><?php echo __('Number of Employees', 'phdhub-cpts'); ?></label>
                        <input type="text" size="80" name="employees" />
                    </p>
                    <!-- End Meta Field: Employees -->
                    <!-- Start Meta Field: Country -->
                    <p>
                        <label><?php echo __('Country', 'phdhub-cpts'); ?></label>
                        <input type="text" size="80" name="country" />
                    </p>
                    <!-- End Meta Field: Country -->
                    <!-- Start Meta Field: City -->
                    <p>
                        <label><?php echo __('City', 'phdhub-cpts'); ?></label>
                        <input type="text" size="80" name="city" />
                    </p>
                    <!-- End Meta Field: City -->
                    <!-- Start Meta Field: 	Address -->
                    <p>
                        <label><?php echo __('Address'); ?></label>
                        <input type="text" size="160" name="address" />
                    </p>
                    <!-- End Meta Field: Address -->
                    <!-- Start Meta Field: 	Postcode -->
                    <p>
                        <label><?php echo __('Postcode'); ?></label>
                        <input type="text" size="80" name="postcode" />
                    </p>
                    <!-- End Meta Field: Postcode -->
                    <!-- Start Meta Field: 	Email -->
                    <p>
                        <label><?php echo __('Email'); ?></label>
                        <input type="email" size="80" name="email" />
                    </p>
                    <!-- End Meta Field: Email -->
                    <!-- Start Meta Field: 	Phone Numbers -->
                    <p>
                        <label><?php echo __('Phone Numbers'); ?></label>
                        <input type="tel" size="80" name="phone_numbers" />
                    </p>
                    <!-- End Meta Field: Phone Numbers -->
                    <!-- Start Meta Field: 	Fax -->
                    <p>
                        <label><?php echo __('Fax'); ?></label>
                        <input type="tel" size="80" name="fax" />
                    </p>
                    <!-- End Meta Field: Fax -->
                    <!-- Start Meta Field: VAT Number -->
                    <p>
                        <label><?php echo __('VAT Number', 'phdhub-cpts'); ?></label>
                        <input type="text" size="80" name="vat_number" />
                    </p>
                    <!-- End Meta Field: VAT Number -->
                    <!-- Start Meta Field: Website -->
                    <p>
                        <label><?php echo __('Website', 'phdhub-cpts'); ?></label>
                        <input type="url" size="80" name="website" />
                    </p>
                    <!-- End Meta Field: Website -->
                </div>
                <div class="inner-settings-box">
                    <p>
                        <label><?php echo __('Location', 'phdhub-cpts'); ?></label>
                        <label><?php echo __('Add the longitude and latitude of the location or a URL of a map.', 'phdhub-cpts'); ?></label>
                    </p>
                    <div class="uk-grid">
                        <div class="uk-width-1-2">
                            <p>
                                <label><?php echo __('Longitude', 'phdhub-cpts'); ?></label>
                                <input type="text" size="80" name="longitude" />
                            </p>
                        </div>
                        <div class="uk-width-1-2">
                            <p>
                                <label><?php echo __('Latitude', 'phdhub-cpts'); ?></label>
                                <input type="text" size="80" name="latitude" />
                            </p>
                        </div>
                    </div>
                    <p>
                        <label><?php echo __('Google Map URL'); ?></label>
                        <input type="url" name="gmap_url" />
                    </p>
                    <p>
                        <label><?php echo __('Bing Map URL'); ?></label>
                        <input type="url" name="bing_map_url" />
                    </p>
                    <p>
                        <label><?php echo __('Yahoo Map URL'); ?></label>
                        <input type="url" name="yahoo_map_url" />
                    </p>
                </div>
            </div>
            <!-- End Left Column -->
            <!-- Start Right Column -->
            <div class="uk-width-1-2 inner-settings-box-right">
                <!-- Start Meta Field: General Info -->
                <p>
                    <label><?php echo __('General Info', 'phdhub-cpts'); ?></label>
                </p>
                <?php wp_editor( html_entity_decode(stripcslashes($company_info)), $info ); ?>
                <!-- End Meta Field: General Info -->

                <!-- Start Meta Field: Contact Info -->
                <p style="margin-top: 30px">
                    <label><?php echo __('Additional Contact Info', 'phdhub-cpts'); ?></label>
                </p>
                <?php wp_editor( html_entity_decode(stripcslashes($contact_info)), $contact ); ?>
                <!-- End Meta Field: Contact Info -->
            </div>
            <!-- End Right Column -->
        </div>
    </div>
    
    <input type="hidden" name="submitted" id="submitted" value="true" />
    <button type="submit" class="submit-button"><?php echo __('Submit', 'phdhub-cpts') ?></button>
</form>
<?php
        } else {
            wp_redirect( home_url() );
            exit;
        }
    } else {
        wp_redirect( home_url() );
        exit;
    }
?>