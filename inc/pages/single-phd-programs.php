<?php
/*
 * PhDHub CPTs
 * Single page file for PhD Programs CPT
 */
defined('ABSPATH') or die;

get_header(); 

?>
<div id="phdhub-cpt-page">
	<main id="phdhub-cpt-main" class="site-main">
		<?php
			while ( have_posts() ) : the_post();
				$post_id = get_the_ID();

				/*
				 * Get custom meta fields based on phd program's ID
				 */
				$institution = esc_attr( get_post_meta( $post_id, 'institution', true ) );
				$institution_details = get_page_by_path($institution, '', 'institutions');
				$faculty = esc_attr( get_post_meta( $post_id, 'faculty', true ) );
				$faculty_details = get_post($faculty);
				$website = esc_url( get_post_meta( $post_id, 'website', true ) );

				$phd_program_info = apply_filters( 'phdhub_cpt_content', get_post_meta( $post_id, 'phd_program_info', true ));
			?>
			<div id="phd-program-content">
				<!-- Start Post -->
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<figure class="uk-overlay">
						<?php
							/*
							 * Display phd program's thumbnail
							 */
							if ( has_post_thumbnail() ) {
								the_post_thumbnail( 'full' );
							}
						?>
						<figcaption class="uk-overlay-panel uk-overlay-background uk-flex uk-flex-bottom">
							<div class="container">
								<?php
									/*
									 * Display faculty's name (Post Title)
									 */
									if ( is_single() ) :
										the_title( '<h1 class="phd-program-name">', '</h1>' );
									else :
										the_title( '<h2 class="phd-program-name"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
									endif;

									if ( !empty ($institution) ) {
								?>
								<p class="faculty-institution">
									<i class="fa fa-university"></i> 
									<a href="<?php echo get_permalink( $institution_details ); ?>">
										<?php echo $institution_details->post_title; ?>
									</a>
								</p>
								<?php
									}
									/*
									 * Display faculty's name if the field is not empty
									 */
									if ($faculty != 'empty') {
								?>
								<!-- Start Meta Field: Faculty -->
								<p class="phd-program-faculty">
									<i class="fa fa-building-o"></i> 
									<a href="<?php echo get_permalink( $faculty ); ?>">
										<?php echo $faculty_details->post_title; ?>
									</a>
								</p>
								<!-- End Meta Field: Faculty -->
								<?php
									}
									if (!empty ($website)) {
								?>
								<p class="website">
									<i class="fa fa-globe"></i> <a href="<?php echo $website; ?>"><?php echo __('Visit Website', 'phdhub-cpts'); ?></a>
								</p>
								<?php
									}
								?>
							</div>
						</figcaption>
					</figure>
					
					<div class="container">
						<div class="uk-grid">
							<div class="uk-width-1-1">
								<div class="about-phd-program">
									<?php
										/*
										 * Display phd program's general info if the field is not empty
										 */
										if (!empty ($phd_program_info)) {
									?>
									<!-- Start Meta Field: PhD Program's General Info -->
									<div class="phd-program-info">
										<?php
											echo apply_filters('the_content', $phd_program_info);
										?>
									</div>
									<!-- End Meta Field: PhD Program's General Info -->
									<?php
										}
									?>
								</div>
							</div>
							
							<div class="uk-width-1-1">
								<!-- Start PhD Program's PhD Openings -->
								<div class="phd-program-openings latest-offers-three-col-grid">
									<h3>
										<span><?php echo __('PhD Openings', 'phdhub-cpts'); ?></span>
									<span class="all-offers"><a href="<?php echo site_url() . '/phd-openings'; ?>"><?php echo __('All PhD Offers', 'phdhub-cpts'); ?></a></span>
									</h3>
									<div class="uk-grid institution-offer-rows">
										<?php
											/*
											 * Get and display institution's recent PhD Openings
											 */
											$phd_openings_args2 = array(
												'posts_per_page' => 3,
												'post_type' => 'phd-openings',
												'orderby' => 'ID',
												'order' => 'DESC',
												'meta_query' => array(
													array(
														'key' => 'phd_program',
														'value' => $post_id,
														'compare' => '='
													)
												),
											);
											$phd_openings2 = get_posts($phd_openings_args2);
											
											foreach ($phd_openings2 as $phd_opening) {
												$post_type = get_post_type_object( get_post_type($phd_opening) );
												$post_cat = get_the_terms( $phd_opening->ID, 'fields-of-science');
												
												$short_info = esc_attr( get_post_meta( $phd_opening->ID, 'short_info', true ) );
												$institution = esc_attr( get_post_meta( $phd_opening->ID, 'institution', true ) );
												$institution_details = get_page_by_path($institution, '', 'institutions');
												$research_areas = get_the_terms($phd_opening->ID, 'fields-of-science');
												
												$institution_logo = esc_url( get_post_meta( $institution_details->ID, 'institution_logo', true ) );
												$institution_city = esc_attr( get_post_meta( $institution_details->ID, 'city', true ) );
												$institution_country = esc_attr( get_post_meta( $institution_details->ID, 'country', true ) );
												$institution_country = esc_attr( get_post_meta( $institution_details->ID, 'country', true ) );
										?>
											<div class="uk-width-1-3">
												<div class="uk-grid phd-opening-box">
													<div class="uk-width-1-1 phd-opening-thumbnail">
														<span class="phd-opening-date">
															<span class="day"><?php echo get_the_date('d', $phd_opening->ID); ?></span>
															<span class="month"><?php echo get_the_date('M', $phd_opening->ID); ?></span>
														</span>
													</div>
													<div class="uk-width-1-1 phd-opening-details">
														<h4>
															<a href="<?php echo get_permalink( $phd_opening->ID ); ?>">
																<?php echo $phd_opening->post_title; ?>
															</a>
														</h4>
														<p class="description"><?php echo $short_info; ?></p>
														<p class="phd-categories">
															<span>
																<?php
																	foreach ($research_areas as $research_area) {
																?>
																<a href="<?php echo get_term_link( $research_area, 'fields-of-science' ); ?>"><?php echo $research_area->name; ?></a>
																<?php
																	}
																?>
															</span>
														</p>
														<p class=" phd-opening-info">
															<?php
																// PhD Opening's Image
																if ( $institution_logo != NULL ) {
															?>
															<a href="<?php echo get_permalink( $institution_details->ID ); ?>"><img src="<?php echo $institution_logo; ?>" alt="Logo"></a>
															<?php
																}
															?>
															<a href="<?php echo get_permalink( $institution_details ); ?>">
																<?php echo $institution_details->post_title; ?>
																<span><?php echo $institution_city . ', ' . $institution_country; ?></span>
															</a>
														</p>
													</div>
												</div>
											</div>
										<?php
											}
										?>
									</div>
								</div>
								<!-- End PhD Program's PhD Openings -->
							</div>
						</div>
					</div>
				</article>
				<!-- End Post -->
			</div>
		<?php

			endwhile; // End of the loop.
		?>
	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
