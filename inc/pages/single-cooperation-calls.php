<?php
/*
 * PhDHub CPTs
 * Single page file for Cooperation Calls CPT
 */
defined('ABSPATH') or die;

get_header(); 

$call_title = (isset($_POST['call_title']) ? $_POST['call_title'] : null);
$fullname = (isset($_POST['interest_fullname']) ? $_POST['interest_fullname'] : null);
$email = (isset($_POST['interest_email']) ? $_POST['interest_email'] : null);
$phone = (isset($_POST['interest_phone']) ? $_POST['interest_phone'] : null);
$user_message = (isset($_POST['interest_message']) ? $_POST['interest_message'] : null);
$to_mail = (isset($_POST['interest_to_mail']) ? $_POST['interest_to_mail'] : null);
$submit = (isset($_POST['express_interest']) ? $_POST['express_interest'] : null);

if ($submit) {
	$to = $to_mail;
	$subject = "Expression of Interest: " . $call_title;
	$headers[] = 'From: '. $email . "\r\n";
	$headers[] = 'Reply-To: ' . $email . "\r\n";

	$message = "Call for Cooperation: " . $call_title . "\r\n\r\nFull Name: " . $fullname . " \r\nEmail: " . $email . " \r\nPhone Number: " . $phone . " \r\nMessage: " . $user_message; 
	$sent = wp_mail($to, $subject, strip_tags($message), $headers);
	
	wp_redirect( get_permalink() . '?submitted=1' );
}
?>
<div id="phdhub-cpt-page">
	<main id="phdhub-cpt-main" class="site-main">
		<?php
			while ( have_posts() ) : the_post();
				$post_id = get_the_ID();
                phdhub_setPostViews($post_id);

				/*
				 * Get custom meta fields based on call's ID
				 */
				$institution = esc_attr( get_post_meta( $post_id, 'institution', true ) );
				$institution_details = get_page_by_path($institution, '', 'institutions');
				$faculty = esc_attr( get_post_meta( $post_id, 'faculty', true ) );
				$faculty_details = get_post($faculty);
				$company = esc_attr( get_post_meta( $post_id, 'company', true ) );
				$company_details = get_page_by_path($company, '', 'companies');
				$phd_program = esc_attr( get_post_meta( $post_id, 'phd_program', true ) );
				$phd_program_details = get_post($phd_program);
				$phd_opening = esc_attr( get_post_meta( $post_id, 'phd_opening', true ) );
				$phd_opening_details = get_post($phd_opening);
				$project_coordinator = esc_attr( get_post_meta( $post_id, 'project_coordinator', true ) );
				$email_address = esc_attr( get_post_meta( $post_id, 'email_address', true ) );

				$user = get_user_by( 'login', $project_coordinator );

				$call_info = apply_filters( 'phdhub_cpt_content', get_post_meta( $post_id, 'call_info', true ));

				if ( is_user_logged_in() ) {
					$current_user = wp_get_current_user();
					$user_info = get_userdata( $current_user->ID );
					$fullname = $user_info->first_name . ' ' . $user_info->last_name;
					$user_email = $user_info->user_email;
				}
				else {
					$fullname = '';
					$user_email = '';
				}
			?>
			<div id="call-content">
				<!-- Start Post -->
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="container">
						<div class="uk-grid">
							<!-- Start Left Column -->
							<div class="uk-width-7-10 about-call">
								<?php
									if(isset($_GET['submitted'])){
   	 									$status = $_GET['submitted'];
     										if($status == 1){
								?>
								<p class="success-notice">
									<?php
        									echo 'The form was submitted successfully!';
									?>
								</p>
								<?php
     									} else if($status == 0){
								?>
								<p class="warning-notice">
									<?php
        									echo "Unfortunately, we couldn't submit your request!";
	     								?>
								</p>
								<?php
									}
 									}
									/*
									 * Display call's title (Post Title)
									 */
									if ( is_single() ) :
										the_title( '<h1 class="call-name">', '</h1>' );
									else :
										the_title( '<h2 class="call-name"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
									endif;

									/*
									 * Display faculty's general info if the field is not empty
									 */
									if (!empty ($call_info)) {
								?>
								<!-- Start Meta Field: Call's General Info -->
								<div class="call-info">
                                    <?php
                                        $views = phdhub_getPostViews($post_id);
                                        if ($views == 1) {
                                            $views_label = 'view';
                                        } else {
                                            $views_label = 'views';
                                        }
                                    ?>
                                    <span class="views"><i class="fa fa-eye"></i> <?php echo $views . ' ' . $views_label; ?></span>
									<?php
										echo apply_filters('the_content', $call_info);

										if (is_user_logged_in()) {
									?>
									<a href="#" class="interest-btn" data-uk-toggle="{target:'#express-interest-form'}"><?php echo __('Express Interest', 'phdhub-cpts'); ?></a>
									
									<div id="express-interest-form" class="uk-hidden">
										<h3>
											<?php echo __('Express Your Interest', 'phdhub-cpts'); ?></h3>
										<p class="form-notice">
											<?php echo __('Use the following form in order to express your interest for this call for cooperation.', 'phdhub-cpts'); ?>
										</p>
										<form method="POST">
											<input type="hidden" name="interest_to_mail" value="<?php echo $email_address; ?>">
											<input type="hidden" name="call_title" value="<?php the_title(); ?>">
											<div class="uk-grid">
												<div class="uk-width-1-1">
													<p>
														<label><?php echo __('Full Name', 'phdhub-cpts'); ?></label>
														<input type="text" name="interest_fullname" value="<?php echo $fullname; ?>" required />
													</p>
												</div>
												<div class="uk-width-1-2">
													<p>
														<label><?php echo __('Email', 'phdhub-cpts'); ?></label>
														<input type="email" name="interest_email" value="<?php echo $user_email; ?>" required />
													</p>
												</div>
												<div class="uk-width-1-2">
													<p>
														<label><?php echo __('Phone Number', 'phdhub-cpts'); ?></label>
														<input type="text" name="interest_phone" />
													</p>
												</div>
												<div class="uk-width-1-1">
													<p>
														<label><?php echo __('Message', 'phdhub-cpts'); ?></label>
														<textarea name="interest_message" rows="5" required></textarea>
													</p>
												</div>
											</div>
											<input type="submit" name="express_interest" value="Send Message" />
										</form>
									</div>
									<?php
										}
									?>
								</div>
								<!-- End Meta Field: Call's General Info -->
								<?php
									}
								?>
								
							</div>
							<!-- End Left Column -->
							<!-- Start Right Column -->
							
							<div class="uk-width-3-10">
								<div class="call-details">
									<!-- Start Call's Associated PhD Opening -->
									<div class="call-phd-opening">
										<h3>
											<span><?php echo __('Associated PhD Opening', 'phdhub-cpts'); ?>:</span>
										</h3>
										<?php
											/*
											 * Get and display call's associated PhD Opening
											 */
											$phd_opening_info = get_post( $phd_opening );
										?>
										<p>
											<a href="<?php echo get_permalink( $phd_opening ); ?>">
												<?php echo $phd_opening_info->post_title; ?>
											</a>
										</p>
									</div>
									<!-- End Call's Associated PhD Opening -->
									<?php 
										/*
										 * Display company's name if the field is not empty
										 */
										if ($company != 'empty') {
									?>
									<!-- Start Meta Field: Company -->
									<p class="call-institution">
										<span><?php echo __('Enterprise', 'phdhub-cpts'); ?>:</span>
										<a href="<?php echo get_permalink( $company_details ); ?>">
											<?php echo $company_details->post_title; ?>
										</a>
									</p>
									<!-- End Meta Field: Company -->
									<?php
										}
										/*
										 * Display institution's name if the field is not empty
										 */
										if ($institution != 'empty') {
									?>
									<!-- Start Meta Field: Institution -->
									<p class="call-institution">
										<span><?php echo __('Institution', 'phdhub-cpts'); ?>:</span>
										<a href="<?php echo get_permalink( $institution_details ); ?>">
											<?php echo $institution_details->post_title; ?>
										</a>
									</p>
									<!-- End Meta Field: Institution -->
									<?php
										}
										/*
										 * Display faculty's name if the field is not empty
										 */
										if ($faculty != 'empty') {
									?>
									<!-- Start Meta Field: Faculty -->
									<p class="call-faculty">
										<span><?php echo __('Faculty', 'phdhub-cpts'); ?>:</span>
										<a href="<?php echo get_permalink( $faculty ); ?>">
											<?php echo $faculty_details->post_title; ?>
										</a>
									</p>
									<!-- End Meta Field: Faculty -->
									<?php
										}
										/*
										 * Display phd program's name if the field is not empty
										 */
										if ($phd_program != 'empty') {
									?>
									<!-- Start Meta Field: PhD Program -->
									<p class="call-faculty">
										<span><?php echo __('PhD Program', 'phdhub-cpts'); ?>:</span>
										<a href="<?php echo get_permalink( $phd_program ); ?>">
											<?php echo $phd_program_details->post_title; ?>
										</a>
									</p>
									<!-- End Meta Field: PhD Program -->
									<?php
										}
										/*
										 * Display project's coordinator name if the field is not empty
										 */
										if ( !empty ($project_coordinator) ) {
									?>
									<!-- Start Meta Field: Project Coordinator -->
									<p class="call-project-coordinator">
										<span><?php echo __('Project Coordinator', 'phdhub-cpts'); ?>:</span>
										<?php echo $user->last_name . ' ' . $user->first_name; ?>
									</p>
									<!-- End Meta Field: Project Coordinator -->
									<?php
										}
									?>
								</div>
							</div>
							<!-- End Right Column -->
						</div>
					</div>
				</article>
				<!-- End Post -->
			</div>
		<?php

			endwhile; // End of the loop.
		?>
	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
