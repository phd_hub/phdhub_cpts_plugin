<?php
/*
 * PhDHub CPTs
 * Single page file for PhD Openings CPT
 */
defined( 'ABSPATH' ) or die;

get_header();
?>
<div id="phdhub-cpt-page">
    <main id="phdhub-cpt-main" class="site-main">
	<?php
	while ( have_posts() ) : the_post();
	    $post_id = get_the_ID();
        phdhub_setPostViews($post_id);

	    /*
	     * Get custom meta fields based on phd opening's ID
	     */
	    $institution		 = esc_attr( get_post_meta( $post_id, 'institution', true ) );
	    $institution_details	 = get_page_by_path( $institution, '', 'institutions' );
	    $long			 = esc_attr( get_post_meta( $institution_details->ID, 'longitude', true ) );
	    $lat			 = esc_attr( get_post_meta( $institution_details->ID, 'latitude', true ) );
	    $institution_logo	 = esc_url( get_post_meta( $institution_details->ID, 'institution_logo', true ) );
	    $faculty		 = esc_attr( get_post_meta( $post_id, 'faculty', true ) );
	    $faculty_details	 = get_post( $faculty );
	    $phd_program		 = esc_attr( get_post_meta( $post_id, 'phd_program', true ) );
	    $phd_program_details	 = get_post( $phd_program );
	    $research_team		 = esc_attr( get_post_meta( $post_id, 'research_team', true ) );
	    $research_team_details	 = get_post( $research_team );

	    $supervisor		 = esc_attr( get_post_meta( $post_id, 'thesis_supervisor', true ) );
	    $duration		 = esc_attr( get_post_meta( $post_id, 'duration', true ) );
	    $duration_type		 = esc_attr( get_post_meta( $post_id, 'duration_type', true ) );
	    $master_degree		 = esc_attr( get_post_meta( $post_id, 'master_degree', true ) );
	    $tuition_fees		 = esc_attr( get_post_meta( $post_id, 'tuition_fees', true ) );
	    $tuition_fees_currency	 = esc_attr( get_post_meta( $post_id, 'tuition_fees_currency', true ) );
	    $funded			 = esc_attr( get_post_meta( $post_id, 'funded', true ) );
	    $type			 = esc_attr( get_post_meta( $post_id, 'type', true ) );
	    $starting_date		 = esc_attr( get_post_meta( $post_id, 'starting_date', true ) );
	    $deadline		 = esc_attr( get_post_meta( $post_id, 'deadline', true ) );
	    $link			 = esc_attr( get_post_meta( $post_id, 'link', true ) );

	    $phd_template		 = esc_attr( get_post_meta( $post_id, 'phd_template', true ) );
	    $phd_template_details	 = get_post( $phd_template );
	    $mean_salary		 = esc_attr( get_post_meta( $phd_template, 'mean_salary', true ) );
	    $conditions		 = esc_attr( get_post_meta( $phd_template, 'phd_template_conditions', true ) );
	    $other_info		 = esc_attr( get_post_meta( $phd_template, 'phd_template_info', true ) );

	    $phd_opening_info = apply_filters( 'phdhub_cpt_content', get_post_meta( $post_id, 'phd_opening_info', true ) );

	    $user		 = get_user_by( 'login', $supervisor );
	    $research_areas	 = get_the_terms( $post_id, 'fields-of-science' );
	    ?>
    	<div id="phd-offer-content">
    	    <!-- Start Post -->
    	    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    		<div class="container">
    		    <div class="uk-grid phd-offer-columns">
    			<!-- Start Left Column -->
    			<div class="uk-width-7-10 about-phd-offer">
				<?php
				/*
				 * Display PhD Opening's title (Post Title)
				 */
				if ( is_single() ) :
				    the_title( '<h1 class="phd-offer-title">', '</h1>' );
				else :
				    the_title( '<h2 class="phd-offer-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				endif;

				if ( is_user_logged_in() ) {
				    the_favorites_button( $post_id );
				}
				?>

    			    <div class="offer-info">
                        <?php
                            $views = phdhub_getPostViews($post_id);
                            if ($views == 1) {
                                $views_label = 'view';
                            } else {
                                $views_label = 'views';
                            }
                        ?>
                        <span class="views"><i class="fa fa-eye"></i> <?php echo $views . ' ' . $views_label; ?></span>
				    <?php
				    /*
				     * Display phd offer's thumbnail
				     */
				    if ( has_post_thumbnail() ) {
					the_post_thumbnail( 'full' );
				    }
				    if ( ! empty( $phd_opening_info ) ) {
					?>
					<!-- Start Meta Field: PhD Opening's General Info -->
					<div class="phd-offer-info">
					    <?php
					    echo apply_filters( 'the_content', $phd_opening_info );
					    ?>
					</div>
					<!-- End Meta Field: PhD Opening's General Info -->
					<?php
				    }
				    $qualifications_list = get_the_term_list( $post->ID, 'skills', '<ul class="skills-required"><li>', '</li><li>', '</li></ul>' );
				    if ( ! empty( $qualifications_list ) ) {
					?>
					<!-- Start Meta Field: Skills Required -->
					<div class="phd-offer-skills">
					    <h3><?php echo __( 'Skills Required', 'phdhub-cpts' ); ?></h3>
					    <?php
					    print $qualifications_list;
					    ?>
					</div>
					<!-- End Meta Field: Skills Required -->
					<?php
				    }
				    if ( ( ! empty( $mean_salary )) || ( ! empty( $conditions )) || ( ! empty( $other_info )) ) {
					?>
					<!-- Start PhD Opening's Other Info -->
					<div class="phd-offer-other-info">
					    <h3><?php echo __( 'More Info', 'phdhub-cpts' ); ?></h3>
					    <?php
					    if ( ! empty( $mean_salary ) ) {
						?>
	    				    <h4><?php echo __( 'Mean Salary', 'phdhub-cpts' ); ?></h4>
						<?php
						echo $mean_salary;
					    }
					    if ( ! empty( $conditions ) ) {
						?>
	    				    <h4><?php echo __( 'Conditions', 'phdhub-cpts' ); ?></h4>
						<?php
						echo $conditions;
					    }
					    if ( ! empty( $other_info ) ) {
						?>
	    				    <h4><?php echo __( 'Other Info', 'phdhub-cpts' ); ?></h4>
						<?php
						echo $other_info;
					    }
					    ?>
					</div>
					<!-- End PhD Opening's Other Info -->
					<?php
				    }

				    /*
				     * Display phd offer's fields of science
				     */
				    $tag_list	 = $tags		 = get_the_term_list( $post->ID, 'research-areas', '<ul class="fields-of-science-list"><li>', '</li><li>', '</li></ul>' );

				    if ( ( ! empty( $tag_list )) || ( ! empty( $research_areas )) ) {
					?>
					<div class="fields-of-science">
					    <div class="uk-grid">
						<div class="uk-width-1-2">
						    <p><?php echo __( 'Fields of Science', 'phdhub-cpts' ); ?>:</p>
						    <ul>
							<?php
							foreach ( $research_areas as $research_area ) {
							    ?>
	    						<li><a href="<?php echo get_term_link( $research_area, 'fields-of-science' ); ?>"><?php echo $research_area->name; ?></a></li>
							    <?php
							}
							?>
						    </ul>
						</div>
						<div class="uk-width-1-2">
						    <p><?php echo __( 'Research Areas', 'phdhub-cpts' ); ?>:</p>
						    <?php
						    print $tag_list;
						    ?>
						</div>
					    </div>
					</div>
					<?php
				    }
				    ?>
    				<div class="share-offer">
    				    <p class="share-label"><?php echo __( 'Share on', 'phdhub-cpts' ) . ':'; ?></p>
					<?php echo do_shortcode( '[TheChamp-Sharing]' ); ?>
    				</div>
    			    </div>
    			</div>
    			<!-- End Left Column -->

    			<!-- Start Right Column -->
    			<div class="uk-width-3-10 phd-offer-institution">
    			    <div class="phd-offer-info-box">
				    <?php
				    /*
				     * Display institution's logo if the field is not empty
				     */
				    if ( ! empty( $institution_logo ) ) {
					?>
					<img src="<?php echo $institution_logo; ?>" alt="Logo">
					<?php
				    }
				    /*
				     * Display institution's name if the field is not empty
				     */
				    if ( $institution != 'empty' ) {
					?>
					<!-- Start Meta Field: Institution -->
					<p>
					    <span><?php echo __( 'Institution', 'phdhub-cpts' ); ?>:</span>
					    <a href="<?php echo get_permalink( $institution_details ); ?>">
						<?php echo $institution_details->post_title; ?>
					    </a>
					</p>
					<!-- End Meta Field: Institution -->
					<?php
				    }
				    /*
				     * Display faculty's name if the field is not empty
				     */
				    if ( $faculty != 'empty' ) {
					?>
					<!-- Start Meta Field: Faculty -->
					<p>
					    <span><?php echo __( 'Faculty', 'phdhub-cpts' ); ?>:</span>
					    <a href="<?php echo get_permalink( $faculty ); ?>">
						<?php echo $faculty_details->post_title; ?>
					    </a>
					</p>
					<!-- End Meta Field: Faculty -->
					<?php
				    }
				    /*
				     * Display PhD Program if the field is not empty
				     */
				    if ( $phd_program != 'empty' ) {
					?>
					<!-- Start Meta Field: PhD Program -->
					<p>
					    <span><?php echo __( 'PhD Program', 'phdhub-cpts' ); ?>:</span>
					    <a href="<?php echo get_permalink( $phd_program ); ?>">
						<?php echo $phd_program_details->post_title; ?>
					    </a>
					</p>
					<!-- End Meta Field: PhD Program -->
					<?php
				    }
				    /*
				     * Display Research Team if the field is not empty
				     */
				    if ( $research_team != 'empty' ) {
					?>
					<!-- Start Meta Field: Research Team -->
					<p>
					    <span><?php echo __( 'Research Team', 'phdhub-cpts' ); ?>:</span>
					    <a href="<?php echo get_permalink( $research_team ); ?>">
						<?php echo $research_team_details->post_title; ?>
					    </a>
					</p>
					<!-- End Meta Field: Research Team -->
					<?php
				    }
				    ?>
    			    </div>
    			    <div class="phd-offer-info-box">
				    <?php
				    if ( ! empty( $supervisor ) ) {
					if ( $user ) {
					    ?>
	    				<!-- Start Meta Field: Thesis Supervisor -->
	    				<p>
	    				    <span><?php echo __( 'Thesis Supervisor', 'phdhub-cpts' ); ?>:</span>
						<?php echo $user->last_name . ' ' . $user->first_name; ?>
	    				</p>
	    				<!-- End Meta Field: Thesis Supervisor -->
					    <?php
					}
				    }
				    ?>

				    <?php
				    if ( $funded != 'empty' ) {
					switch ( $funded ) {
					    case "full":
						$funded_label	 = 'Full Funded';
						break;
					    case "partial":
						$funded_label	 = 'Partial Funded';
						break;
					    case "no":
						$funded_label	 = 'Not Funded';
						break;
					}
					?>
					<!-- Start Meta Field: Funded or Not -->
					<p>
					    <span><?php echo __( 'Funded', 'phdhub-cpts' ); ?>:</span>
					    <?php echo __( $funded_label, 'phdhub-cpts' ); ?>
					</p>
					<!-- End Meta Field: Funded or Not -->
					<?php
				    }
				    ?>

				    <?php
				    if ( $master_degree != 'empty' ) {
					switch ( $master_degree ) {
					    case "required":
						$master_degree_label	 = 'Required';
						break;
					    case "optional":
						$master_degree_label	 = 'Not Required';
						break;
					}
					?>
					<!-- Start Meta Field: Master Degree Required -->
					<p>
					    <span><?php echo __( 'Master Degree', 'phdhub-cpts' ); ?>:</span>
					    <?php echo __( $master_degree_label, 'phdhub-cpts' ); ?>
					</p>
					<!-- End Meta Field: Master Degree Required -->
					<?php
				    }
				    ?>

				    <?php
				    if ( ! empty( $tuition_fees ) ) {
					switch ( $tuition_fees_currency ) {
					    case "eur":
						$tuition_fees_currency_label	 = '€';
						break;
					    case "bgn":
						$tuition_fees_currency_label	 = 'лв';
						break;
					    case "gbp":
						$tuition_fees_currency_label	 = '£';
						break;
					    case "hrk":
						$tuition_fees_currency_label	 = 'kn';
						break;
					    case "czk":
						$tuition_fees_currency_label	 = 'Kč';
						break;
					    case "dkk":
						$tuition_fees_currency_label	 = 'kr';
						break;
					    case "huf":
						$tuition_fees_currency_label	 = 'Ft';
						break;
					    case "pln":
						$tuition_fees_currency_label	 = 'zł';
						break;
					    case "ron":
						$tuition_fees_currency_label	 = 'Leu';
						break;
					    case "chf":
						$tuition_fees_currency_label	 = 'Fr.';
						break;
					    default:
						$tuition_fees_currency_label	 = '€';
					}
					?>
					<!-- Start Meta Field: Tuition Fees -->
					<p>
					    <span><?php echo __( 'Tuition Fees', 'phdhub-cpts' ); ?>:</span>
					    <?php echo $tuition_fees_currency_label . ' ' . money_format( '%i', $tuition_fees ); ?>
					</p>
					<!-- End Meta Field: Tuition Fees -->
					<?php
				    }
				    ?>

				    <?php
				    if ( ( ! empty( $duration )) || ( ! empty( $type )) || ( ! empty( $starting_date )) || ( ! empty( $deadline )) ) {
					if ( ! empty( $duration ) ) {
					    switch ( $duration_type ) {
						case "months":
						    $duration_label	 = 'Months';
						    $duration	 = $duration;
						    break;
						case "years":
						    $duration_label	 = 'Years';
						    $duration	 = $duration / 12;
						    break;
					    }
					    ?>
	    				<!-- Start Meta Field: Duration -->
	    				<p>
	    				    <span><?php echo __( 'Duration', 'phdhub-cpts' ); ?>:</span>
						<?php echo $duration . ' ' . $duration_label; ?>
	    				</p>
	    				<!-- End Meta Field: Duration -->
					    <?php
					}
					if ( $type != 'empty' ) {
					    ?>
	    				<!-- Start Meta Field: Full/Part Time -->
	    				<p>
	    				    <span><?php echo __( 'Full/Part Time', 'phdhub-cpts' ); ?>:</span>
						<?php
						switch ( $type ) {
						    case "full-time":
							$type_label	 = 'Full Time';
							break;
						    case "part-time":
							$type_label	 = 'Part Time';
							break;
						}
						echo __( $type_label, 'phdhub-cpts' );
						?>
	    				</p>
	    				<!-- End Meta Field: Full/Part Time -->
					    <?php
					}
					if ( ! empty( $starting_date ) ) {
					    ?>
	    				<!-- Start Meta Field: Starting Date -->
	    				<p>
	    				    <span><?php echo __( 'Starting Date', 'phdhub-cpts' ); ?>:</span>
						<?php echo date( 'd F Y', strtotime( $starting_date ) ); ?>
	    				</p>
	    				<!-- End Meta Field: Starting Date -->
					    <?php
					}
					if ( ! empty( $deadline ) ) {
					    ?>
	    				<!-- Start Meta Field: Deadline to Apply -->
	    				<p>
	    				    <span><?php echo __( 'Deadline to Apply', 'phdhub-cpts' ); ?>:</span>
						<?php echo date( 'd F Y', strtotime( $deadline ) ); ?>
	    				</p>
	    				<!-- End Meta Field: Deadline to Apply -->
					    <?php
					}
				    }

				    if ( ! empty( $link ) ) {
					$current_date	 = date( 'Y/m/d' );
					$deadline_date	 = date( 'Y/m/d', strtotime( $deadline ) );
					if ( $current_date <= $deadline_date ) {
					    ?>
	    				<!-- Start Meta Field: Apply Now link -->
	    				<p class="apply-btn">
						<?php echo __( 'Are you interested in this PhD Offer?', 'phdhub-cpts' ); ?>
	    				    <a href="<?php echo $link; ?>"><?php echo __( 'Apply Now', 'phdhub-cpts' ); ?></a>
	    				</p>
	    				<!-- End Meta Field: Apply Now link -->
					    <?php
					} else {
					    ?>
	    				<p class="apply-btn">
						<?php echo __( 'Unfortunately, this PhD Offer has expired!', 'phdhub-cpts' ); ?>
	    				</p>
					    <?php
					}
				    }
				    ?>
    			    </div>

				<?php
				if ( ! empty( $long ) && ! empty( $lat ) ) {
				    ?>
				    <div id="map-area" style="height: 480px"></div>
				    <script type="text/javascript">
					var mymap = L.map('map-area').setView([<?php echo $long; ?>, <?php echo $lat; ?>], 15);
					L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
					    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
					    maxZoom: 18,
					    id: 'mapbox.streets',
					    accessToken: 'pk.eyJ1IjoiaXphY2hyb3MiLCJhIjoiY2pvcHJ0aHEzMXR0ZDNwcWF4YndtN3pnYiJ9.3cu-dWnd2OEdKIm_nhO6dQ'
					}).addTo(mymap);
					var marker = L.marker([<?php echo $long; ?>, <?php echo $lat; ?>]).addTo(mymap);
				    </script>
				    <?php
				}
				?>
    			</div>
    			<!-- End Right Column -->
    		    </div>

    		    <div class="institution-columns latest-offers-three-col-grid">
    			<div class="container">
    			    <div class="uk-grid">
    				<!-- Start Left Column -->
    				<div class="uk-width-1-1">
    				    <h3>
    					<span><?php echo __( 'Institution\'s Other PhD Offers', 'phdhub-cpts' ); ?></span>
    					<span class="all-offers">
    					    <a href="<?php echo site_url() . '/phd-openings'; ?>"><?php echo __( 'All PhD Offers', 'phdhub-cpts' ); ?></a>
    					</span>
    				    </h3>
    				    <div class="uk-grid institution-offer-rows">
					    <?php
					    /*
					     * Get and display institution's recent PhD Openings
					     */
					    $phd_openings_args2	 = array(
						'posts_per_page' => 3,
						'post_type'	 => 'phd-openings',
						'orderby'	 => 'ID',
						'order'		 => 'DESC',
						'exclude'	 => array( get_the_ID() ),
						'meta_query'	 => array(
						    array(
							'key'		 => 'institution',
							'value'		 => $institution,
							'compare'	 => '='
						    )
						),
					    );
					    $phd_openings2		 = get_posts( $phd_openings_args2 );

					    foreach ( $phd_openings2 as $phd_opening ) {
						$post_type	 = get_post_type_object( get_post_type( $phd_opening ) );
						$post_cat	 = get_the_terms( $phd_opening->ID, 'fields-of-science' );

						$short_info		 = substr( get_post_meta( $post_id, 'phd_opening_info', true ), 0, 150 );
						$institution		 = esc_attr( get_post_meta( $phd_opening->ID, 'institution', true ) );
						$institution_details	 = get_page_by_path( $institution, '', 'institutions' );
						$research_areas		 = get_the_terms( $phd_opening->ID, 'fields-of-science' );

						$institution_logo	 = esc_url( get_post_meta( $institution_details->ID, 'institution_logo', true ) );
						$institution_city	 = esc_attr( get_post_meta( $institution_details->ID, 'city', true ) );
						$institution_country	 = esc_attr( get_post_meta( $institution_details->ID, 'country', true ) );
						$institution_country	 = esc_attr( get_post_meta( $institution_details->ID, 'country', true ) );
						?>
						<div class="uk-width-1-3">
						    <div class="uk-grid phd-opening-box">
							<div class="uk-width-1-1 phd-opening-thumbnail">
							    <span class="phd-opening-date">
								<span class="day"><?php echo get_the_date( 'd', $phd_opening->ID ); ?></span>
								<span class="month"><?php echo get_the_date( 'M', $phd_opening->ID ); ?></span>
							    </span>
							</div>
							<div class="uk-width-1-1 phd-opening-details">
							    <h4>
								<a href="<?php echo get_permalink( $phd_opening->ID ); ?>">
								    <?php echo $phd_opening->post_title; ?>
								</a>
							    </h4>
							    <p class="description"><?php echo $short_info . '...'; ?></p>
							    <p class="phd-categories">
								<span>
								    <?php
								    foreach ( $research_areas as $research_area ) {
									?>
	    							    <a href="<?php echo get_term_link( $research_area, 'fields-of-science' ); ?>"><?php echo $research_area->name; ?></a>
									<?php
								    }
								    ?>
								</span>
							    </p>
							    <p class=" phd-opening-info">
								<?php
								// PhD Opening's Image
								if ( $institution_logo != NULL ) {
								    ?>
	    							<a href="<?php echo get_permalink( $institution_details->ID ); ?>"><img src="<?php echo $institution_logo; ?>" alt="Logo"></a>
								    <?php
								}
								?>
								<a href="<?php echo get_permalink( $institution_details ); ?>">
								    <?php echo $institution_details->post_title; ?>
								    <span><?php echo $institution_city . ', ' . $institution_country; ?></span>
								</a>
							    </p>
							</div>
						    </div>
						</div>
						<?php
					    }
					    ?>
    				    </div>
					<?php
					if ( empty( $phd_openings2 ) ) {
					    ?>
					    <p class="no-entries"><?php echo __( 'It seems that there are not any other PhD Offers of this institution.', 'phdhub-cpts' ); ?></p>
					    <?php
					}
					?>
    				</div>
    				<!-- End Right Column -->
    			    </div>
    			</div>
    		    </div>

    		    <div class="institution-columns latest-offers-three-col-grid">
    			<div class="container">
    			    <div class="uk-grid">
    				<!-- Start Left Column -->
    				<div class="uk-width-1-1">
    				    <h3>
    					<span><?php echo __( 'Related PhD Offers', 'phdhub-cpts' ); ?></span>
    					<span class="all-offers">
    					    <a href="<?php echo site_url() . '/phd-openings'; ?>"><?php echo __( 'All PhD Offers', 'phdhub-cpts' ); ?></a>
    					</span>
    				    </h3>
    				    <div class="uk-grid institution-offer-rows">
					    <?php
					    $config		 = array(
						'endpoint' => array(
						    'localhost' => array(
							'host'	 => SOLR_HOST,
							'port'	 => 8983,
							'path'	 => '/solr/' . SOLR_PHD_OFFERS_CORE . '/'
						    )
						)
					    );
					    $client		 = new Solarium\Client( $config );
					    $query		 = $client->createMoreLikeThis();
					    $query->setQuery( 'id:' . md5( get_the_guid( $post_id ) ) );
					    $query->setMltFields( 'categories_iri', 'research_areas_iri', "expanded_research_areas_iri" );
					    $query->setMinimumDocumentFrequency( 1 );
					    $query->setMinimumTermFrequency( 1 );
					    $query->setInterestingTerms( 'all_text' );
					    $query->setMatchInclude( true );
					    $resultset	 = $client->select( $query );
					    foreach ( $resultset as $document ) {
						$post_id = $document->ID[ 0 ];
						$offer	 = new PhDOfferPosting( $document );
						?>
						<div class="uk-width-1-3">
						    <div class="uk-grid phd-opening-box">
							<div class="uk-width-1-1 phd-opening-thumbnail">
							    <span class="phd-opening-date">
								<span class="day"><?php echo $offer->post_day; ?></span>
								<span class="month"><?php echo $offer->post_month; ?></span>
							    </span>
							</div>
							<div class="uk-width-1-1 phd-opening-details">
							    <h4>
								<a href="<?php echo $offer->permalink; ?>">
								    <?php echo $offer->title; ?>
								</a>
							    </h4>
							    <p class="description"><?php echo $offer->short; ?></p>
							    <p class="phd-categories">
								<span>
								    <?php
								    foreach ( $offer->categories_permalink as $key => $category ) {
									?>
	    							    <a href="<?php echo $category; ?>"><?php echo $offer->categories_labels[ $key ]; ?></a>
									<?php
								    }
								    ?>
								</span>
							    </p>
							    <p class=" phd-opening-info">	    
								<a href="<?php echo $offer->institution_permalink; ?>"><img src="<?php echo $offer->institution_logo; ?>" alt="Logo"></a>

								<a href="<?php $offer->institution_permalink; ?>">
								    <?php echo $offer->institution_label; ?>
								    <span><?php echo $offer->institution_city . ', ' . $offer->institution_country; ?></span>
								</a>
							    </p>
							</div>
						    </div>
						</div>
						<?php
					    }
					    ?>
    				    </div>
    				</div>
    				<!-- End Right Column -->
    			    </div>
    			</div>
    		    </div>
    		</div>
    	    </article>
    	    <!-- End Post -->
    	</div>
	    <?php
	endwhile; // End of the loop.
	?>
    </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();