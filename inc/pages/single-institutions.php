<?php
/*
 * PhDHub CPTs
 * Single page file for Institutions CPT
 */
defined('ABSPATH') or die;

get_header(); 

global $post;
?>
<div id="phdhub-cpt-page">
	<main id="phdhub-cpt-main" class="site-main">
		<?php
			while ( have_posts() ) : the_post();
				$post_id = get_the_ID();

				/*
				 * Get custom meta fields based on institution's ID
				 */
				$logo = esc_url( get_post_meta( $post_id, 'institution_logo', true ) );
				$country = esc_attr( get_post_meta( $post_id, 'country', true ) );
				$city = esc_attr( get_post_meta( $post_id, 'city', true ) );
				$address = esc_attr( get_post_meta( $post_id, 'address', true ) );
				$postcode = esc_attr( get_post_meta( $post_id, 'postcode', true ) );
				$email = esc_attr( get_post_meta( $post_id, 'email', true ) );
				$phone_numbers = esc_attr( get_post_meta( $post_id, 'phone_numbers', true ) );
				$fax = esc_attr( get_post_meta( $post_id, 'fax', true ) );
				$professors = esc_attr( get_post_meta( $post_id, 'professors', true ) );
				$students = esc_attr( get_post_meta( $post_id, 'students', true ) );
				$website = esc_url( get_post_meta( $post_id, 'website', true ) );
				$long = esc_attr( get_post_meta( $post_id, 'longitude', true ) );
				$lat = esc_attr( get_post_meta( $post_id, 'latitude', true ) );

				$contact_info = apply_filters( 'phdhub_cpt_content', get_post_meta( $post_id, 'contact_info', true ));
				$institution_info = apply_filters( 'phdhub_cpt_content', get_post_meta( $post_id, 'institution_info', true ));
				$post = get_post($post_id); 
				$slug = $post->post_name;
			?>
			<div id="institution-content">
				<!-- Start Post -->
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<figure class="uk-overlay">
						<?php
							/*
							 * Display institutions's thumbnail
							 */
							if ( has_post_thumbnail() ) {
								the_post_thumbnail( 'full' );
							}
						?>
						<figcaption class="uk-overlay-panel uk-overlay-background uk-flex uk-flex-bottom">
							<div class="container">
								<?php
									if (!empty ($logo)) {
								?>
								<div class="institution-image">
									<img src="<?php echo $logo; ?>" alt="Logo">
								</div>
								<?php
									}
								?>
								<div class="institution-details-right">
									<?php
										/*
										 * Display institution's name (Post Title)
										 */
										if ( is_single() ) :
											the_title( '<h1 class="institution-name">', '</h1>' );
										else :
											the_title( '<h2 class="institution-name"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
										endif;

										if ( (!empty ($country)) || (!empty ($city)) ) {
									?>
									<p class="location">
										<i class="fa fa-map-marker"></i> <?php echo $city . ', ' . $country; ?>
									</p>
									<?php
										}
										if (!empty ($website)) {
									?>
									<p class="website">
										<i class="fa fa-globe"></i> <a href="<?php echo $website; ?>"><?php echo __('Visit Website', 'phdhub-cpts'); ?></a>
									</p>
									<?php
										}
									?>
								</div>
							</div>
						</figcaption>
					</figure>
					<div class="container">
						<div class="uk-grid institution-columns">
							<div class="uk-width-7-10">
								<?php
									/*
									 * Display institution's general info if the field is not empty
									 */
									 if (!empty ($institution_info)) {
								?>
								<!-- Start Meta Field: Institution's General Info -->
								<div class="institution-info">
									<?php echo apply_filters('the_content', $institution_info); ?>
								</div>
								<!-- End Meta Field: Institution's General Info -->
								<?php
									}
								?>
							</div>
							<div class="uk-width-3-10">
								<?php
									/*
									 * Display institution's map if the field is not empty
									 */
									if ( !empty( $long ) && !empty( $lat ) ) {
								?>
								<div id="map-area" style="height: 480px"></div>
								<script type="text/javascript">
									var mymap = L.map('map-area').setView([<?php echo $long; ?>, <?php echo $lat; ?>], 15);
									L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
										attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
										maxZoom: 18,
										id: 'mapbox.streets',
										accessToken: 'pk.eyJ1IjoiaXphY2hyb3MiLCJhIjoiY2pvcHJ0aHEzMXR0ZDNwcWF4YndtN3pnYiJ9.3cu-dWnd2OEdKIm_nhO6dQ'
									}).addTo(mymap);
									var marker = L.marker([<?php echo $long; ?>, <?php echo $lat; ?>]).addTo(mymap);
								</script>
								<?php
									}

									/*
									 * Display institution's contact info if the field is not empty
									 */
									if ( (!empty ($contact_info)) || (!empty ($address)) || (!empty ($postcode)) || (!empty ($email)) || (!empty ($phone_numbers)) || (!empty ($fax)) ){
								?>
								<!-- Start Meta Field: Contact Info -->
								<div class="contact-info">
									<p>
										<span><?php echo __('Contact Info', 'phdhub-cpts'); ?>:</span>
									</p>
									<?php
										if (!empty ($address)) {
									?>
									<span><?php echo $address; ?></span>
									<?php
										}
										if (!empty ($postcode)) {
									?>
									<span><?php echo $postcode; ?></span>
									<?php
										}
										if (!empty ($email)) {
									?>
									<span><?php echo $email; ?></span>
									<?php
										}
										if (!empty ($phone_numbers)) {
									?>
									<span><?php echo $phone_numbers; ?></span>
									<?php
										}
										if (!empty ($fax)) {
									?>
									<span><?php echo $fax; ?></span>
									<?php
										}
										if (!empty ($contact_info)) {
											echo $contact_info;
										}
									?>
								</div>
								<?php
									}
								?>
							</div>
						</div>
					</div>
					
					<div class="institution-statistics">
						<div class="container">
							<div class="uk-grid">
								<?php
									if (!empty ($professors)) {
								?>
								<div class="uk-width-1-4">
									<i class="fa fa-users"></i>
									<?php echo $professors; ?>
									<span><?php echo __('Professors', 'phdhub-cpts'); ?></span>
								</div>
								<?php
									}
									if (!empty ($students)) {
								?>
								<div class="uk-width-1-4">
									<i class="fa fa-graduation-cap"></i>
									<?php echo $students; ?>
									<span><?php echo __('Students', 'phdhub-cpts'); ?></span>
								</div>
								<?php
									}
										
									/*
									 * Get and display institution's Faculties
									 */
									$faculties_args = array(
										'posts_per_page' => -1,
										'post_type' => 'faculties',
										'orderby' => 'title',
										'order' => 'ASC',
										'meta_query' => array(
											array(
												'key' => 'institution',
												'value' => $slug,
												'compare' => '='
											)
										),
									);
									$faculties = get_posts($faculties_args);
									$faculties_number = count($faculties);
								?>
								<div class="uk-width-1-4">
									<i class="fa fa-university"></i>
									<?php echo $faculties_number; ?>
									<span><?php echo __('Faculties', 'phdhub-cpts'); ?></span>
								</div>
								<?php
									/*
									 * Get and display institution's PhD Openings
									 */
									$phd_openings_args = array(
										'posts_per_page' => -1,
										'post_type' => 'phd-openings',
										'orderby' => 'ID',
										'order' => 'DESC',
										'meta_query' => array(
											array(
												'key' => 'institution',
												'value' => $slug,
												'compare' => '='
											)
										),
									);
									$phd_openings = get_posts($phd_openings_args);
									$phd_openings_number = count($phd_openings);
								?>
								<div class="uk-width-1-4">
									<i class="fa fa-clipboard"></i>
									<?php echo $phd_openings_number; ?>
									<span><?php echo __('Active PhD Offers', 'phdhub-cpts'); ?></span>
								</div>
							</div>
						</div>
					</div>
					
					<div class="institution-columns latest-offers-three-col-grid">
						<div class="container">
							<div class="uk-grid">
								<!-- Start Left Column -->
								<div class="uk-width-1-1">
									<h3>
										<span><?php echo __('PhD Offers', 'phdhub-cpts'); ?></span>
										<span class="all-offers">
											<a href="<?php echo site_url() . '/phd-openings'; ?>"><?php echo __('All PhD Offers', 'phdhub-cpts'); ?></a>
										</span>
									</h3>
									<div class="uk-grid institution-offer-rows">
										<?php
											/*
											 * Get and display institution's recent PhD Openings
											 */
											$phd_openings_args2 = array(
												'posts_per_page' => 3,
												'post_type' => 'phd-openings',
												'orderby' => 'ID',
												'order' => 'DESC',
												'meta_query' => array(
													array(
														'key' => 'institution',
														'value' => $slug,
														'compare' => '='
													)
												),
											);
											$phd_openings2 = get_posts($phd_openings_args2);
											
											foreach ($phd_openings2 as $phd_opening) {
												$post_type = get_post_type_object( get_post_type($phd_opening) );
												$post_cat = get_the_terms( $phd_opening->ID, 'fields-of-science');
												
												$short_info = substr( get_post_meta( $phd_opening->ID, 'phd_opening_info', true ), 0, 150 );
												$institution = esc_attr( get_post_meta( $phd_opening->ID, 'institution', true ) );
												$institution_details = get_page_by_path($institution, '', 'institutions');
												$research_areas = get_the_terms($phd_opening->ID, 'fields-of-science');
												
												$institution_logo = esc_url( get_post_meta( $institution_details->ID, 'institution_logo', true ) );
												$institution_city = esc_attr( get_post_meta( $institution_details->ID, 'city', true ) );
												$institution_country = esc_attr( get_post_meta( $institution_details->ID, 'country', true ) );
												$institution_country = esc_attr( get_post_meta( $institution_details->ID, 'country', true ) );
										?>
											<div class="uk-width-1-3">
												<div class="uk-grid phd-opening-box">
													<div class="uk-width-1-1 phd-opening-thumbnail">
														<span class="phd-opening-date">
															<span class="day"><?php echo get_the_date('d', $phd_opening->ID); ?></span>
															<span class="month"><?php echo get_the_date('M', $phd_opening->ID); ?></span>
														</span>
													</div>
													<div class="uk-width-1-1 phd-opening-details">
														<h4>
															<a href="<?php echo get_permalink( $phd_opening->ID ); ?>">
																<?php echo $phd_opening->post_title; ?>
															</a>
														</h4>
														<p class="description"><?php echo $short_info . '...'; ?></p>
														<p class="phd-categories">
															<span>
																<?php
																	foreach ($research_areas as $research_area) {
																?>
																<a href="<?php echo get_term_link( $research_area, 'fields-of-science' ); ?>"><?php echo $research_area->name; ?></a>
																<?php
																	}
																?>
															</span>
														</p>
														<p class=" phd-opening-info">
															<?php
																// PhD Opening's Image
																if ( $institution_logo != NULL ) {
															?>
															<a href="<?php echo get_permalink( $institution_details->ID ); ?>"><img src="<?php echo $institution_logo; ?>" alt="Logo"></a>
															<?php
																}
															?>
															<a href="<?php echo get_permalink( $institution_details ); ?>">
																<?php echo $institution_details->post_title; ?>
																<span><?php echo $institution_city . ', ' . $institution_country; ?></span>
															</a>
														</p>
													</div>
												</div>
											</div>
										<?php
											}
										?>
									</div>
								</div>
								<!-- End Right Column -->
								<!-- Start Left Column -->
								<div class="uk-width-1-1 cooperation-calls">
									<h3>
										<span><?php echo __('Calls for Cooperation', 'phdhub-cpts'); ?></span>
										<span class="all-offers">
											<a href="<?php echo site_url() . '/cooperation-calls'; ?>"><?php echo __('All Calls for Cooperation', 'phdhub-cpts'); ?></a>
										</span>
									</h3>
									<div class="uk-grid institution-offer-rows">
										<?php
											/*
											 * Get and display institution's recent PhD Openings
											 */
											$cooperations_args = array(
												'posts_per_page' => 3,
												'post_type' => 'cooperation-calls',
												'orderby' => 'ID',
												'order' => 'DESC',
												'meta_query' => array(
													array(
														'key' => 'institution',
														'value' => $slug,
														'compare' => '='
													)
												),
											);
											$cooperations = get_posts($cooperations_args);
											
											foreach ($cooperations as $cooperation_call) {
												$post_type = get_post_type_object( get_post_type($cooperation_call) );
												$post_cat = get_the_terms( $cooperation_call->ID, 'fields-of-science');
												
												$short_info = esc_attr( get_post_meta( $cooperation_call->ID, 'short_info', true ) );
												$institution = esc_attr( get_post_meta( $cooperation_call->ID, 'institution', true ) );
												$institution_details = get_page_by_path($institution, '', 'institutions');
												$research_areas = get_the_terms($cooperation_call->ID, 'fields-of-science');
												
												$institution_logo = esc_url( get_post_meta( $institution_details->ID, 'institution_logo', true ) );
												$institution_city = esc_attr( get_post_meta( $institution_details->ID, 'city', true ) );
												$institution_country = esc_attr( get_post_meta( $institution_details->ID, 'country', true ) );
												$company = esc_attr( get_post_meta( $cooperation_call->ID, 'company', true ) );
												$company_details = get_page_by_path($company, '', 'companies');
												$company_logo = esc_url( get_post_meta( $company_details->ID, 'company_logo', true ) );
												$company_city = esc_attr( get_post_meta( $company_details->ID, 'city', true ) );
												$company_country = esc_attr( get_post_meta( $company_details->ID, 'country', true ) );
										?>
											<div class="uk-width-1-3">
												<div class="uk-grid phd-opening-box">
													<div class="uk-width-1-1 phd-opening-thumbnail">
														<span class="phd-opening-date">
															<span class="day"><?php echo get_the_date('d', $cooperation_call->ID); ?></span>
															<span class="month"><?php echo get_the_date('M', $cooperation_call->ID); ?></span>
														</span>
													</div>
													<div class="uk-width-1-1 phd-opening-details">
														<h4>
															<a href="<?php echo get_permalink( $cooperation_call->ID ); ?>">
																<?php echo $cooperation_call->post_title; ?>
															</a>
														</h4>
														<p class="description"><?php echo $short_info; ?></p>
														<p class=" phd-opening-info call-info">
															<span>
																<?php
																	// PhD Opening's Image
																	if ( $institution_logo != NULL ) {
																?>
																<a href="<?php echo get_permalink( $institution_details->ID ); ?>"><img src="<?php echo $institution_logo; ?>" alt="Logo"></a>
																<?php
																	}
																?>
																<a href="<?php echo get_permalink( $institution_details ); ?>">
																	<?php echo $institution_details->post_title; ?>
																	<span><?php echo $institution_city . ', ' . $institution_country; ?></span>
																</a>
															</span>
															<span>
																<?php
																	// Company's Logo
																	if ( $company_logo != NULL ) {
																?>
																<a href="<?php echo get_permalink( $company_details ); ?>"><img src="<?php echo $company_logo; ?>" alt="Logo"></a>
																<?php
																	}
																?>
																<a href="<?php echo get_permalink( $company_details ); ?>">
																	<?php echo $company_details->post_title; ?>
																	<span><?php echo $company_city . ', ' . $company_country; ?></span>
																</a>
															</span>
														</p>
													</div>
												</div>
											</div>
										<?php
											}
										?>
									</div>
								</div>
								<!-- End Right Column -->
							</div>
						</div>
					</div>
					
					<?php
						if (!empty ($faculties)) {
					?>
					<div class="institution-faculties">
						<div class="container">
							<h3><?php echo __('Faculties', 'phdhub-cpts'); ?></h3>
							<div data-uk-slideset="{default: 4}">
								<ul class="uk-grid uk-slideset">
									<?php
										foreach ($faculties as $faculty) {
									?>
									<li>
										<p>
											<a href="<?php echo get_permalink( $faculty->ID ); ?>">
												<?php echo get_the_post_thumbnail( $faculty->ID, 'medium' ); ?>
												<span><?php echo $faculty->post_title; ?></span>
											</a>
										</p>
									</li>
									<?php
										}
									?>
								</ul>
								<a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
								<a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
							</div>
						</div>
					</div>
					<?php
						}
						
						/*
						 * Get and display institution's PhD Programs
						 */
						$phd_programs_args = array(
							'posts_per_page' => -1,
							'post_type' => 'phd-programs',
							'orderby' => 'title',
							'order' => 'ASC',
							'meta_query' => array(
								array(
									'key' => 'institution',
									'value' => $slug,
									'compare' => '='
								)
							),
						);
						$phd_programs = get_posts($phd_programs_args);
						
						if (!empty ($phd_programs)) {
					?>
					<div class="institution-faculties">
						<div class="container">
							<h3><?php echo __('PhD Programs', 'phdhub-cpts'); ?></h3>
							<div data-uk-slideset="{default: 4}">
								<ul class="uk-grid uk-slideset">
									<?php
										foreach ($phd_programs as $phd_program) {
									?>
									<li>
										<p>
											<a href="<?php echo get_permalink( $phd_program->ID ); ?>">
												<?php echo get_the_post_thumbnail( $phd_program->ID, 'medium' ); ?>
												<span><?php echo $phd_program->post_title; ?></span>
											</a>
										</p>
									</li>
									<?php
										}
									?>
								</ul>
								<a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
								<a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
							</div>
						</div>
					</div>
					<?php
						}
						
						/*
						 * Get and display institution's PhD Programs
						 */
						$research_teams_args = array(
							'posts_per_page' => -1,
							'post_type' => 'research-teams',
							'orderby' => 'title',
							'order' => 'ASC',
							'meta_query' => array(
								array(
									'key' => 'institution',
									'value' => $slug,
									'compare' => '='
								)
							),
						);
						$research_teams = get_posts($research_teams_args);
						
						if (!empty ($research_teams)) {
					?>
					<div class="institution-faculties">
						<div class="container">
							<h3><?php echo __('Research Teams', 'phdhub-cpts'); ?></h3>
							<div data-uk-slideset="{default: 4}">
								<ul class="uk-grid uk-slideset">
									<?php
										foreach ($research_teams as $research_team) {
									?>
									<li>
										<p>
											<a href="<?php echo get_permalink( $research_team->ID ); ?>">
												<?php echo get_the_post_thumbnail( $research_team->ID, 'medium' ); ?>
												<span><?php echo $research_team->post_title; ?></span>
											</a>
										</p>
									</li>
									<?php
										}
									?>
									<a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
									<a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
								</ul>
							</div>
						</div>
					</div>
					<?php
						}
					?>
				</article>
				<!-- End Post -->
			</div>
		<?php

			endwhile; // End of the loop.
		?>
	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
