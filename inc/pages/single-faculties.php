<?php
/*
 * PhDHub CPTs
 * Single page file for Faculties CPT
 */
defined('ABSPATH') or die;

get_header(); 

?>
<div id="phdhub-cpt-page">
	<main id="phdhub-cpt-main" class="site-main">
		<?php
			while ( have_posts() ) : the_post();
				$post_id = get_the_ID();

				/*
				 * Get custom meta fields based on faculty's ID
				 */
				$institution = esc_attr( get_post_meta( $post_id, 'institution', true ) );
				$institution_details = get_page_by_path($institution, '', 'institutions');
				$professors = esc_attr( get_post_meta( $post_id, 'professors', true ) );
				$students = esc_attr( get_post_meta( $post_id, 'students', true ) );
				$address = esc_attr( get_post_meta( $post_id, 'address', true ) );
				$postcode = esc_attr( get_post_meta( $post_id, 'postcode', true ) );
				$email = esc_attr( get_post_meta( $post_id, 'email', true ) );
				$phone_numbers = esc_attr( get_post_meta( $post_id, 'phone_numbers', true ) );
				$fax = esc_attr( get_post_meta( $post_id, 'fax', true ) );
				$website = esc_url( get_post_meta( $post_id, 'website', true ) );

				$contact_info = apply_filters( 'phdhub_cpt_content', get_post_meta( $post_id, 'contact_info', true ));
				$faculty_info = apply_filters( 'phdhub_cpt_content', get_post_meta( $post_id, 'faculty_info', true ));
			?>
			<div id="faculty-content">
				<!-- Start Post -->
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<figure class="uk-overlay">
						<?php
							/*
							 * Display institutions's thumbnail
							 */
							if ( has_post_thumbnail() ) {
								the_post_thumbnail( 'full' );
							}
						?>
						<figcaption class="uk-overlay-panel uk-overlay-background uk-flex uk-flex-bottom">
							<div class="container">
								<?php
									/*
									 * Display faculty's name (Post Title)
									 */
									if ( is_single() ) :
										the_title( '<h1 class="faculty-name">', '</h1>' );
									else :
										the_title( '<h2 class="faculty-name"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
									endif;

									if ( !empty ($institution) ) {
								?>
								<p class="faculty-institution">
									<i class="fa fa-university"></i> 
									<a href="<?php echo get_permalink( $institution_details ); ?>">
										<?php echo $institution_details->post_title; ?>
									</a>
								</p>
								<?php
									}
									if (!empty ($website)) {
								?>
								<p class="website">
									<i class="fa fa-globe"></i> <a href="<?php echo $website; ?>"><?php echo __('Visit Website', 'phdhub-cpts'); ?></a>
								</p>
								<?php
									}
								?>
							</div>
						</figcaption>
					</figure>
					
					<div class="container">
						<div class="uk-grid faculty-columns">
							<!-- Start Left Column -->
							<div class="uk-width-7-10 about-faculty">
								<?php
									/*
									 * Display faculty's general info if the field is not empty
									 */
									if (!empty ($faculty_info)) {
								?>
								<!-- Start Meta Field: Faculty's General Info -->
								<div class="faculty-info">
									<?php
										echo apply_filters('the_content', $faculty_info);
									?>
								</div>
								<!-- End Meta Field: Faculty's General Info -->
								<?php
									}
								?>
							</div>
							<!-- End Left Column -->
							<!-- Start Right Column -->
							<div class="uk-width-3-10">
								<?php 
									/*
									 * Display faculty's contact info if the field is not empty
									 */
									if ( (!empty ($contact_info)) || (!empty ($address)) || (!empty ($postcode)) || (!empty ($email)) || (!empty ($phone_numbers)) || (!empty ($fax)) ){
								?>
								<!-- Start Meta Field: Contact Info -->
								<div class="contact-info">
									<p>
										<span><?php echo __('Contact Info', 'phdhub-cpts'); ?>:</span>
									</p>
									<?php
										if (!empty ($address)) {
									?>
									<span><?php echo $address; ?></span>
									<?php
										}
										if (!empty ($postcode)) {
									?>
									<span><?php echo $postcode; ?></span>
									<?php
										}
										if (!empty ($email)) {
									?>
									<span><?php echo $email; ?></span>
									<?php
										}
										if (!empty ($phone_numbers)) {
									?>
									<span><?php echo $phone_numbers; ?></span>
									<?php
										}
										if (!empty ($fax)) {
									?>
									<span><?php echo $fax; ?></span>
									<?php
										}
										if (!empty ($contact_info)) {
											echo $contact_info;
										}
									?>
								</div>
								<!-- End Meta Field: Contact Info -->
								<?php
									}
								?>
							</div>
							<!-- End Right Column -->
						</div>
					</div>
					
					
					<div class="faculty-statistics">
						<div class="container">
							<div class="uk-grid">
								<?php
									if (!empty ($professors)) {
								?>
								<div class="uk-width-1-4">
									<i class="fa fa-users"></i>
									<?php echo $professors; ?>
									<span><?php echo __('Professors', 'phdhub-cpts'); ?></span>
								</div>
								<?php
									}
									if (!empty ($students)) {
								?>
								<div class="uk-width-1-4">
									<i class="fa fa-graduation-cap"></i>
									<?php echo $students; ?>
									<span><?php echo __('Students', 'phdhub-cpts'); ?></span>
								</div>
								<?php
									}
									
									/*
									 * Get and display faculty's PhD Programs
									 */
									$phd_programs_args = array(
										'posts_per_page' => -1,
										'post_type' => 'phd-programs',
										'orderby' => 'title',
										'order' => 'ASC',
										'meta_query' => array(
											array(
												'key' => 'faculty',
												'value' => $post_id,
												'compare' => '='
											)
										),
									);
									$phd_programs = get_posts($phd_programs_args);
									$phd_programs_number = count($phd_programs);
								?>
								<div class="uk-width-1-4">
									<i class="fa fa-university"></i>
									<?php echo $phd_programs_number; ?>
									<span><?php echo __('PhD Programs', 'phdhub-cpts'); ?></span>
								</div>
								<?php
									/*
									 * Get and display faculty's research teams
									 */
									$research_teams_args = array(
										'posts_per_page' => -1,
										'post_type' => 'research-teams',
										'orderby' => 'title',
										'order' => 'ASC',
										'meta_query' => array(
											array(
												'key' => 'faculty',
												'value' => $post_id,
												'compare' => '='
											)
										),
									);
									$research_teams = get_posts($research_teams_args);
									$research_teams_number = count($research_teams);
								?>
								<div class="uk-width-1-4">
									<i class="fa fa-flask"></i>
									<?php echo $research_teams_number; ?>
									<span><?php echo __('Research Teams', 'phdhub-cpts'); ?></span>
								</div>
							</div>
						</div>
					</div>
					
					
					
					<div class="faculty-offers latest-offers-three-col-grid">
						<div class="container">
							<div class="uk-grid">
								<!-- Start Left Column -->
								<div class="uk-width-1-1">
									<h3>
										<span><?php echo __('PhD Offers', 'phdhub-cpts'); ?></span>
										<span class="all-offers">
											<a href="<?php echo site_url() . '/phd-openings'; ?>"><?php echo __('All PhD Offers', 'phdhub-cpts'); ?></a>
										</span>
									</h3>
									<div class="uk-grid institution-offer-rows">
										<?php
											/*
											 * Get and display institution's recent PhD Openings
											 */
											$phd_openings_args2 = array(
												'posts_per_page' => 3,
												'post_type' => 'phd-openings',
												'orderby' => 'ID',
												'order' => 'DESC',
												'meta_query' => array(
													array(
														'key' => 'faculty',
														'value' => $post_id,
														'compare' => '='
													)
												),
											);
											$phd_openings2 = get_posts($phd_openings_args2);
											
											foreach ($phd_openings2 as $phd_opening) {
												$post_type = get_post_type_object( get_post_type($phd_opening) );
												$post_cat = get_the_terms( $phd_opening->ID, 'fields-of-science');
												
												$short_info = esc_attr( get_post_meta( $phd_opening->ID, 'short_info', true ) );
												$institution = esc_attr( get_post_meta( $phd_opening->ID, 'institution', true ) );
												$institution_details = get_page_by_path($institution, '', 'institutions');
												$research_areas = get_the_terms($phd_opening->ID, 'fields-of-science');
												
												$institution_logo = esc_url( get_post_meta( $institution_details->ID, 'institution_logo', true ) );
												$institution_city = esc_attr( get_post_meta( $institution_details->ID, 'city', true ) );
												$institution_country = esc_attr( get_post_meta( $institution_details->ID, 'country', true ) );
												$institution_country = esc_attr( get_post_meta( $institution_details->ID, 'country', true ) );
										?>
											<div class="uk-width-1-3">
												<div class="uk-grid phd-opening-box">
													<div class="uk-width-1-1 phd-opening-thumbnail">
														<span class="phd-opening-date">
															<span class="day"><?php echo get_the_date('d', $phd_opening->ID); ?></span>
															<span class="month"><?php echo get_the_date('M', $phd_opening->ID); ?></span>
														</span>
													</div>
													<div class="uk-width-1-1 phd-opening-details">
														<h4>
															<a href="<?php echo get_permalink( $phd_opening->ID ); ?>">
																<?php echo $phd_opening->post_title; ?>
															</a>
														</h4>
														<p class="description"><?php echo $short_info; ?></p>
														<p class="phd-categories">
															<span>
																<?php
																	foreach ($research_areas as $research_area) {
																?>
																<a href="<?php echo get_term_link( $research_area, 'fields-of-science' ); ?>"><?php echo $research_area->name; ?></a>
																<?php
																	}
																?>
															</span>
														</p>
														<p class=" phd-opening-info">
															<?php
																// PhD Opening's Image
																if ( $institution_logo != NULL ) {
															?>
															<a href="<?php echo get_permalink( $institution_details->ID ); ?>"><img src="<?php echo $institution_logo; ?>" alt="Logo"></a>
															<?php
																}
															?>
															<a href="<?php echo get_permalink( $institution_details ); ?>">
																<?php echo $institution_details->post_title; ?>
																<span><?php echo $institution_city . ', ' . $institution_country; ?></span>
															</a>
														</p>
													</div>
												</div>
											</div>
										<?php
											}
										?>
									</div>
								</div>
								<!-- End Right Column -->
							</div>
						</div>
					</div>
					
					<?php
						/*
						 * Get and display institution's PhD Programs
						 */
						$phd_programs_args = array(
							'posts_per_page' => -1,
							'post_type' => 'phd-programs',
							'orderby' => 'title',
							'order' => 'ASC',
							'meta_query' => array(
								array(
									'key' => 'faculty',
									'value' => $post_id,
									'compare' => '='
								)
							),
						);
						$phd_programs = get_posts($phd_programs_args);
						
						if (!empty ($phd_programs)) {
					?>
					<div class="institution-faculties">
						<div class="container">
							<h3><?php echo __('PhD Programs', 'phdhub-cpts'); ?></h3>
							<div data-uk-slideset="{default: 4}">
								<ul class="uk-grid uk-slideset">
									<?php
										foreach ($phd_programs as $phd_program) {
									?>
									<li>
										<p>
											<a href="<?php echo get_permalink( $phd_program->ID ); ?>">
												<?php echo get_the_post_thumbnail( $phd_program->ID, 'medium' ); ?>
												<span><?php echo $phd_program->post_title; ?></span>
											</a>
										</p>
									</li>
									<?php
										}
									?>
								</ul>
								<a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
								<a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
							</div>
						</div>
					</div>
					<?php
						}
						/*
						 * Get and display institution's PhD Programs
						 */
						$research_teams_args = array(
							'posts_per_page' => -1,
							'post_type' => 'research-teams',
							'orderby' => 'title',
							'order' => 'ASC',
							'meta_query' => array(
								array(
									'key' => 'faculty',
									'value' => $post_id,
									'compare' => '='
								)
							),
						);
						$research_teams = get_posts($research_teams_args);
						
						if (!empty ($research_teams)) {
					?>
					<div class="institution-faculties">
						<div class="container">
							<h3><?php echo __('Research Teams', 'phdhub-cpts'); ?></h3>
							<div data-uk-slideset="{default: 4}">
								<ul class="uk-grid uk-slideset">
									<?php
										foreach ($research_teams as $research_team) {
									?>
									<li>
										<p>
											<a href="<?php echo get_permalink( $research_team->ID ); ?>">
												<?php echo get_the_post_thumbnail( $research_team->ID, 'medium' ); ?>
												<span><?php echo $research_team->post_title; ?></span>
											</a>
										</p>
									</li>
									<?php
										}
									?>
									<a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
									<a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
								</ul>
							</div>
						</div>
					</div>
					<?php
						}
					?>
				</article>
				<!-- End Post -->
			</div>
		<?php

			endwhile; // End of the loop.
		?>
	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
