<?php
/*
 * PhDHub CPTs
 * Single page file for Companies CPT
 */
defined('ABSPATH') or die;

get_header(); 

?>
<div id="phdhub-cpt-page">
	<main id="phdhub-cpt-main" class="site-main">
		<?php
			while ( have_posts() ) : the_post();
				$post_id = get_the_ID();

				/*
				 * Get custom meta fields based on company's ID
				 */
				$ceo = esc_attr( get_post_meta( $post_id, 'ceo', true ) );
				$establishment = esc_attr( get_post_meta( $post_id, 'establishment', true ) );
				$employees = esc_attr( get_post_meta( $post_id, 'employees', true ) );
				$city = esc_attr( get_post_meta( $post_id, 'city', true ) );
				$country = esc_attr( get_post_meta( $post_id, 'country', true ) );
				$logo = esc_attr( get_post_meta( $post_id, 'company_logo', true ) );
				$vat_number = esc_attr( get_post_meta( $post_id, 'vat_number', true ) );
				$website = esc_url( get_post_meta( $post_id, 'website', true ) );
				$address = esc_attr( get_post_meta( $post_id, 'address', true ) );
				$postcode = esc_attr( get_post_meta( $post_id, 'postcode', true ) );
				$email = esc_attr( get_post_meta( $post_id, 'email', true ) );
				$phone_numbers = esc_attr( get_post_meta( $post_id, 'phone_numbers', true ) );
				$fax = esc_attr( get_post_meta( $post_id, 'fax', true ) );
				$website = esc_url( get_post_meta( $post_id, 'website', true ) );
				$long = esc_attr( get_post_meta( $post_id, 'longitude', true ) );
				$lat = esc_attr( get_post_meta( $post_id, 'latitude', true ) );

				$contact_info = apply_filters( 'phdhub_cpt_content', get_post_meta( $post_id, 'contact_info', true ));
				$company_info = apply_filters( 'phdhub_cpt_content', get_post_meta( $post_id, 'company_info', true ));

				$post = get_post($post_id); 
				$slug = $post->post_name;
			?>
			<div id="company-content">
				<!-- Start Post -->
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<figure class="uk-overlay">
						<?php
							/*
							 * Display company's thumbnail
							 */
							if ( has_post_thumbnail() ) {
								the_post_thumbnail( 'full' );
							}
						?>
						<figcaption class="uk-overlay-panel uk-overlay-background uk-flex uk-flex-bottom">
							<div class="container">
								<?php
									if (!empty ($logo)) {
								?>
								<div class="company-image">
									<img src="<?php echo $logo; ?>" alt="Logo">
								</div>
								<?php
									}
								?>
								
								<div class="company-details-right">
									<?php
										/*
										 * Display company's title (Post Title)
										 */
										if ( is_single() ) :
											the_title( '<h1 class="company-title">', '</h1>' );
										else :
											the_title( '<h2 class="company-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
										endif;

										if ( (!empty ($country)) || (!empty ($city)) ) {
									?>
									<p class="location">
										<i class="fa fa-map-marker"></i> <?php echo $city . ', ' . $country; ?>
									</p>
									<?php
										}
										if (!empty ($website)) {
									?>
									<p class="website">
										<i class="fa fa-globe"></i> <a href="<?php echo $website; ?>"><?php echo __('Visit Website', 'phdhub-cpts'); ?></a>
									</p>
									<?php
										}
									?>
								</div>
							</div>
						</figcaption>
					</figure>
					
					<div class="container">
						<div class="about-company">
							<div class="uk-grid">
								<div class="uk-width-7-10">
									<?php
										if (!empty ($company_info)) {
									?>
									<!-- Start Meta Field: Company's General Info -->
									<div class="company-info">
										<?php
											echo apply_filters('the_content', $company_info);
										?>
									</div>
									<!-- End Meta Field: Company's General Info -->
									<?php
										}
									?>
									<div class="company-details">
										<p>
											<?php
												/*
												 * Display company's ceo if it's not empty
												 */
												if (!empty ($ceo)) {
											?>
											<!-- Start Meta Field: CEO -->
											<span>
												<i class="fa fa-user-circle-o"></i> <?php echo $ceo . ' (' . __('CEO', 'phdhub-cpts') . ')'; ?>
											</span>
											<!-- End Meta Field: CEO -->
											<?php
												}
												/*
												 * Display company's year of establishment if it's not empty
												 */
												if (!empty ($establishment)) {
											?>
											<!-- Start Meta Field: Establishment -->
											<span>
												<i class="fa fa-calendar-o"></i> <?php echo __('Established in', 'phdhub-cpts') . ': ' . $establishment; ?>
											</span>
											<!-- End Meta Field: Establishment -->
											<?php
												}
												/*
												 * Display company's number of employers if it's not empty
												 */
												if (!empty ($employees)) {
											?>
											<!-- Start Meta Field: Employees -->
											<span>
												<i class="fa fa-users"></i> <?php echo $employees . ' ' . __('employees', 'phdhub-cpts'); ?>
											</span>
											<!-- End Meta Field: Employees -->
											<?php
												}
												/*
												 * Display company's vat number if it's not empty
												 */
												if (!empty ($vat_number)) {
											?>
											<!-- Start Meta Field: VAT Number -->
											<span>
												<i class="fa fa-id-badge"></i> <?php echo __('VAT Number', 'phdhub-cpts') . ': ' . $vat_number; ?>
											</span>
											<!-- End Meta Field: VAT Number -->
											<?php
												}
											?>
										</p>
									</div>
								</div>
								<div class="uk-width-3-10">
									<?php
										if ( (!empty ($contact_info)) || (!empty ($address)) || (!empty ($postcode)) || (!empty ($email)) || (!empty ($phone_numbers)) || (!empty ($fax)) ){
									?>
									<div class="contact-info">
										<h3>
											<span><?php echo __('Contact Info', 'phdhub-cpts'); ?></span>
										</h3>
										<?php
											if (!empty ($address)) {
										?>
										<span><?php echo $address; ?></span>
										<?php
											}
											if (!empty ($postcode)) {
										?>
										<span><?php echo $postcode; ?></span>
										<?php
											}
											if (!empty ($email)) {
										?>
										<span><?php echo $email; ?></span>
										<?php
											}
											if (!empty ($phone_numbers)) {
										?>
										<span><?php echo $phone_numbers; ?></span>
										<?php
											}
											if (!empty ($fax)) {
										?>
										<span><?php echo $fax; ?></span>
										<?php
											}
											if (!empty ($contact_info)) {
												echo $contact_info;
											}
										?>
									</div>
									<?php
										}
									?>
								</div>
							</div>
						</div>
					</div>
					
					<?php
						if ( !empty( $long ) && !empty( $lat ) ) {
					?>
					<div id="map-area" style="height: 480px"></div>
					<script type="text/javascript">
						var mymap = L.map('map-area').setView([<?php echo $long; ?>, <?php echo $lat; ?>], 16);
						L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
							attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
							maxZoom: 18,
							id: 'mapbox.streets',
							accessToken: 'pk.eyJ1IjoiaXphY2hyb3MiLCJhIjoiY2pvcHJ0aHEzMXR0ZDNwcWF4YndtN3pnYiJ9.3cu-dWnd2OEdKIm_nhO6dQ'
						}).addTo(mymap);
						var marker = L.marker([<?php echo $long; ?>, <?php echo $lat; ?>]).addTo(mymap);
					</script>
					<?php
						}
					?>
								
					<div class="company-calls">
						<div class="container">
							<h3>
								<?php echo __('Calls for Cooperation', 'phdhub-cpts'); ?>
								<span class="all-calls"><a href="<?php echo site_url() . '/cooperation-calls'; ?>"><?php echo __('All Calls for Cooperation', 'phdhub-cpts'); ?></a></span>
							</h3>
							<div class="uk-grid">
								<?php
									/*
									 * Get and display company's calls for cooperation
									 */
									$calls_args = array(
										'posts_per_page' => -1,
										'post_type' => 'cooperation-calls',
										'orderby' => 'ID',
										'order' => 'DESC',
										'meta_query' => array(
											array(
												'key' => 'company',
												'value' => $slug,
												'compare' => '='
											)
										),
									);
									$calls = get_posts($calls_args);
									foreach ($calls as $call) {
										$short_info = esc_attr( get_post_meta( $call->ID, 'short_info', true ) );
										$institution = esc_attr( get_post_meta( $call->ID, 'institution', true ) );
										$institution_details = get_page_by_path($institution, '', 'institutions');
										$institution_logo = esc_url( get_post_meta( $institution_details->ID, 'institution_logo', true ) );
										$institution_city = esc_attr( get_post_meta( $institution_details->ID, 'city', true ) );
										$institution_country = esc_attr( get_post_meta( $institution_details->ID, 'country', true ) );
										$company = esc_attr( get_post_meta( $call->ID, 'company', true ) );
										$company_details = get_page_by_path($company, '', 'companies');
										$company_logo = esc_url( get_post_meta( $company_details->ID, 'company_logo', true ) );
										$company_city = esc_attr( get_post_meta( $company_details->ID, 'city', true ) );
										$company_country = esc_attr( get_post_meta( $company_details->ID, 'country', true ) );
								?>
								<div class="uk-width-1-3">
									<div class="call-item">
										<p class="title">
											<a href="<?php echo get_permalink( $call->ID ); ?>"><?php echo $call->post_title; ?></a>
										</p>
										<?php
											if (!empty ($short_info)) {
										?>
										<p class="short-info">
											<?php echo $short_info; ?>
										</p>
										<?php
											}
										?>
										<p class="date">
											<i class="fa fa-calendar"></i>
											<?php the_date('d F Y'); ?>
										</p>
										<p class="call-info">
											<span>
												<?php
													// Institution's Logo
													if ( $institution_logo != NULL ) {
												?>
												<a href="<?php echo get_permalink( $institution_details ); ?>"><img src="<?php echo $institution_logo; ?>" alt="Logo"></a>
												<?php
													}
												?>
												<a href="<?php echo get_permalink( $institution_details ); ?>">
													<?php echo $institution_details->post_title; ?>
													<span><?php echo $institution_city . ', ' . $institution_country; ?></span>
												</a>
											</span>
											<span>
												<?php
													// Company's Logo
													if ( $company_logo != NULL ) {
												?>
												<a href="<?php echo get_permalink( $company_details ); ?>"><img src="<?php echo $company_logo; ?>" alt="Logo"></a>
												<?php
													}
												?>
												<a href="<?php echo get_permalink( $company_details ); ?>">
													<?php echo $company_details->post_title; ?>
													<span><?php echo $company_city . ', ' . $company_country; ?></span>
												</a>
											</span>
										</p>
									</div>
								</div>
								<?php
									}
								?>
							</div>
						</div>
					</div>
				</article>
				<!-- End Post -->
			</div>
		<?php

			endwhile; // End of the loop.
		?>
	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
