<?php
	defined('ABSPATH') or die;

	get_header();
	
	$form_fullname = (isset($_POST['form_fullname']) ? $_POST['form_fullname'] : null);
	$form_email = (isset($_POST['form_email']) ? $_POST['form_email'] : null);
	$form_tel = (isset($_POST['form_tel']) ? $_POST['form_tel'] : null);
	$form_subject = (isset($_POST['form_subject']) ? $_POST['form_subject'] : null);
	$form_message = (isset($_POST['form_message']) ? $_POST['form_message'] : null);
	$submit = (isset($_POST['send_mail']) ? $_POST['send_mail'] : null);
	$recipient_email = esc_attr( get_post_meta( get_the_ID(), 'recipient_email', true ) );
	
	if ( $submit ) {
		
		if (!empty ($recipient_email)) {
			$to = $recipient_email;
		} else {
			$to = get_bloginfo('admin_email');
		}
		$subject = $form_subject;
		$headers[] = 'From: '. $form_email . "\r\n";
		$headers[] = 'Reply-To: ' . $form_email . "\r\n";
		
		$message = $form_message;
		$sent = wp_mail( $to, $subject, strip_tags( $message ), $headers );
		wp_redirect( get_permalink() );
	}
?>
<div id="primary" class="content-area">
	<main id="main" class="site-main">
		<div id="contact-page">
			<header class="entry-header cpt-header">
				<div class="container">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</div>
			</header><!-- .entry-header -->
			
			<div class="container">
				<form class="contact-form" method="POST">
					<p class="required-notice"><?php echo __('All fields with an asterisk (*) are required!', 'phdhub-cpts'); ?></p>
					<div class="uk-grid">
						<div class="uk-width-1-2">
							<p>
								<input type="text" name="form_fullname" placeholder="<?php echo __('Full Name', 'phdhub-cpts') . '*'; ?>" required />
							</p>
						</div>
						<div class="uk-width-1-2">
							<p>
								<input type="email" name="form_email" placeholder="<?php echo __('Email', 'phdhub-cpts') . '*'; ?>" required />
							</p>
						</div>
						<div class="uk-width-1-2">
							<p>
								<input type="tel" name="form_tel" placeholder="<?php echo __('Phone Number', 'phdhub-cpts'); ?>" />
							</p>
						</div>
						<div class="uk-width-1-2">
							<p>
								<input type="text" name="form_subject" placeholder="<?php echo __('Subject', 'phdhub-cpts') . '*'; ?>" required />
							</p>
						</div>
					</div>
					<p>
						<textarea name="form_message" rows="8" placeholder="<?php echo __('Add your message here...', 'phdhub-cpts'); ?>"></textarea>
					</p>
					<p class="confirmation">
						<span>
							<?php echo __('This form collects your first name, last name, email and/or phone number so that we can reply to your message. Check out our', 'phdhub-cpts') . ' <a href="' . site_url() . '/privacy-policy">' . __('Privacy Policy', 'phdhub-cpts') . '</a> ' . __('for more info on how we protect and manage your submitted data!', 'phdhub-cpts'); ?>
							</span>
						<input type="checkbox" required><?php echo __('I consent to having this website store my submitted information so they can respond to my message.', 'phdhub-cpts'); ?>
					</p>
					<input type="submit" name="send_mail" value="<?php echo __('Send Message', 'phdhub-cpts'); ?>" />
				</form>
			</div>
		</div>
	</main>
</div>
<?php
	get_footer();
?>