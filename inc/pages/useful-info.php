<?php
	
	defined('ABSPATH') or die;

	get_header();
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main">
		<div id="useful-info-page">
			<header class="entry-header cpt-header">
				<div class="container">
					<?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
				</div>
			</header><!-- .entry-header -->

			<div class="container">
				<?php
					$page_id = get_the_ID();
					$practical_info = get_post_meta( $page_id, 'practical_info', true );
					$accomodation_info = get_post_meta( $page_id, 'accomodation_info', true );
					$entry_conditions = get_post_meta( $page_id, 'entry_conditions', true );
					$banking_info = get_post_meta( $page_id, 'banking_info', true );
					$insurance_info = get_post_meta( $page_id, 'insurance_info', true );
					$taxation_info = get_post_meta( $page_id, 'taxation_info', true );
					$phd_regulations = get_post_meta( $page_id, 'phd_regulations', true );
					$diplomas_recognition = get_post_meta( $page_id, 'diplomas_recognition', true );
				?> 
				<div class="useful-info-accordion">
					<div class="uk-accordion" data-uk-accordion>

 	   					<h3 class="uk-accordion-title"><?php echo __('Arriving', 'phdhub-cpts'); ?></h3>
    						<div class="uk-accordion-content">
							<?php
								if (!empty ($practical_info)) {
							?>
							<div class="useful-info-section">
								<h4><?php echo __('Practical Information', 'phdhub-cpts'); ?></h4>
								<?php echo $practical_info; ?>
							</div>
							<?php
								}
                                                                if (!empty ($accomodation_info)) {
                                                        ?>
                                                        <div class="useful-info-section">
                                                                <h4><?php echo __('Accomodation', 'phdhub-cpts'); ?></h4>
                                                                <?php echo $accomodation_info; ?>
                                                        </div>
                                                        <?php
                                                                }
                                                                if (!empty ($entry_conditions)) {
                                                        ?>
                                                        <div class="useful-info-section">
                                                                <h4><?php echo __('Entry Conditions/Visa', 'phdhub-cpts'); ?></h4>
                                                                <?php echo $entry_conditions; ?>
                                                        </div>
                                                        <?php
                                                                }
							?>
						</div>

    						<h3 class="uk-accordion-title"><?php echo __('Living', 'phdhub-cpts'); ?></h3>
    						<div class="uk-accordion-content">
							<?php
                                                                if (!empty ($banking_info)) {
                                                        ?>
                                                        <div class="useful-info-section">
                                                                <h4><?php echo __('Banking', 'phdhub-cpts'); ?></h4>
                                                                <?php echo $banking_info; ?>
                                                        </div>
                                                        <?php
                                                                }
                                                                if (!empty ($insurance_info)) {
                                                        ?>
                                                        <div class="useful-info-section">
                                                                <h4><?php echo __('Insurance/Medical Care', 'phdhub-cpts'); ?></h4>
                                                                <?php echo $insurance_info; ?>
                                                        </div>
                                                        <?php
                                                                }
                                                                if (!empty ($taxation_info)) {
                                                        ?>
                                                        <div class="useful-info-section">
                                                                <h4><?php echo __('Taxation/Salaries', 'phdhub-cpts'); ?></h4>
                                                                <?php echo $taxation_info; ?>
                                                        </div>
                                                        <?php
                                                                }
							?>
						</div>

   	 					<h3 class="uk-accordion-title"><?php echo __('Doing a PhD', 'phdhub-cpts'); ?></h3>
    						<div class="uk-accordion-content">
							<?php
                                                                if (!empty ($phd_regulations)) {
                                                        ?>
                                                        <div class="useful-info-section">
                                                                <h4><?php echo __('PhD Regulations', 'phdhub-cpts'); ?></h4>
                                                                <?php echo $phd_regulations; ?>
                                                        </div>
                                                        <?php
                                                                }
                                                                if (!empty ($diplomas_recognition)) {
                                                        ?>
                                                        <div class="useful-info-section">
                                                                <h4><?php echo __('Recognition of Diplomas', 'phdhub-cpts'); ?></h4>
                                                                <?php echo $diplomas_recognition; ?>
                                                        </div>
                                                        <?php
                                                                }
							?>
						</div>

					</div>
				</div>
			</div>
		</div>
	</main>
</div>
<?php
	get_footer();
?>
