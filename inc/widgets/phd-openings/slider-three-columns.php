<?php
/*
 * PhD Hub - PhD Openings Widget
 * Style: Grid Two Columns
 */
defined('ABSPATH') or die;
?>
<div class="latest-offers-three-col-grid">
	<div data-uk-slideset="{default: 3}">
		<ul class="uk-grid uk-slideset">
	<?php
		$phd_openings_args = array(
			'posts_per_page' => $number_of_openings,
			'post_type' => 'phd-openings',
			'orderby' => 'ID',
			'order' => 'DESC'
		);
		/*
		 * Get PhD Openings based on previous arguments
		 */
		$phd_openings = get_posts($phd_openings_args);
		/*
		 * For every PhD Opening, display its info
		 */
		foreach ($phd_openings as $phd_opening) {
			$post_type = get_post_type_object( get_post_type($phd_opening) );
			$post_cat = get_the_terms( $phd_opening->ID, 'fields-of-science');
			
			$short_info = substr( get_post_meta( $phd_opening->ID, 'phd_opening_info', true ), 0, 200 );
			$institution = esc_attr( get_post_meta( $phd_opening->ID, 'institution', true ) );
			$institution_details = get_page_by_path($institution, '', 'institutions');
			$research_areas = get_the_terms($phd_opening->ID, 'fields-of-science');
			
			$institution_logo = esc_url( get_post_meta( $institution_details->ID, 'institution_logo', true ) );
			$institution_city = esc_attr( get_post_meta( $institution_details->ID, 'city', true ) );
			$institution_country = esc_attr( get_post_meta( $institution_details->ID, 'country', true ) );
			$institution_country = esc_attr( get_post_meta( $institution_details->ID, 'country', true ) );
	?>
		<li>
			<div class="uk-grid phd-opening-box">
				<div class="uk-width-1-1 phd-opening-thumbnail">
					<span class="phd-opening-date">
						<span class="day"><?php echo get_the_date('d', $phd_opening->ID); ?></span>
						<span class="month"><?php echo get_the_date('M', $phd_opening->ID); ?></span>
					</span>
				</div>
				<div class="uk-width-1-1 phd-opening-details">
					<h4>
						<a href="<?php echo get_permalink( $phd_opening->ID ); ?>">
							<?php echo $phd_opening->post_title; ?>
						</a>
					</h4>
					<p class="description"><?php echo strip_tags($short_info) . '...'; ?></p>
					<p class="phd-categories">
						<span>
							<?php
								foreach ($research_areas as $research_area) {
							?>
							<a href="<?php echo get_term_link( $research_area, 'fields-of-science' ); ?>"><?php echo $research_area->name; ?></a>
							<?php
								}
							?>
						</span>
					</p>
					<p class=" phd-opening-info">
						<?php
							// PhD Opening's Image
							if ( $institution_logo != NULL ) {
						?>
						<a href="<?php echo get_permalink( $institution_details->ID ); ?>"><img src="<?php echo $institution_logo; ?>" alt="Logo"></a>
						<?php
							}
						?>
						<a href="<?php echo get_permalink( $institution_details ); ?>">
							<?php echo $institution_details->post_title; ?>
							<span><?php echo $institution_city . ', ' . $institution_country; ?></span>
						</a>
					</p>
				</div>
			</div>
		</li>
	<?php
		}
	?>
	</ul>
								<a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous" title="Previous PhD Offers"><span class="uk-hidden"><?php echo __('Previous PhD Offers', 'phdhub-cpts'); ?></span></a>
								<a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next" title="Next PhD Offers"><span class="uk-hidden"><?php echo __('Next PhD Offers', 'phdhub-cpts'); ?></span></a>

	</div>
</div>
