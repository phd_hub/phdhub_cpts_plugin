<?php
/*
 * PhD Hub - PhD Openings Widget
 * Style: Grid Two Columns
 */
defined('ABSPATH') or die;
?>
<div class="latest-offers-two-col-grid">
	<?php
		$phd_openings_args = array(
			'posts_per_page' => $number_of_openings,
			'post_type' => 'phd-openings',
			'orderby' => 'ID',
			'order' => 'DESC'
		);
		/*
		 * Get PhD Openings based on previous arguments
		 */
		$phd_openings = get_posts($phd_openings_args);
		/*
		 * For every PhD Opening, display its info
		 */
		foreach ($phd_openings as $phd_opening) {
			$post_type = get_post_type_object( get_post_type($phd_opening) );
			$post_cat = get_the_terms( $phd_opening->ID, 'fields-of-science');
			
			$short_info = esc_attr( get_post_meta( $phd_opening->ID, 'short_info', true ) );
			$institution = esc_attr( get_post_meta( $phd_opening->ID, 'institution', true ) );
			$institution_details = get_page_by_path($institution, '', 'institutions');
			$research_areas = get_the_terms($phd_opening->ID, 'fields-of-science');
	?>
	<div class="uk-grid">
		<div class="uk-width-1-2">
			<div class="uk-grid phd-opening-box">
				<div class="uk-width-2-10 phd-opening-thumbnail">
					<a href="<?php echo get_permalink( $phd_opening->ID ); ?>">
						<?php echo get_the_post_thumbnail($phd_opening->ID, 'thumbnail'); ?>
					</a>
					<span class="phd-opening-date">
						<span class="day"><?php echo get_the_date('d', $phd_opening->ID); ?></span>
						<span class="month"><?php echo get_the_date('M', $phd_opening->ID); ?></span>
					</span>
				</div>
				<div class="uk-width-8-10 phd-opening-details">
					<h4>
						<a href="<?php echo get_permalink( $phd_opening->ID ); ?>">
							<?php echo $phd_opening->post_title; ?>
						</a>
					</h4>
					<p class="description"><?php echo $short_info; ?></p>
					<p class="phd-opening-info">
						<a href="<?php echo get_permalink( $institution_details ); ?>"><?php echo $institution_details->post_title; ?></a>
						<span>
							<i class="fa fa-tags"></i>
							<?php
								foreach ($research_areas as $research_area) {
							?>
							<a href="<?php echo get_term_link( $research_area, 'fields-of-science' ); ?>"><?php echo $research_area->name; ?></a>
							<?php
								}
							?>
						</span>
					</p>
				</div>
			</div>
		</div>
	</div>
	<?php
		}
	?>
</div>