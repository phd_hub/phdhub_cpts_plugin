<?php
/*
 * PhD Hub - Research Areas Widget
 * Style: List Four Columns
 */
defined('ABSPATH') or die;
?>
<div class="research-areas-four-columns-list">
	<div class="uk-grid">
		<?php
			/*
			 * Display/Hide empty Research Areas based on widget's settings
			 */
			if ($empty_research_areas == 'no') {
				$hide_empty = false;
			}
			else {
				$hide_empty = true;
			}
			$research_areas = get_terms(array( 'taxonomy' => 'fields-of-science', 'hide_empty' => $hide_empty ));
			/*
			 * For every Research Area display its name and the number of the associated PhD Offers
			 */
			foreach ($research_areas as $research_area) {
				$research_area_link = get_term_link( $research_area, 'fields-of-science' );
		?>
		<div class="uk-width-1-4">
			<p>
				<a href="<?php echo $research_area_link; ?>">
					<?php
						echo $research_area->name;
					?>
				</a>
				<?php echo __('(' . $research_area->count . ')'); ?>
			</p>
		</div>
		<?php
			}
		?>
	</div>
</div>