<?php

	defined('ABSPATH') or die;

	/*
	 * Category/Archive template for PhD Openings
	 */
	get_header();
?>
<div id="phd-opening-archive">
	<div class="cpt-header">
		<div class="container">
			<?php
				the_archive_title('<h2 class="research-area-title"><span>', '</span></h2>');
				the_archive_description('<div class="research-area-description">', '</div>');
			?>
		</div>
	</div>
	
	<div class="container">
		<div class="uk-grid">
			<div class="uk-width-3-10 archive-left-sidebar">
				<?php
					if ( is_active_sidebar('phd-opening-archive-sidebar') ) {
						dynamic_sidebar('phd-opening-archive-sidebar');
					}
				?>
			</div>
			<div class="uk-width-7-10">
				<?php
					if(have_posts()) : while(have_posts()) : the_post();
						$post_id = get_the_ID();
						$short_info = esc_attr( get_post_meta( $post_id, 'short_info', true ) );
						$institution = esc_attr( get_post_meta( $post_id, 'institution', true ) );
						$institution_details = get_page_by_path($institution, '', 'institutions');
						$institution_logo = esc_url( get_post_meta( $institution_details->ID, 'institution_logo', true ) );
				?>
				<div class="phd-opening-item">
					<div class="uk-grid">
						<div class="uk-width-1-6">
							<?php
								// PhD Opening's Image
								if ( $institution_logo != NULL ) {
							?>
							<a href="<?php echo get_permalink( $post_id ); ?>"><img src="<?php echo $institution_logo; ?>" alt="Logo"></a>
							<?php
								}
							?>
						</div>
						<div class="uk-width-5-6">
							<p class="title">
								<a href="<?php echo get_permalink( $post_id ); ?>"><?php echo $post->post_title; ?></a>
							</p>
							<?php
								if (!empty ($short_info)) {
							?>
							<p class="short-info">
								<?php echo $short_info; ?>
							</p>
							<?php
								}
							?>
							<p class=" phd-opening-info">
								<a href="<?php echo get_permalink( $institution_details ); ?>">
									<?php echo $institution_details->post_title; ?>
								</a>
							</p>
						</div>
					</div>
				</div>
				<?php
						endwhile; 
					else:
				?>
				<p class="no-phd-openings">
					<?php echo __('No PhD Offers found', 'phdhub-cpts'); ?>
				</p>
				<?php
					endif;
				?>
				<div class="pagination-numbers">
					<?php
						echo paginate_links();
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	get_footer();
?>