<?php

	defined('ABSPATH') or die;

	/*
	 * Category/Archive template for Companies
	 */
	get_header();

	global $wp_query;

	/*
	 * Include file for new query based on filters
	 */
	include dirname(plugin_dir_path(__FILE__)) . '/filters/companies-filters-query.php';
?>
<div id="company-archive">
	<div class="uk-grid">
		<div class="uk-width-1-1">
			<div class="cpt-header">
				<div class="container">
					<?php
						the_archive_title('<h2 class="companies-title"><span>', '</span></h2>');
						/*
						 * Include file for new query based on filters
						 */
						include dirname(plugin_dir_path(__FILE__)) . '/filters/companies-filters.php';
						the_archive_description('<div class="institutions-description">', '</div>');
					?>
				</div>
			</div>
			
			<div class="container">
				<div class="uk-grid">
					<?php
						if(have_posts()) : while(have_posts()) : the_post();
							$post_id = get_the_ID();
							$city = esc_attr( get_post_meta( $post_id, 'city', true ) );
							$country = esc_attr( get_post_meta( $post_id, 'country', true ) );
					?>
					<div class="uk-width-1-4">
						<div class="company-item">
							<a href="<?php echo get_permalink( $post_id ); ?>">
								<?php
								   /*
							  	    * Display company's thumbnail
									*/
									if ( has_post_thumbnail() ) {
										the_post_thumbnail( 'medium' );
									}
								?>
							</a>
							<p class="title">
								<a href="<?php echo get_permalink( $post_id ); ?>"><?php echo $post->post_title; ?></a>
							</p>
							<?php
								if ( (!empty ($city)) && (!empty ($country)) ) {
							?>
							<p class="location">
								<?php echo $city . ', ' . $country; ?>
							</p>
							<?php
								}
							?>
						</div>
					</div>
				<?php
						endwhile; 
					else:
				?>
				<div class="no-companies uk-width-1-1">
					<?php echo __('No Companies Found', 'phdhub-cpts'); ?>
				</div>
				<?php
					endif;
				?>
				</div>
				<div class="pagination-numbers">
					<?php
						echo paginate_links();
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	get_footer();
?>