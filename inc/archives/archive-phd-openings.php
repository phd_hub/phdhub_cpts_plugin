<?php

	defined('ABSPATH') or die;

	/*
	 * Category/Archive template for PhD Openings
	 */
	get_header();
?>
<div id="phd-opening-archive-new">
	<div class="cpt-header">
		<div class="container">
			<?php
				the_archive_title('<h2 class="research-area-title"><span>', '</span></h2>');
				if ( is_active_sidebar('phd-opening-archive-sidebar') ) {
					dynamic_sidebar('phd-opening-archive-sidebar');
				}
			?>
		</div>
	</div>
	
	<div class="container">
		<div class="uk-grid">
			<div class="uk-width-1-2">
				<button class="fields-of-science-btn" data-uk-modal="{target:'#fields-of-science-list'}">
			<?php echo __('Select Field of Science', 'phdhub'); ?>
		</button>
				<div id="fields-of-science-list" class="uk-modal">
			<div class="uk-modal-dialog">
				<h3>
					<?php echo __('Select Field of Science', 'phdhub-cpts'); ?>
					<a class="uk-modal-close uk-close"></a>
				</h3>
				
				<div class="uk-grid fos-list">
					<div class="uk-width-1-2">
						<ul data-uk-switcher="{connect:'#fos-list'}">
							<?php 
								$hiterms = get_terms("fields-of-science", array("hide_empty" => false, "parent" => 0));
								foreach($hiterms as $key => $hiterm) {
							?>
							<li>
								<a href=""><?php echo $hiterm->name; ?></a>
							</li>
							<?php
								}
							?>
						</ul>
					</div>
					<div class="uk-width-1-2">
						<ul id="fos-list" class="uk-switcher">
							<?php 
								foreach($hiterms as $key => $hiterm) {
							?>
							<li>
								<a href="<?php echo get_term_link($hiterm->term_id); ?>"><i class="fa fa-angle-double-right"></i> <?php echo $hiterm->name; ?></a>
								<?php
									$loterms = get_terms("fields-of-science", array("hide_empty" => false, "parent" => $hiterm->term_id));
									foreach($loterms as $key => $loterm) {
										$loterms2 = get_terms("fields-of-science", array("hide_empty" => false, "parent" => $loterm->term_id));
								?>
								<a href="<?php echo get_term_link($loterm->term_id); ?>"><i class="fa fa-angle-double-right"></i> <?php echo $loterm->name; ?></a>
								<?php
										if ($loterms2) {
								?>
								<ul class="children-fos">
									<?php
										foreach ($loterms2 as $key => $loterm2) {
									?>
									<li>
										<a href="<?php echo get_term_link($loterm2->term_id); ?>"><i class="fa fa-angle-double-right"></i> <?php echo $loterm2->name; ?></a>
									</li>
									<?php
										}
									?>
								</ul>
								<?php 
										}
									}
								?>
							</li>
							<?php
								}
							?>
						</ul>
					</div>
				</div>
			</div>
		</div>
			</div>
			<div class="uk-width-1-2 offers-filters">
				<a class="current-offers-page" href="<?php echo site_url(); ?>/phd-openings"><?php echo __('Local Offers', 'phdhub-cpts'); ?></a>
				<a href="<?php echo site_url(); ?>/phd-offers"><?php echo __('All Offers', 'phdhub-cpts'); ?></a>
			</div>
		</div>
		
		<div class="uk-grid">
			<?php
				if(have_posts()) : while(have_posts()) : the_post();
					$post_id = get_the_ID();
					$short_info = substr( get_post_meta( $post_id, 'phd_opening_info', true ), 0, 200 );
					$institution = esc_attr( get_post_meta( $post_id, 'institution', true ) );
					$institution_details = get_page_by_path($institution, '', 'institutions');
					$institution_logo = esc_url( get_post_meta( $institution_details->ID, 'institution_logo', true ) );
					$institution_city = esc_attr( get_post_meta( $institution_details->ID, 'city', true ) );
					$institution_country = esc_attr( get_post_meta( $institution_details->ID, 'country', true ) );
	 				$terms = get_the_terms( $post_id , 'fields-of-science' );
			?>
			<div class="uk-width-1-3">
				<div class="phd-opening-item">
					<p class="title">
						<a href="<?php echo get_permalink( $post_id ); ?>"><?php echo $post->post_title; ?></a>
					</p>
					<?php
						if (!empty ($short_info)) {
					?>
					<p class="short-info">
						<?php echo strip_tags($short_info) . '...'; ?>
					</p>
					<?php
						}
						if ($terms) {
					?>
					<p class="field-of-science">
						<i class="fa fa-folder-o"></i>
						<?php
							foreach ( $terms as $term) {
						?>
						<a href="<?php echo get_term_link($term->term_id); ?>"><?php echo $term->name; ?></a>
						<?php
							}
						?>
					</p>
					<?php
						}
					?>
					<p class="date">
						<i class="fa fa-calendar"></i>
						<?php echo get_the_date('d F Y'); ?>
					</p>
					<p class=" phd-opening-info">
						<?php
							// PhD Opening's Image
							if ( $institution_logo != NULL ) {
						?>
						<a href="<?php echo get_permalink( $institution_details ); ?>"><img src="<?php echo $institution_logo; ?>" alt="Logo"></a>
						<?php
							}
						?>
						<a href="<?php echo get_permalink( $institution_details ); ?>">
							<?php echo $institution_details->post_title; ?>
							<span><?php echo $institution_city . ', ' . $institution_country; ?></span>
						</a>
					</p>
				</div>
			</div>
			<?php
					endwhile; 
				else:
			?>
			<p class="no-phd-openings">
				<?php echo __('No PhD Offers found', 'phdhub-cpts'); ?>
			</p>
			<?php
				endif;
			?>
		</div>
		<div class="pagination-numbers">
			<?php
				echo paginate_links();
			?>
		</div>
	</div>
</div>
<?php
	get_footer();
?>