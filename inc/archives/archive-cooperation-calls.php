<?php

	defined('ABSPATH') or die;

	/*
	 * Category/Archive template for PhD Openings
	 */
	get_header();
?>
<div id="calls-archive-new">
	<div class="cpt-header">
		<div class="container">
			<?php
				echo '<h2 class="calls-title"><span>' . __('Cooperation Offers', 'phdhub-cpts') . '</span></h2>';
				the_archive_description('<div class="calls-description">', '</div>');
			?>
		</div>
	</div>
	
	<div class="container">
		<?php
			$user_is_member = 0;
			$subsites = get_sites();
			if ( ! empty ( $subsites ) ) {
				foreach( $subsites as $subsite ) {
					$subsite_id = get_object_vars( $subsite )["blog_id"];
					$user_id = get_current_user_id();
					if ( is_user_member_of_blog( $user_id, $blog_id ) ) {
						$user_is_member = 1;
					}
				}
			}
			if ( $user_is_member == 1) {
		?>
		<div class="uk-grid">
			<?php
				if(have_posts()) : while(have_posts()) : the_post();
					$post_id = get_the_ID();
					$short_info = esc_attr( get_post_meta( $post_id, 'short_info', true ) );
					$institution = esc_attr( get_post_meta( $post_id, 'institution', true ) );
					$institution_details = get_page_by_path($institution, '', 'institutions');
					$institution_logo = esc_url( get_post_meta( $institution_details->ID, 'institution_logo', true ) );
					$institution_city = esc_attr( get_post_meta( $institution_details->ID, 'city', true ) );
					$institution_country = esc_attr( get_post_meta( $institution_details->ID, 'country', true ) );
					$company = esc_attr( get_post_meta( $post_id, 'company', true ) );
					$company_details = get_page_by_path($company, '', 'companies');
					$company_logo = esc_url( get_post_meta( $company_details->ID, 'company_logo', true ) );
					$company_city = esc_attr( get_post_meta( $company_details->ID, 'city', true ) );
					$company_country = esc_attr( get_post_meta( $company_details->ID, 'country', true ) );
			?>
			<div class="uk-width-1-3">
				<div class="call-item">
					<p class="title">
						<a href="<?php echo get_permalink( $post_id ); ?>"><?php echo $post->post_title; ?></a>
					</p>
					<?php
						if (!empty ($short_info)) {
					?>
					<p class="short-info">
						<?php echo $short_info; ?>
					</p>
					<?php
						}
					?>
					<p class="date">
						<i class="fa fa-calendar"></i>
						<?php the_date('d F Y'); ?>
					</p>
					<p class="call-info">
						<span>
							<?php
								// Institution's Logo
								if ( $institution_logo != NULL ) {
							?>
							<a href="<?php echo get_permalink( $institution_details ); ?>"><img src="<?php echo $institution_logo; ?>" alt="Logo"></a>
							<?php
								}
							?>
							<a href="<?php echo get_permalink( $institution_details ); ?>">
								<?php echo $institution_details->post_title; ?>
								<span><?php echo $institution_city . ', ' . $institution_country; ?></span>
							</a>
						</span>
						<span>
							<?php
								// Company's Logo
								if ( $company_logo != NULL ) {
							?>
							<a href="<?php echo get_permalink( $company_details ); ?>"><img src="<?php echo $company_logo; ?>" alt="Logo"></a>
							<?php
								}
							?>
							<a href="<?php echo get_permalink( $company_details ); ?>">
								<?php echo $company_details->post_title; ?>
								<span><?php echo $company_city . ', ' . $company_country; ?></span>
							</a>
						</span>
					</p>
				</div>
			</div>
			<?php
					endwhile; 
				else:
			?>
			<p class="no-calls">
				<?php echo __('No Calls for Cooperation Found', 'phdhub-cpts'); ?>
			</p>
			<?php
				endif;
			?>
		</div>
		<div class="pagination-numbers">
			<?php
				echo paginate_links();
			?>
		</div>
		<?php
			} else {
				echo __('Only a member of the local hubs can view the content of this page!', 'phdhub-cpts');
			}
		?>
	</div>
</div>
<?php
	get_footer();
?>