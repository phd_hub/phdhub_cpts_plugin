==== Version 2.0.0 - 14/08/2018 ====

Changelog:
- NEW: Multilingual support (with .po and .mo files) for Custom Post Types
- NEW: logo field for Institutions, Faculties, Companies and Research Teams CPTs
- NEW: frontend pages (single/archive pages) for the following CPTs: Institutions, Faculties, PhD Programs, PhD Openings, Research Teams, Companies, Calls for Cooperation
- NEW: predefined user roles and capabilities (no need to add them manually)
- NEW: TGMPA plugin activation (in order to require and install/activate 3rd party plugins)
- NEW: multilingual support for custom categories and CPTs is defined during plugin's activation (no need to add it manually)
- NEW: User meta fields