jQuery('#institution-autocomplete').change(function(){
	var institution = jQuery('input[name="institution"]').val();
	var phd_id = jQuery('#phd-id').val();
	jQuery.ajax({
		url: adminscripts.ajax_url,
		type: 'post',
		data: {
			action: 'institution_faculty_list',
			institution: institution,
			phd_id: phd_id
		},
		success: function (response) {
			jQuery('#faculty').html(response);
		}
	});
	return false;
});

jQuery(document).ready(function($){
	var institution = $('input[name="institution"]').val();
	var phd_id = $('#phd-id').val();
	jQuery.ajax({
		url: adminscripts.ajax_url,
		type: 'post',
		data: {
			action: 'institution_faculty_list',
			institution: institution,
			phd_id: phd_id
		},
		success: function (response) {
			jQuery('#faculty').html(response);
		}
	});
	return false;
});

jQuery('#institution-autocomplete').change(function(){
	var institution = jQuery('input[name="institution"]').val();
	var phd_id = jQuery('#phd-id').val();
	jQuery.ajax({
		url: adminscripts.ajax_url,
		type: 'post',
		data: {
			action: 'institution_phd_programs_list',
			institution: institution,
			phd_id: phd_id
		},
		success: function (response) {
			jQuery('#phd-program').html(response);
		}
	});
	return false;
});

jQuery(document).ready(function($){
	var institution = $('input[name="institution"]').val();
	var phd_id = $('#phd-id').val();
	jQuery.ajax({
		url: adminscripts.ajax_url,
		type: 'post',
		data: {
			action: 'institution_phd_programs_list',
			institution: institution,
			phd_id: phd_id
		},
		success: function (response) {
			jQuery('#phd-program').html(response);
		}
	});
	return false;
});

jQuery('#institution-autocomplete').change(function(){
	var institution = jQuery('input[name="institution"]').val();
	var phd_id = jQuery('#phd-id').val();
	jQuery.ajax({
		url: adminscripts.ajax_url,
		type: 'post',
		data: {
			action: 'institution_research_teams_list',
			institution: institution,
			phd_id: phd_id
		},
		success: function (response) {
			jQuery('#research-team').html(response);
		}
	});
	return false;
});

jQuery(document).ready(function($){
	var institution = $('input[name="institution"]').val();
	var phd_id = $('#phd-id').val();
	jQuery.ajax({
		url: adminscripts.ajax_url,
		type: 'post',
		data: {
			action: 'institution_research_teams_list',
			institution: institution,
			phd_id: phd_id
		},
		success: function (response) {
			jQuery('#research-team').html(response);
		}
	});
	return false;
});

jQuery(document).ready(function($){
	var institution = $('input[name="institution"]').val();
	var phd_id = $('#phd-id').val();
	jQuery.ajax({
		url: adminscripts.ajax_url,
		type: 'post',
		data: {
			action: 'institution_phd_openings_list',
			institution: institution,
			phd_id: phd_id
		},
		success: function (response) {
			jQuery('#phd-opening').html(response);
		}
	});
	return false;
});

jQuery('#institution-autocomplete').change(function(){
	var institution = jQuery('input[name="institution"]').val();
	var phd_id = jQuery('#phd-id').val();
	jQuery.ajax({
		url: adminscripts.ajax_url,
		type: 'post',
		data: {
			action: 'institution_phd_openings_list',
			institution: institution,
			phd_id: phd_id
		},
		success: function (response) {
			jQuery('#phd-opening').html(response);
		}
	});
	return false;
});

jQuery(document).ready(function($){

   	var custom_uploader;
	$('#institution_logo_button').click(function(e) {
		e.preventDefault();
		//If the uploader object has already been created, reopen the dialog
		if (custom_uploader) {
			custom_uploader.open();
			return;
		}
		//Extend the wp.media object
		custom_uploader = wp.media.frames.file_frame = wp.media({
			title: 'Choose Image',
			button: {
				text: 'Choose Image'
			},
			multiple: false
		});
		//When a file is selected, grab the URL and set it as the text field's value
		custom_uploader.on('select', function() {
			attachment = custom_uploader.state().get('selection').first().toJSON();
			$('#institution_logo').val(attachment.url);
		});
		//Open the uploader dialog
		custom_uploader.open();
	});
});


jQuery(document).ready(function($){

   	var custom_uploader;
	$('#faculty_logo_button').click(function(e) {
		e.preventDefault();
		//If the uploader object has already been created, reopen the dialog
		if (custom_uploader) {
			custom_uploader.open();
			return;
		}
		//Extend the wp.media object
		custom_uploader = wp.media.frames.file_frame = wp.media({
			title: 'Choose Image',
			button: {
				text: 'Choose Image'
			},
			multiple: false
		});
		//When a file is selected, grab the URL and set it as the text field's value
		custom_uploader.on('select', function() {
			attachment = custom_uploader.state().get('selection').first().toJSON();
			$('#faculty_logo').val(attachment.url);
		});
		//Open the uploader dialog
		custom_uploader.open();
	});
});


jQuery(document).ready(function($){

   	var custom_uploader;
	$('#research_team_logo_button').click(function(e) {
		e.preventDefault();
		//If the uploader object has already been created, reopen the dialog
		if (custom_uploader) {
			custom_uploader.open();
			return;
		}
		//Extend the wp.media object
		custom_uploader = wp.media.frames.file_frame = wp.media({
			title: 'Choose Image',
			button: {
				text: 'Choose Image'
			},
			multiple: false
		});
		//When a file is selected, grab the URL and set it as the text field's value
		custom_uploader.on('select', function() {
			attachment = custom_uploader.state().get('selection').first().toJSON();
			$('#research_team_logo').val(attachment.url);
		});
		//Open the uploader dialog
		custom_uploader.open();
	});
});

jQuery(document).ready(function($){

   	var custom_uploader;
	$('#company_logo_button').click(function(e) {
		e.preventDefault();
		//If the uploader object has already been created, reopen the dialog
		if (custom_uploader) {
			custom_uploader.open();
			return;
		}
		//Extend the wp.media object
		custom_uploader = wp.media.frames.file_frame = wp.media({
			title: 'Choose Image',
			button: {
				text: 'Choose Image'
			},
			multiple: false
		});
		//When a file is selected, grab the URL and set it as the text field's value
		custom_uploader.on('select', function() {
			attachment = custom_uploader.state().get('selection').first().toJSON();
			$('#company_logo').val(attachment.url);
		});
		//Open the uploader dialog
		custom_uploader.open();
	});
});