<?php
/*
Plugin Name: PhD Hub CPTs
Plugin URI: http://it.auth.gr
Description: Custom Post Types for PhD Hub
Text Domain: phdhub-cpts
Domain Path: /lang
Version: 2.0.0
Author: Ioannis Zachros
Author URI: https://it.auth.gr
License: GPLv2 or later
*/

defined('ABSPATH') or die;

add_action( 'plugins_loaded', 'phdhub_cpts_load_textdomain' );
function phdhub_cpts_load_textdomain() {
	load_plugin_textdomain( 'phdhub-cpts', false, dirname( plugin_basename(__FILE__) ) . '/lang/' );
}

define("PHDHUB_CPTS_PLUGIN_DIR", dirname(plugin_basename(__FILE__)));

/*
 * Include the main PhD Hub CPTs file
 */
include_once dirname( __FILE__ ) . '/inc/config.php';

?>
